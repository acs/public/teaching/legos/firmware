//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "driver/ledc.h"
#include "driver/spi_master.h"
#include "driver/timer.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_power.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "ms4.h"
#include "sdkconfig.h"

static const char* TAG = "Skyscraper"; // Logging tag

// ---------- GPIO Definitions ----------
#define PIN_EEPROM GPIO_NUM_0 // 1 Wire EEPROM

#define PIN_TXD GPIO_NUM_1 // Data communication Programming
#define PIN_RXD GPIO_NUM_3

#define PIN_EVENT GPIO_NUM_22 // Ambimate Presence Trigger

#define PIN_SCADA_FAULT GPIO_NUM_4 // Leds for Monitoring
#define PIN_SCADA_LOAD GPIO_NUM_16
#define PIN_SCADA_SOLAR GPIO_NUM_17
#define PIN_SCADA_FUEL GPIO_NUM_19
#define PIN_SCADA_WIND GPIO_NUM_21

#define PIN_CYB_SECURITY GPIO_NUM_14 // Leds for Cyber Security
#define PIN_CYB_BREACH GPIO_NUM_25
#define PIN_CYB_ATTACK GPIO_NUM_27

#define PIN_HOUSE_TV GPIO_NUM_26

#define PIN_LIGHT_INT GPIO_NUM_33 // Lighting LEDs
#define PIN_LIGHT_EXT GPIO_NUM_32

#define IRQ_INA GPIO_NUM_22 // INA233 Interrupt Input

#define PIN_AHU_FLAP GPIO_NUM_15 // Position of Flap
#define PIN_AHU_FAN GPIO_NUM_2   // Air speed
#define PIN_AHU_HEAT GPIO_NUM_12 // Heating

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

enum pwm_control {
    PWM_HOUSE_TV = 0,
    PWM_LIGHT_INT,
    PWM_LIGHT_EXT,
    PWM_AHU_FLAP,
    PWM_AHU_FAN,
    PWM_AHU_HEAT,
    PWM_CONTROL_MAX
};

enum led_monitor_channels {
    LED_SCADA_FAULT = PWM_CONTROL_MAX,
    LED_SCADA_LOAD,
    LED_SCADA_SOLAR,
    LED_SCADA_FUEL,
    LED_SCADA_WIND,
    LED_CYB_SECURITY,
    LED_CYB_BREACH,
    LED_CYB_ATTACK,
    LED_MONITOR_MAX,
};

enum servo_pos {
    SERVO_POS_OPEN = 490,  // Servo position Open
    SERVO_POS_CLOSE = 800, // Servo position Close
};

// ---------- Entity Definitions ----------

/**
 * @brief SCADA system status
 */
typedef struct {
    float load;
    float solar;
    float fuel;
    float wind;
    bool fault;
    bool attack;
    bool breach;
    bool security;
} scada_t;

/**
 * @brief HVAC system status
 */
typedef struct {
    float fan;
    float heat;
    bool flap;
} hvac_t;

/**
 * @brief Entity status structure
 */
struct Skyscraper {
    uint64_t id;             // Unique port identifier
    uint8_t event;           // Sensor event
    scada_t scada;           // SCADA status
    hvac_t hvac;             // HVAC status
    ms4_t ms4;               // Sensor readings
    bool match;              // Sport match status
    float led_intensity;     // Normalized global intensity
    power_readings power_in; // Input telemetry
} entity;

typedef struct skyscraper_control {
    power_monitor power_mon;
    id_bus id_bus;
    // TODO: GPIO
} skyscraper_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/skyscraper";
static const char mqtt_topic_sub[] = "legos/skyscraper/cmd";

static i2c_port_t i2c_num = I2C_NUM_1;    // I2C port
static uint8_t ambimate = MS4_ADDRESS_2A; // I2C address EnviSens

static uint16_t entity_timer_ms = 250;        // Main-tasks Thread interval
static uint64_t entity_timer_alarm;           // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;       // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
    TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
    TIMER_DIVIDER };
static uint8_t remote_override = 0;
static float remote_lights = 0;

// LEDC channels configuration
static ledc_channel_config_t pwm_channel[LED_MONITOR_MAX] = {
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_HOUSE_TV,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LIGHT_INT,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 2,
        .duty = 0,
        .gpio_num = PIN_LIGHT_EXT,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 3,
        .duty = 0,
        .gpio_num = PIN_AHU_FLAP,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 4,
        .duty = 0,
        .gpio_num = PIN_AHU_FAN,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 5,
        .duty = 0,
        .gpio_num = PIN_AHU_HEAT,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_SCADA_FAULT,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = PIN_SCADA_LOAD,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 2,
        .duty = 0,
        .gpio_num = PIN_SCADA_SOLAR,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 3,
        .duty = 0,
        .gpio_num = PIN_SCADA_FUEL,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 4,
        .duty = 0,
        .gpio_num = PIN_SCADA_WIND,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 5,
        .duty = 0,
        .gpio_num = PIN_CYB_SECURITY,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 6,
        .duty = 0,
        .gpio_num = PIN_CYB_BREACH,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 7,
        .duty = 0,
        .gpio_num = PIN_CYB_ATTACK,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 50,                        // PWM frequency
    .speed_mode = PWM_LS_MODE,            // PWM timer mode
    .timer_num = PWM_LS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 *
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel,
        duty, fade_time_ms);
    ledc_fade_start(
        pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG, "PWM fade\tch:%d@%d in %d ms", ch, duty, fade_time_ms);
}

/**
 * @brief Set PWM channel dutycycle
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 *
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG, "PWM switch\tch:%d@%d", ch, duty);
}

/**
 * @brief Update SCADA indicators
 *
 * @param[in] led_intensity Normalized LED intensity
 *
 * @return void
 */
void update_scada_indicator(float led_intensity)
{
    pwm_switch(
        LED_SCADA_FAULT, entity.scada.fault ? led_intensity : LED_INTENSITY_0);
    pwm_switch(LED_SCADA_LOAD, entity.scada.load * led_intensity);
    pwm_switch(LED_SCADA_SOLAR, entity.scada.solar * led_intensity);
    pwm_switch(LED_SCADA_FUEL, entity.scada.fuel * led_intensity);
    pwm_switch(LED_SCADA_WIND, entity.scada.wind * led_intensity);

    pwm_switch(LED_CYB_SECURITY,
        entity.scada.security ? led_intensity : LED_INTENSITY_0);
    pwm_switch(
        LED_CYB_BREACH, entity.scada.breach ? led_intensity : LED_INTENSITY_0);
    pwm_switch(
        LED_CYB_ATTACK, entity.scada.attack ? led_intensity : LED_INTENSITY_0);
}

/**
 * @brief Update HVAC system
 *
 * @param[in] led_intensity Normalized LED intensity
 *
 * @return void
 */
void update_hvac(float led_intensity)
{
    // TODO
}

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief Entity main task
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void entity_main_tasks(void* arg)
{
    skyscraper_control* control = (skyscraper_control*)arg;
    // Initialize manufacturing line count
    entity.hvac.flap = SERVO_POS_OPEN;
    entity.hvac.fan = 1;
    entity.scada.security = true;
    uint8_t cycle = 0;

    entity.ms4.status = true;

    while (1) {
        // Wait timer
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
        entity.power_in = read_power(&control->power_mon);

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity * LED_INTENSITY_100;

        // Update SCADA status
        update_scada_indicator(led_intensity);

        // Update AmbiMate sensors
        ms4_readAll();

        // Update building lights
        pwm_switch(PWM_LIGHT_EXT, led_intensity);
        pwm_switch(
            PWM_HOUSE_TV, entity.match ? led_intensity : LED_INTENSITY_0);
        if (remote_override) {
            pwm_switch(PWM_LIGHT_INT, remote_lights * LED_INTENSITY_100);
            remote_override--;
        } else {
            pwm_switch(PWM_LIGHT_INT,
                entity.ms4.status ? led_intensity : LED_INTENSITY_0);
        }

        // Control HVAC
        pwm_switch(PWM_AHU_FAN, entity.hvac.fan * LED_INTENSITY_100);
        pwm_switch(PWM_AHU_HEAT, entity.hvac.heat * LED_INTENSITY_100);
        pwm_switch(
            PWM_AHU_FLAP, entity.hvac.flap ? SERVO_POS_OPEN : SERVO_POS_CLOSE);

        if (cycle == 199)
            cycle = 0;
        else
            cycle++;

        if (cycle == 0)
            pwm_fade(PWM_AHU_FLAP, SERVO_POS_OPEN, entity_timer_ms * 50);
        if (cycle == 100)
            pwm_fade(PWM_AHU_FLAP, SERVO_POS_CLOSE, entity_timer_ms * 50);
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init PWM channels
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer0 for low speed channels
    // Prepare and set configuration of timer1 for high speed channels
    pwm_timer.freq_hz = 5000;
    pwm_timer.speed_mode = PWM_HS_MODE;
    pwm_timer.timer_num = PWM_HS_TIMER;
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer1 for high speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < LED_MONITOR_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0)); // Initialize fade service
}

void init_ambimate()
{
    ESP_ERROR_CHECK(ms4_init(i2c_num, ambimate, &entity.ms4));
    ms4_readAll();
}

/**
 * @brief Init timer for main entity task
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    entity_timer_alarm = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_entity(skyscraper_control* control)
{
    // Set default LED intensity
    entity.led_intensity = 0.5;

    ESP_LOGD(TAG, "Init PWM");
    init_pwm();

    ESP_LOGD(TAG, "Init ID bus");
    init_id_bus(&control->id_bus, PIN_EEPROM);
    entity.id = read_id(&control->id_bus);
    if (entity.id == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init I2C");
    init_i2c(CONFIG_PWR_MON_PIN_SDA, CONFIG_PWR_MON_PIN_SCL,
        CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_I2C_FREQ);
    control->power_mon
        = pwr_monitor_init(CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon,
        atof(CONFIG_PWR_MON_R_SHUNT), atof(CONFIG_PWR_MON_I_MAX)));

    ESP_LOGD(TAG, "Init ambimate");
    init_ambimate();

    ESP_LOGD(TAG, "Starting main task");
    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());
}

//
//---- EXTERNAL FUNCTIONS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t skyscraper_http_resp(char* str)
{
    sprintf(str, "power|%.2f", entity.power_in.power / 1000);

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t skyscraper_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.led_intensity = val;
            ESP_LOGD(TAG, "LIGHT");
        }
        return ESP_OK;
    }

    if (msg_unmarshall(str, "lights", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            remote_lights = val;
            remote_override = 150;
            ESP_LOGD(TAG, "LIGHTS");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void skyscraper_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    esp_mqtt_client_subscribe(
        mqtt_client, mqtt_topic_service, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_service);

    esp_mqtt_client_subscribe(
        mqtt_client, mqtt_topic_sub, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_sub);
}

void skyscraper_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    char data[256];
    sprintf(data, "%10s: %7.1f mA [%4.2f V]", TAG, entity.power_in.current,
        entity.power_in.voltage);
    esp_mqtt_client_publish(
        mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
}

void skyscraper_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }

        if (msg_unmarshall(data, "light", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0)) {
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return;
        }
        if (msg_unmarshall(data, "fuel", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0))
                entity.scada.fuel = val;
            ESP_LOGD(TAG, "FUEL");
            return;
        }

        if (msg_unmarshall(data, "solar", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0))
                entity.scada.solar = val;
            ESP_LOGD(TAG, "SOLAR");
            return;
        }

        if (msg_unmarshall(data, "wind", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0))
                entity.scada.wind = val;
            ESP_LOGD(TAG, "WIND");
            return;
        }

        if (msg_unmarshall(data, "stadium", &msg_payload)) {
            uint8_t val = atoi(msg_payload.val);
            if (val == 2)
                entity.match = true;
            ESP_LOGD(TAG, "MATCH");
            return;
        }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    skyscraper_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY);
    mqtt_callbacks_t mqtt_cb
        = { .init_callback = skyscraper_mqtt_subscribe_topics,
              .post_callback = skyscraper_mqtt_post,
              .read_callback = skyscraper_mqtt_read };
    init_mqtt(CONFIG_ENTITY, &mqtt_cb); // Init MQTT message protocol

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
