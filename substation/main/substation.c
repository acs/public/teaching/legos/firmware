//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "driver/gpio.h"
#include "driver/ledc.h"
#include "driver/timer.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "sdkconfig.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM GPIO_NUM_0 // 1 Wire EEPROM

#define PIN_TXD GPIO_NUM_1 // Data communication Programming
#define PIN_RXD GPIO_NUM_3

#define FAULT_1 GPIO_NUM_17 // Faults
#define FAULT_2 GPIO_NUM_4
#define FAULT_3 GPIO_NUM_2
#define FAULT_4 GPIO_NUM_12
#define FAULT_5 GPIO_NUM_14
#define FAULT_6 GPIO_NUM_27

#define BUS_EN_1 GPIO_NUM_16
#define BUS_EN_2 GPIO_NUM_19
#define BUS_EN_3 GPIO_NUM_22
#define BUS_EN_4 GPIO_NUM_13
#define BUS_EN_5 GPIO_NUM_32
#define BUS_EN_6 GPIO_NUM_25

#define BUS_IRQ_1 GPIO_NUM_18
#define BUS_IRQ_2 GPIO_NUM_21
#define BUS_IRQ_3 GPIO_NUM_23
#define BUS_IRQ_4 GPIO_NUM_35
#define BUS_IRQ_5 GPIO_NUM_33
#define BUS_IRQ_6 GPIO_NUM_26

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

enum pwm_channels {
    PWM_FAULT_1 = 0, // GPIO_NUM_17  Fault
    PWM_FAULT_2,     // GPIO_NUM_4
    PWM_FAULT_3,     // GPIO_NUM_2
    PWM_FAULT_4,     // GPIO_NUM_12
    PWM_FAULT_5,     // GPIO_NUM_14
    PWM_FAULT_6,     // GPIO_NUM_27
    PWM_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
struct Substation {
    uint64_t id;           // Unique port identifier
    uint8_t bus_linked[6]; // Bus link detected
    uint8_t bus_status[6]; // Bus fault status
    uint8_t bus_enable[6]; // Bus enable status
} entity;

typedef struct substation_control {
    id_bus id_bus;
    // TODO: GPIO
} substation_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/substation";
static const char mqtt_topic_sub[] = "legos/substation/cmd";

static const char* TAG = "Substation"; // Logging tag

static uint16_t entity_timer_ms = 100;        // Main-tasks Thread interval
static uint64_t entity_timer_alarm;           // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;       // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
    TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
    TIMER_DIVIDER };

static uint8_t remote_override = 0;
uint8_t PIN_EN[6]
    = { BUS_EN_1, BUS_EN_2, BUS_EN_3, BUS_EN_4, BUS_EN_5, BUS_EN_6 };
uint8_t PIN_IRQ[6]
    = { BUS_IRQ_1, BUS_IRQ_2, BUS_IRQ_3, BUS_IRQ_4, BUS_IRQ_5, BUS_IRQ_6 };

// LEDC channels configuration
static ledc_channel_config_t pwm_channel[PWM_MAX] = {
    { .channel = 0,
        .duty = 0,
        .gpio_num = FAULT_1, // PWM_FAULT_1
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = FAULT_2, // PWM_FAULT_2
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 2,
        .duty = 0,
        .gpio_num = FAULT_3, // PWM_FAULT_3
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 3,
        .duty = 0,
        .gpio_num = FAULT_4, // PWM_FAULT_4
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 4,
        .duty = 0,
        .gpio_num = FAULT_5, // PWM_FAULT_5
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 5,
        .duty = 0,
        .gpio_num = FAULT_6, // PWM_FAULT_6
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 5000,                      // PWM frequency
    .speed_mode = PWM_LS_MODE,            // PWM timer mode
    .timer_num = PWM_LS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 *
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel,
        duty, fade_time_ms);
    ledc_fade_start(
        pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG, "PWM fade\tch:%d@%d in %d ms", ch, duty, fade_time_ms);
}

/**
 * @brief Set PWM channel dutycycle
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 *
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG, "PWM switch\tch:%d@%d", ch, duty);
}

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief Entity main task
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void entity_main_tasks(void* arg)
{
    // Detect bus link
    for (int i = 0; i < 6; i++)
        entity.bus_linked[i] = !gpio_get_level(PIN_EN[i]);
    ESP_LOGI(TAG, "BUS link");
    ESP_LOG_BUFFER_HEX_LEVEL(TAG, entity.bus_linked, 6, ESP_LOG_INFO);

    // Switch linked bus to control mode
    for (int i = 0; i < 6; i++) {
        if (entity.bus_linked[i]) {
            ESP_ERROR_CHECK(gpio_set_direction(PIN_EN[i], GPIO_MODE_OUTPUT));
            gpio_set_level(PIN_EN[i], 0);
        }
    }

    int8_t fault_cnt = -1;

    while (1) {
        // Wait timer
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Detect bus status
        bool fault = false;
        if (!remote_override) {
            for (int i = 0; i < 6; i++) {
                entity.bus_status[i] = !gpio_get_level(PIN_IRQ[i]);
                fault |= entity.bus_status[i];
            }
        } else {
            remote_override--;
        }

        // FLISR
        if (!fault) { // Restore service
            // Restore service
            for (int i = 0; i < 6; i++)
                if (entity.bus_linked[i])
                    gpio_set_level(PIN_EN[i], 0);
            fault_cnt = -1;
        } else { // Trip reclosers
            if (fault_cnt < 0) {
                // Start overload sequence
                for (int i = 0; i < 6; i++)
                    if (entity.bus_status[i])
                        pwm_fade(i, LED_INTENSITY_100, 3000);
            } else {
                // Open reclosers with delay
                if (fault_cnt == 40)
                    for (int i = 0; i < 6; i++)
                        if (entity.bus_status[i])
                            gpio_set_level(PIN_EN[i], 1);
            }
            fault_cnt++;
        }
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init GPIO
 *
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 0;
    for (int i = 0; i < 6; i++)
        io_conf.pin_bit_mask |= (1ULL << PIN_EN[i]) | (1ULL << PIN_IRQ[i]);
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);

    return ESP_OK;
}

/**
 * @brief Init PWM channels
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer0 for low speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < PWM_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0)); // Initialize fade service
}

/**
 * @brief Init timer for main entity task
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    entity_timer_alarm = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGI(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_entity(substation_control* control)
{
    ESP_LOGD(TAG, "Init GPIO");
    init_gpio();
    ESP_LOGD(TAG, "Init PWM");
    init_pwm();

    ESP_LOGD(TAG, "Init ID bus");
    init_id_bus(&control->id_bus, PIN_EEPROM);
    entity.id = read_id(&control->id_bus);
    if (entity.id == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Starting main task");
    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());
}

//
//---- EXTERNAL CALLBACKS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t substation_http_resp(char* str)
{
    sprintf(str, "%d|%d;%d|%d;%d|%d;%d|%d;%d|%d;%d|%d", entity.bus_linked[0],
        entity.bus_status[0], entity.bus_linked[1], entity.bus_status[1],
        entity.bus_linked[2], entity.bus_status[2], entity.bus_linked[3],
        entity.bus_status[3], entity.bus_linked[4], entity.bus_status[4],
        entity.bus_linked[5], entity.bus_status[5]);

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t substation_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "fault", &msg_payload)) {
        uint8_t val = atoi(msg_payload.val);
        if (val <= 5) {
            remote_override = 100;
            entity.bus_status[val] = 1;
            ESP_LOGD(TAG, "DELTA");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void substation_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_service));
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_sub));
}

void substation_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    char data[260];
    sprintf(data,
        "{\"bus_1_link\": %s, \"bus_1_fault\": %s, \"bus_2_link\": %s, "
        "\"bus_2_fault\": %s, \"bus_3_link\": %s, \"bus_3_fault\": %s, "
        "\"bus_4_link\": %s, \"bus_4_fault\": %s, \"bus_5_link\": %s, "
        "\"bus_5_fault\": %s, \"bus_6_link\": %s, \"bus_6_fault\": %s }",
        entity.bus_linked[0] ? "true" : "false",
        entity.bus_status[0] ? "true" : "false",
        entity.bus_linked[1] ? "true" : "false",
        entity.bus_status[1] ? "true" : "false",
        entity.bus_linked[2] ? "true" : "false",
        entity.bus_status[2] ? "true" : "false",
        entity.bus_linked[3] ? "true" : "false",
        entity.bus_status[3] ? "true" : "false",
        entity.bus_linked[4] ? "true" : "false",
        entity.bus_status[4] ? "true" : "false",
        entity.bus_linked[5] ? "true" : "false",
        entity.bus_status[5] ? "true" : "false");
    esp_mqtt_client_publish(
        mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
}

void substation_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    substation_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY);
    mqtt_callbacks_t mqtt_cb
        = { .init_callback = substation_mqtt_subscribe_topics,
              .post_callback = substation_mqtt_post,
              .read_callback = substation_mqtt_read };
    init_mqtt(CONFIG_ENTITY, &mqtt_cb); // Init MQTT message protocol

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
