//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "driver/ledc.h"
#include "driver/timer.h"
#include "driver/touch_pad.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_power.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "sdkconfig.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM GPIO_NUM_0 // 1 Wire EEPROM

#define PIN_TXD GPIO_NUM_1 // Data communication
#define PIN_RXD GPIO_NUM_3

#define PIN_LOAD_1 GPIO_NUM_25 // Factory Load LED
#define PIN_LOAD_2 GPIO_NUM_26
#define PIN_LOAD_3 GPIO_NUM_27
#define PIN_LOAD_4 GPIO_NUM_14
#define PIN_LOAD_5 GPIO_NUM_12
#define PIN_WELD_W GPIO_NUM_19 // Factory Welding Blue
#define PIN_WELD_B GPIO_NUM_21 // Factory Welding White
#define PIN_ROBOT GPIO_NUM_15  // Factory Robotic Arm

#define IRQ_INA GPIO_NUM_16 // INA233 Interrupt

// ---------- Touch Definitions ----------
#define PIN_TOUCH_POS TOUCH_PAD_NUM9 // GPIO_NUM_32
#define PIN_TOUCH_NEG TOUCH_PAD_NUM8 // GPIO_NUM_33

#define TOUCH_THRESH_NO_USE (0)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

// ---------- PWM Channels Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

enum led_load {
    LED_FACTORY_LOAD_1 = 0, // GPIO_NUM_25  Factory Load LED
    LED_FACTORY_LOAD_2,     // GPIO_NUM_26
    LED_FACTORY_LOAD_3,     // GPIO_NUM_27
    LED_FACTORY_LOAD_4,     // GPIO_NUM_14
    LED_FACTORY_LOAD_5,     // GPIO_NUM_12
    LED_FACTORY_WELD_W,     // GPIO_NUM_19  Factory Welding Blue
    LED_FACTORY_WELD_B,     // GPIO_NUM_21  Factory Welding White
    PWM_FACTORY_ROBOT,      // GPIO_NUM_15  Factory Robotic Arm
    PWM_FACTORY_MAX,
};

enum servo_pos {
    SERVO_POS_0 = 307,   // Servo position   0°
    SERVO_POS_90 = 614,  // Servo position  90°
    SERVO_POS_180 = 922, // Servo position 180°
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
struct Factory {
    uint64_t id;             // Unique port identifier
    float factory_load;      // Normalized factory load
    float led_intensity;     // Normalized global brightness
    power_readings power_in; // Input telemetry
} entity;

typedef struct factory_control {
    power_monitor power_mon;
    id_bus id_bus;
    // TODO: GPIO
} factory_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/factory";
static const char mqtt_topic_sub[] = "legos/factory/cmd";
static const char* TAG = "Factory"; // Logging tag

static uint16_t entity_timer_ms = 100;        // Main-tasks Thread interval
static uint64_t entity_timer_alarm;           // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;       // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
    TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
    TIMER_DIVIDER };

static uint16_t touchpad_thresh = 600; // Touchpad sensitivity threshold
static float factory_load_old = 0.0;
static uint8_t factory_weld_w;
static uint8_t factory_weld_b;

// LEDC channels configuration
static ledc_channel_config_t pwm_channel[PWM_FACTORY_MAX] = {
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_LOAD_1, // LED_FACTORY_LOAD_1
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LOAD_2, // LED_FACTORY_LOAD_2
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 2,
        .duty = 0,
        .gpio_num = PIN_LOAD_3, // LED_FACTORY_LOAD_3
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 3,
        .duty = 0,
        .gpio_num = PIN_LOAD_4, // LED_FACTORY_LOAD_4
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 4,
        .duty = 0,
        .gpio_num = PIN_LOAD_5, // LED_FACTORY_LOAD_5
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 5,
        .duty = 0,
        .gpio_num = PIN_WELD_W, // LED_FACTORY_WELD_W
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 6,
        .duty = 0,
        .gpio_num = PIN_WELD_B, // LED_FACTORY_WELD_B
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_ROBOT, // FACTORY_ROBOT
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 50,                        // PWM frequency
    .speed_mode = PWM_LS_MODE,            // PWM timer mode
    .timer_num = PWM_LS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 *
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel,
        duty, fade_time_ms);
    ledc_fade_start(
        pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG, "PWM fade\tch:%d@%d in %d ms", ch, duty, fade_time_ms);
}

/**
 * @brief Set PWM channel dutycycle
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 *
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG, "PWM switch\tch:%d@%d", ch, duty);
}

/**
 * @brief Read Touchpad level
 *
 * @param[in] ch Touchpad channel
 *
 * @return bool
 */
bool read_touch(touch_pad_t ch)
{
    uint16_t touch_value;
    touch_pad_read_filtered(ch, &touch_value);

    return (touch_value < touchpad_thresh);
}

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief Update Factory load indicator
 *
 * @param[in] led_intensity Normalized LED intensity
 *
 * @return void
 */
void update_led_indicator(float led_intensity)
{
    uint8_t step_fixed = entity.factory_load * (LED_FACTORY_LOAD_5 + 1);
    float step_fade
        = entity.factory_load * (LED_FACTORY_LOAD_5 + 1) - step_fixed;
    step_fade *= step_fade;

    // Set LED status
    for (int i = 0; i < step_fixed; i++)
        pwm_switch(i, led_intensity);
    pwm_switch(step_fixed, step_fade * led_intensity);
    for (int i = step_fixed + 1; i < (LED_FACTORY_LOAD_5 + 1); i++)
        pwm_switch(i, LED_INTENSITY_0);

    // Update old Factory load value
    factory_load_old = entity.factory_load;
}

/**
 * @brief Update Factory manufaturing step
 *
 * @param[in] step Assembly line step
 * @param[in] led_intensity Normalized LED intensity
 *
 * @return void
 */
void update_assembly(uint8_t step, float led_intensity)
{
    float load_intensity = entity.factory_load * led_intensity;

    if (step == 0) {
        factory_weld_w = rand() % 0xFF;
        factory_weld_b = rand() % 0xFF;
    }

    if (step == 1) {
        pwm_fade(PWM_FACTORY_ROBOT, SERVO_POS_180, entity_timer_ms * 25);
    }

    if ((step >= 30) && (step < 38)) {
        pwm_switch(LED_FACTORY_WELD_W,
            ((factory_weld_w & (0x01 << (step - 30))) ? load_intensity
                                                      : LED_INTENSITY_0));
        pwm_switch(LED_FACTORY_WELD_B,
            ((factory_weld_b & (0x01 << (step - 30))) ? load_intensity
                                                      : LED_INTENSITY_0));
    }

    if ((step >= 50) && (step < 58)) {
        pwm_switch(LED_FACTORY_WELD_W,
            ((factory_weld_b & (0x01 << (step - 50))) ? load_intensity
                                                      : LED_INTENSITY_0));
        pwm_switch(LED_FACTORY_WELD_B,
            ((factory_weld_w & (0x01 << (step - 50))) ? load_intensity
                                                      : LED_INTENSITY_0));
    }

    if (step == 60) {
        pwm_fade(PWM_FACTORY_ROBOT, SERVO_POS_0, entity_timer_ms * 30);
    }

    if (step == 110)
        step = 0;
}

void entity_main_tasks(void* arg)
{
    factory_control* control = (factory_control*)arg;
    // Initialize random seed
    srand(time(NULL));

    // Initialize manufacturing line count
    uint8_t step_count = 0;

    while (1) {
        // Wait timer
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
        entity.power_in = read_power(&control->power_mon);

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity * LED_INTENSITY_100;

        // Get touchpad status
        if (read_touch(PIN_TOUCH_POS)) {
            entity.factory_load += 0.05; // 5% increment per touch
            if (entity.factory_load > 1.0F)
                entity.factory_load = 1.0F;
        }
        if (read_touch(PIN_TOUCH_NEG)) {
            entity.factory_load -= 0.05; // 5% decrement per touch
            if (entity.factory_load < 0.0F)
                entity.factory_load = 0.0F;
        }

        // Update Factory LED indicator
        if (entity.factory_load != factory_load_old)
            update_led_indicator(led_intensity);

        // Update Factory manufaturing step
        update_assembly(step_count, led_intensity);
        step_count++;
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init GPIO
 *
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio() { return ESP_OK; }

/**
 * @brief Init PWM channels
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer0 for low speed channels
    // Prepare and set configuration of timer1 for high speed channels
    pwm_timer.freq_hz = 5000;
    pwm_timer.speed_mode = PWM_HS_MODE;
    pwm_timer.timer_num = PWM_HS_TIMER;
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer1 for high speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < PWM_FACTORY_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0)); // Initialize fade service
}

/**
 * @brief Init Touch Channels
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_touch()
{
    ESP_ERROR_CHECK(
        touch_pad_init()); // The default fsm mode is software trigger mode

    ESP_ERROR_CHECK(touch_pad_set_voltage(
        TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_NEG, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_POS, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD));

    return ESP_OK;
}

/**
 * @brief Init timer for main entity task
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    entity_timer_alarm = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_entity(factory_control* control)
{
    entity.led_intensity = 0.5;
    entity.factory_load = 0.2;

    ESP_LOGD(TAG, "Init GPIO");
    init_gpio();
    ESP_LOGD(TAG, "Init PWM");
    init_pwm();
    ESP_LOGD(TAG, "Init Touchbar");
    init_touch();

    ESP_LOGD(TAG, "Init ID bus");
    init_id_bus(&control->id_bus, PIN_EEPROM);
    entity.id = read_id(&control->id_bus);
    if (entity.id == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init I2C");
    init_i2c(CONFIG_PWR_MON_PIN_SDA, CONFIG_PWR_MON_PIN_SCL,
        CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_I2C_FREQ);
    control->power_mon
        = pwr_monitor_init(CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon,
        atof(CONFIG_PWR_MON_R_SHUNT), atof(CONFIG_PWR_MON_I_MAX)));

    ESP_LOGD(TAG, "Starting main task");
    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());
}

//
//---- EXTERNAL FUNCTIONS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t factory_http_resp(char* str)
{
    sprintf(str, "power|%.2f;load|%.2f", entity.power_in.power / 1000,
        entity.factory_load);

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t factory_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.led_intensity = val;
            ESP_LOGD(TAG, "LIGHT");
        }
        return ESP_OK;
    }

    if (msg_unmarshall(str, "load", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.factory_load = val;
            ESP_LOGD(TAG, "LOAD");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void factory_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_service));
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_sub));
}

void factory_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    char data[128];
    sprintf(data,
        "{\"current_in\": %6.1f, \"voltage\": %4.2f, \"factory_load\": "
        "%3.1f}",
        entity.power_in.current, entity.power_in.voltage,
        entity.factory_load * 100);
    esp_mqtt_client_publish(
        mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
}

void factory_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }

        if (msg_unmarshall(data, "light", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0)) {
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return;
        }
    }

    if (!strcmp(topic, mqtt_topic_sub)) {
        if (msg_unmarshall(data, "load", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0)) {
                entity.factory_load = val;
                ESP_LOGD(TAG, "LOAD");
            }
            return;
        }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    factory_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY);
    mqtt_callbacks_t mqtt_cb = { .init_callback = factory_mqtt_subscribe_topics,
        .post_callback = factory_mqtt_post,
        .read_callback = factory_mqtt_read };
    init_mqtt(CONFIG_ENTITY, &mqtt_cb); // Init MQTT message protocol

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
