//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "MFRC522.h"
#include "driver/dac.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "driver/ledc.h"
#include "driver/spi_master.h"
#include "driver/timer.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_i2c.h"
#include "legos_power.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "sdkconfig.h"
// ---------- GPIO Definitions ----------

#define PIN_EEPROM GPIO_NUM_0 // 1 Wire EEPROM

#define PIN_TXD GPIO_NUM_1 // Data communication Programming
#define PIN_RXD GPIO_NUM_3

#define PIN_MISO GPIO_NUM_26 // SPI Pins
#define PIN_MOSI GPIO_NUM_27
#define PIN_CLK GPIO_NUM_14
#define PIN_CS1 GPIO_NUM_13
#define PIN_CS2 GPIO_NUM_5
#define PIN_RST GPIO_NUM_15

#define PIN_CAR_R GPIO_NUM_17
#define PIN_CAR_G GPIO_NUM_16


// ---------- PWM channel Definitions ----------

#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

static const char* TAG = "EC station"; // Logging tag

static spi_device_handle_t RFID1; // SPI handle to RFID reader 1

static uint16_t entity_timer_ms = 1000;       // Main-tasks Thread interval
static uint64_t entity_timer_alarm;           // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;       // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
    TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
    TIMER_DIVIDER };

enum led_car {
    LED_CAR2_R,     // GPIO_NUM_17
    LED_CAR2_G,     // GPIO_NUM_16
    LED_CAR_MAX
};

/**
 * @brief Slot occupation
 */
typedef enum {
    SLOT_NONE = 0,
    SLOT_RIGHT,
    SLOT_LEFT,
    SLOT_BOTH,
} slot_t;

/**
 * @brief RFID tag status
 */
typedef enum { TAG_GONE = 0, TAG_PRESENT } tag_status_t;

/**
 * @brief RFID tag flag
 */
typedef enum { READ_TAG = 0, NO_READ_TAG } tag_flag_t;

/**
 * @brief RFID status structure
 */
typedef struct {
    bool status;            // Tag already read
    bool detect;            // Tag detect
    bool load;              // load info of current step
    uint8_t error_counter;  // Tag presence counteer
    uint8_t storage[18];    // Tag block buffer
    uint8_t BlockAddr;      // Tag block address
    int8_t animation_count; // Animation index of storage
    int8_t animation_fail;  // Fault visualization tag
} rfid_info_t;

/**
 * @brief Entity status structure
 */
struct ECStation {
    uint64_t id;             // Unique port identifier
    uint8_t charging_load;   // Charging load
    float led_intensity;     // Normalized global brightness
    float server_load;       //
    power_readings power_in; // Input telemetry
} entity;

typedef struct ec_station_control {
    power_monitor power_mon;
    id_bus id_bus;
    // TODO: GPIO
} ec_station_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/ec_station";
static const char mqtt_topic_sub[] = "legos/ec_station/cmd";

static slot_t status_remote = SLOT_NONE;
rfid_info_t rfid_car_1; // RFID 1 status

// LEDC channels configuration
static ledc_channel_config_t pwm_channel[LED_CAR_MAX] = {
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_CAR_R,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = PIN_CAR_G,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 5000,                      // PWM frequency
    .speed_mode = PWM_LS_MODE,            // PWM timer mode
    .timer_num = PWM_LS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

// SPI bus configuration
static spi_bus_config_t buscfg = {
    .miso_io_num = PIN_MISO, // SPI MISO
    .mosi_io_num = PIN_MOSI, // SPI MOSI
    .sclk_io_num = PIN_CLK,  // SPI CLK
    .quadwp_io_num = -1,     // -1 if not used
    .quadhd_io_num = -1      // -1 if not used
};

// SPI device configuration
static spi_device_interface_config_t RFID1_cfg = {
    .clock_speed_hz = 1000000, // SPI Clock out at 1 MHz
    .mode = 0,                 // SPI Mode 0
    .spics_io_num = PIN_CS1,   // SPI CS
    .queue_size = 7,           // SPI queue dimension
                     //.pre_cb=spi_pre_transfer_callback,    // Specify
                     // pre-transfer callback to handle D/C line
};
static spi_device_interface_config_t RFID2_cfg = {
    .clock_speed_hz = 1000000, // SPI Clock out at 1 MHz
    .mode = 0,                 // SPI Mode 0
    .spics_io_num = PIN_CS2,   // SPI CS
    .queue_size = 7,           // SPI queue dimension
                     //.pre_cb=spi_pre_transfer_callback,    // Specify
                     // pre-transfer callback to handle D/C line
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 *
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel,
        duty, fade_time_ms);
    ledc_fade_start(
        pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG, "PWM fade\tch:%d@%d in %d ms", ch, duty, fade_time_ms);
}

/**
 * @brief Set PWM channel dutycycle
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 *
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG, "PWM switch\tch:%d@%d", ch, duty);
}

/**
 * @brief  Init RFID Tag Info
 *
 * @param[out] info Pointer to RFID tag status
 *
 * @return void
 */
void rfid_init_info(rfid_info_t* info)
{
    info->error_counter = 0;
    info->status = READ_TAG;
    info->BlockAddr = 60;
    info->animation_count = -1;
    info->animation_fail = 0;
}

/**
 * @brief  Detect RFID Tag
 *
 * @param[in] device SPI device handle
 * @param[in] info Pointer to RFID tag status
 *
 * @return void
 */
void rfid_detect_tag(spi_device_handle_t device, rfid_info_t* info)
{
    uint8_t bufferATQA[4];
    uint8_t bufferSize;

    bufferSize = sizeof(bufferATQA);
    // Checking if a card is present for communication
    info->detect = PICC_WUPA(device, bufferATQA, &bufferSize);
    if (info->detect == STATUS_OK)
        info->error_counter = 0;
    else
        info->error_counter++;

    if (info->error_counter > 2)
        info->status = READ_TAG;
}

/**
 * @brief  Read RFID Tag
 *
 * @param[in] device SPI device handle
 * @param[in] info Pointer to RFID tag status
 *
 * @return void
 */
void rfid_read_tag(spi_device_handle_t device, rfid_info_t* info)
{
    // RFID memory access key
    MIFARE_Key key;
    for (uint8_t i = 0; i < 6; i++)
        key.keyByte[i] = 0xFF;

    PICC_ReadCardSerial(device);
    PICC_StoreSector(device, &uid, info->BlockAddr, info->storage, &key);

    ESP_LOGD(TAG, "RFID buffer content");
    esp_log_buffer_hex_internal(TAG, info->storage, 16, ESP_LOG_DEBUG);
};

/**
 * @brief Car charging LED animation
 *
 * @param[in] info Pointer to RFID tag status
 * @param[in] led_intensity Normalized LED intensity
 * @param[in] led_red   GPIO red LED
 * @param[in] led_green GPIO red GREEN
 *
 * @return void
 */
void LED_animation(
    rfid_info_t* info, float led_intensity, uint8_t led_red, uint8_t led_green)
{
    // show 1 byte out of 18 byte read information
    if (info->status == TAG_PRESENT && info->animation_count <= 16) {
        float load = ((info->storage[info->animation_count])
            / 100.0); // storage value between 0...100
        pwm_switch(led_green, LED_INTENSITY_0);
        pwm_fade(led_red, led_intensity * load, entity_timer_ms);
        info->animation_count++;
        ESP_LOGD(TAG, "Load %5.1f %%", load * 100);
    }

    // reset to ready
    if (((info->status == TAG_PRESENT) && (info->animation_count > 16))
        || (info->status == TAG_GONE && info->animation_fail == 2)) {
        ESP_LOGD(TAG, "Load reset");

        pwm_switch(led_red, LED_INTENSITY_0);
        pwm_switch(led_green, led_intensity);
        info->animation_fail = 0;
        info->animation_count = -1;
    }

    // RFID tag gone before charging finished
    else if ((info->status == TAG_GONE) && (info->animation_count != -1)) {
        ESP_LOGD(TAG, "Load removed");
        pwm_switch(led_green, led_intensity);
        pwm_switch(led_red, led_intensity);
        info->animation_fail++;
    }
};

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

void entity_main_tasks(void* arg)
{
    ec_station_control* control = (ec_station_control*)arg;
    // Init LED
    pwm_switch(LED_CAR2_G, LED_INTENSITY_100);
    pwm_switch(LED_CAR2_R, LED_INTENSITY_0);

    uint8_t load_1, load_2;

    while (1) {
        // Wait timer
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
        entity.power_in = read_power(&control->power_mon);

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity * LED_INTENSITY_100;

        // Detect RFID tags
        rfid_detect_tag(RFID1, &rfid_car_1);
        if ((rfid_car_1.detect == STATUS_OK)
            && (rfid_car_1.status == READ_TAG)) {
            rfid_car_1.status = NO_READ_TAG;
            rfid_read_tag(RFID1, &rfid_car_1);
            rfid_car_1.animation_count = 0;
        };

        // visualise loading cycle
        if (rfid_car_2.animation_count != -1)
            LED_animation(&rfid_car_2, led_intensity, LED_CAR2_R, LED_CAR2_G);

        // Set current load
        if (rfid_car_1.status == TAG_PRESENT) {
            // Local control
            if (rfid_car_1.animation_count != -1)
                load_1 = 127 * (rfid_car_1.storage[rfid_car_1.animation_count])
                    / 100;
            else
                load_1 = 0;

            entity.charging_load = load_1 + load_2;
        } else {
            // Remote control
            // TODO: Fix
            pwm_switch(LED_CAR2_R, led_intensity);
            pwm_switch(LED_CAR2_G, LED_INTENSITY_0);
            entity.charging_load = status_remote * 85;
        }

        // Set virtual load
        dac_output_voltage(DAC_CHANNEL_2, entity.charging_load);
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init GPIO
 *
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    ESP_ERROR_CHECK(gpio_set_direction(PIN_RST, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(dac_output_enable(DAC_CHANNEL_2));
    ESP_ERROR_CHECK(dac_output_voltage(DAC_CHANNEL_2, 0));

    gpio_set_level(PIN_RST, 1);

    return ESP_OK;
}

/**
 * @brief Init PWM channels
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer0 for low speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < LED_CAR_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0)); // Initialize fade service
}

/**
 * @brief Init the SPI interface
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
esp_err_t init_spi()
{
    ESP_ERROR_CHECK(
        spi_bus_initialize(HSPI_HOST, &buscfg, 0)); // Initialize the SPI bus
    ESP_ERROR_CHECK(spi_bus_add_device(
        HSPI_HOST, &RFID1_cfg, &RFID1)); // Add the RFID reader to the SPI bus
    ESP_ERROR_CHECK(spi_bus_add_device(HSPI_HOST, &RFID2_cfg, &RFID2));

    return ESP_OK;
}

/**
 * @brief Init RFID readers
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_rfid()
{
    // Reset RFID Boards before init
    gpio_set_level(PIN_RST, 0);
    vTaskDelay(50 / portTICK_PERIOD_MS);
    gpio_set_level(PIN_RST, 1);
    vTaskDelay(50 / portTICK_PERIOD_MS);

    Init_RFID_shield(RFID1);

    rfid_init_info(&rfid_car_1);
    rfid_init_info(&rfid_car_2);

    return ESP_OK;
}

/**
 * @brief Init timer for main entity task
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    entity_timer_alarm = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_entity(ec_station_control* control)
{
    // Set default LED intensity
    entity.led_intensity = 0.5;
    entity.server_load = 0.25;

    ESP_LOGD(TAG, "Init GPIO");
    init_gpio();
    ESP_LOGD(TAG, "Init PWM");
    init_pwm();
    ESP_LOGD(TAG, "Init SPI");
    init_spi();

    ESP_LOGD(TAG, "Init ID bus");
    init_id_bus(&control->id_bus, PIN_EEPROM);
    entity.id = read_id(&control->id_bus);
    if (entity.id == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init I2C");
    init_i2c(CONFIG_PWR_MON_PIN_SDA, CONFIG_PWR_MON_PIN_SCL,
        CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_I2C_FREQ);
    control->power_mon
        = pwr_monitor_init(CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon,
        atof(CONFIG_PWR_MON_R_SHUNT), atof(CONFIG_PWR_MON_I_MAX)));

    ESP_ERROR_CHECK(init_rfid());

    ESP_LOGD(TAG, "Starting main task");
    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());
}

//
//---- EXTERNAL FUNCTIONS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t ecstation_http_resp(char* str)
{
    sprintf(str, "power|%.2f;", entity.power_in.power / 1000);

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t ecstation_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.led_intensity = val;
            ESP_LOGD(TAG, "LIGHT");
        }
        return ESP_OK;
    }

    if (msg_unmarshall(str, "status", &msg_payload)) {
        uint8_t val = atoi(msg_payload.val);
        if (val <= 3) {
            status_remote = val;
            ESP_LOGD(TAG, "STATUS");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void ec_station_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_service));
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_sub));
}

void ec_station_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    char data[128];
    sprintf(data,
        "{\"current_in\": %6.1f, \"voltage\": %4.2f, \"charging_load\": "
        "%4.1f, \"car1_present\": %s, \"car2_present\": %s }",
        entity.power_in.current, entity.power_in.voltage,
        entity.charging_load / 2.55,
        rfid_car_1.status == TAG_PRESENT ? "true" : "false",
        rfid_car_2.status == TAG_PRESENT ? "true" : "false");

    esp_mqtt_client_publish(
        mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
}

void ec_station_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }

        if (msg_unmarshall(data, "light", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0)) {
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return;
        }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    ec_station_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY);
    mqtt_callbacks_t mqtt_cb
        = { .init_callback = ec_station_mqtt_subscribe_topics,
              .post_callback = ec_station_mqtt_post,
              .read_callback = ec_station_mqtt_read };
    init_mqtt(CONFIG_ENTITY, &mqtt_cb); // Init MQTT message protocol

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
