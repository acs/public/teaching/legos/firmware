//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _TMP100_H_
#define _TMP100_H_

#include "driver/i2c.h"

// Datasheet    https://www.ti.com/lit/ds/symlink/tmp100.pdf

// I2C Addresses
#define TMP100_ADDRESS_72 (0x48) // 1001000 (A1+A0=GND)
#define TMP100_ADDRESS_73 (0x49) // 1001001 (A1=GND,  A0=OPEN)
#define TMP100_ADDRESS_74 (0x4A) // 1001010 (A1=GND,  A0=VDD)
#define TMP100_ADDRESS_75 (0x4B) // 1001011 (A1=OPEN, A0=GND)
#define TMP100_ADDRESS_76 (0x4C) // 1001100 (A1=VDD,  A0=GND)
#define TMP100_ADDRESS_77 (0x4D) // 1001101 (A1=VDD,  A0=OPEN)
#define TMP100_ADDRESS_78 (0x4E) // 1001110 (A1+A0=VDD)
#define TMP100_ADDRESS_79 (0x4F) // 1001111 (A1=OPEN, A0=VDD)

// Device Registers
//      Name            Code    Function                                R/W Size
//      Default
#define TMP_REG_TEMP                                                           \
    (0x00) // Stores the temperature readings      R       2       N/A
#define TMP_REG_CONF                                                           \
    (0x01) // Stores the configuration parameters  R/W     1       0x00
#define TMP_REG_TLOW                                                           \
    (0x02) // Stores the low temperature limit     R/W     2       N/A
#define TMP_REG_THIGH                                                          \
    (0x03) // Stores the high temperature limit    R/W     2       N/A

// TMP100 Configuration Bits
#define TMP_CONF_SD (0)  // Shutdown Mode:   off(0)/on(1)
#define TMP_CONF_TM (1)  // Thermostat Mode: comparator(0)/interrupt(1)
#define TMP_CONF_POL (2) // Alert polarity:  default(0)/inverted(1)
#define TMP_CONF_F0 (3)  // Fault Queue limit
#define TMP_CONF_F1 (4)  // Fault Queue limit
#define TMP_CONF_R0 (5)  // Converter Resolution
#define TMP_CONF_R1 (6)  // Converter Resolution
#define TMP_CONF_OS (7)  // One-Shot/Alert

// TMP100 Resolution Bits
#define TMP_RES_09 (0) //  9 bits (0.50°C)    40 ms
#define TMP_RES_10 (1) // 10 bits (0.25°C)    80 ms
#define TMP_RES_11 (2) // 11 bits (0.125°C)  160 ms
#define TMP_RES_12 (3) // 12 bits (0.0625°C) 320 ms

// I2C definitions
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define ACK_CHECK_EN 0x1            // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0           // I2C master will not check ack from slave
#define ACK_VAL 0x0                 // I2C ack value
#define NACK_VAL 0x1                // I2C nack value
#define I2C_MASTER_TX_BUF_DISABLE 0 // I2C master do not need buffer
#define I2C_MASTER_RX_BUF_DISABLE 0 // I2C master do not need buffer
#define I2C_MASTER_FREQ_HZ 400000   // I2C master clock frequency

#ifdef __cplusplus
extern "C" {
#endif

float tmp100_readTemp(uint8_t addr);
esp_err_t tmp100_init(i2c_port_t v_i2c_num, uint8_t addr);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_TMP100_H_
