//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "tmp100.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"

static const char* TAG = "tmp100";

static i2c_port_t i2c_num = I2C_NUM_1;
uint8_t i2c_address = 0;

/**
 * @brief  Write to I2C address
 *         Send N bytes from data to register reg
 *
 * @param[in]  reg   Register address
 * @param[in]  data  Data buffer
 * @param[in]  N     Data size in bytes
 *
 * @return void
 */
static void i2c_write(uint8_t reg, uint8_t* data, uint8_t N)
{
    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (i2c_address << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN));
    ESP_ERROR_CHECK(i2c_master_write(cmd, &reg, 1, ACK_CHECK_EN));
    for (i = 0; i < N; i++)
        ESP_ERROR_CHECK(i2c_master_write(cmd, &data[i], 1, ACK_CHECK_EN));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);

    vTaskDelay(1 / portTICK_PERIOD_MS);
}

/**
 * @brief  Read from I2C address
 *         Read N bytes in data from register reg
 *
 * @param[in]  reg   Register address
 * @param[out] data  Data buffer
 * @param[in]  N     Data size in bytes
 *
 * @return void
 */
static void i2c_read(uint8_t reg, uint8_t* data, uint8_t size)
{
    // Send register to read from
    i2c_write(reg, NULL, 0);

    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (i2c_address << 1) | I2C_MASTER_READ, ACK_CHECK_EN));
    for (i = 0; i < size; i++)
        ESP_ERROR_CHECK(
            i2c_master_read_byte(cmd, &data[size - 1 - i], ACK_VAL));
    ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], NACK_VAL));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);
}

/**
 * @brief  Set TMP100 temperature conversion resolution
 *
 * @return void
 */
static void tmp100_setResolution(uint8_t addr, uint8_t res)
{
    i2c_address = addr;
    uint8_t conf[2] = { ((res & 0x03) << TMP_CONF_R0), 0xFF };
    i2c_write(TMP_REG_CONF, conf, 2);

    ESP_LOGD(TAG, "Resolution: %d\n", res + 9);
}

/**
 * @brief  Get TMP100 configuration
 *
 * @return void
 */
static void tmp100_getConfiguration(uint8_t addr, uint8_t* conf)
{
    i2c_address = addr;
    i2c_read(TMP_REG_CONF, conf, 1);

    char binary[20];
    ESP_LOGD(TAG, "Configuration: %s\n", __itoa(*conf, binary, 2));
}

/**
 * @brief  Read TMP100 temperature
 *
 * @param[in] addr Device I2C address
 *
 * @return float Temperature
 */
float tmp100_readTemp(uint8_t addr)
{
    i2c_address = addr;
    uint16_t value;
    i2c_read(TMP_REG_TEMP, (uint8_t*)&value, 2);

    float temp = ((int16_t)value) * 39.0625e-4L;
    ESP_LOGD(TAG, "Temperature (0x%2X): %.1f °C\n", addr, temp);

    return temp;
}

/**
 * @brief  Init TMP100 temperature sensor
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_FAIL Device init error
 */
esp_err_t tmp100_init(i2c_port_t v_i2c_num, uint8_t addr)
{
    i2c_num = v_i2c_num;

    // Reset
    // i2c_write(0x06,NULL,0);

    uint8_t conf;

    tmp100_setResolution(addr, TMP_RES_12);
    tmp100_getConfiguration(addr, &conf);

    tmp100_readTemp(addr);

    if (conf != ((TMP_RES_12 << TMP_CONF_R0) | (0x01 << TMP_CONF_OS)))
        return ESP_FAIL;
    else
        return ESP_OK;
}
