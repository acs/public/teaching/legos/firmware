//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <klimt.jonathan@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _PCA9956_H_
#define _PCA9956_H_

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_err.h"

// Datasheet    https://www.nxp.com/docs/en/data-sheet/PCA9956B.pdf

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief  PCA9956 device handle. Should be created with pca9956_init
 *
 * @param[in] pca device handle
 * @param[in] ch LED Channel
 * @param[in] duty LED Duty Cycle
 */
typedef struct pca9956_t {
    uint8_t address;
    i2c_port_t i2c_num;
    float r_ext;
    float i_max_mA;
    gpio_num_t oe_pin;
} pca9956_t;

/**
 * @brief  Set PCA9956 Single LED PWM Value
 *
 * @param[in] pca device handle
 * @param[in] ch LED Channel
 * @param[in] duty LED Duty Cycle
 */
void pca9956_single_switch(pca9956_t* pca, uint8_t ch, uint8_t duty);

/**
 * @brief  Configure the maximum current for a single channel.
 *
 * @param[in] pca device handle
 * @param[in] ch the channel to configure
 * @param[in] i_max_mA the maximum current in mA
 */
void pca9956_set_max_current(pca9956_t* pca, uint8_t ch, float i_max_mA);

/**
 * @brief  Checks the PCA9956's error status and prints the error registers in
 * case of an error
 *
 * @param[in] pca device handle
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_FAIL Device reports error
 */
esp_err_t pca9956_error_check(pca9956_t* pca);

/**
 * @brief  Init PCA9956 power monitor
 *
 * @param[in] v_i2c_num I2C port
 * @param[in] addr I2C device address
 * @param[in] r_ext the external current limiting resistor
 * @param[in] oe_pin the gpio pin connected to the PCA9956 output-enable.
 *
 * @return handle struct for the configured PCA9956
 */
pca9956_t pca9956_init(
    i2c_port_t v_i2c_num, uint8_t addr, float r_ext, gpio_num_t oe_pin);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_PCA9956_H_
