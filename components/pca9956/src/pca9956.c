//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <klimt.jonathan@eonerc.rwth-aachen.de>
//  License:    MIT

#include "pca9956.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_err.h"
#include "esp_log.h"
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>

// Device Registers
#define PCA9956_MODE_1 (0x00)
#define PCA9956_MODE_2 (0x01)
#define PCA9956_LEDOUT_0 (0x02)
#define PCA9956_GROUP_PWM (0x08)
#define PCA9956_GROUP_FREQ (0x09)
#define PCA9956_PWM_0 (0x0A)
#define PCA9956_IREF_0 (0x22)
#define PCA9956_OFFSET (0x3A)
#define PCA9956_SUBADR1 (0x3B)
#define PCA9956_SUBADR2 (0x3C)
#define PCA9956_SUBADR3 (0x3D)
#define PCA9956_ALL_CALLADR (0x3E)
#define PCA9956_ALL_PWM (0x3F)
#define PCA9956_ALL_IREF (0x40)
#define PCA9956_ERFLAGS_0 (0x41)

// I2C definitions
#define ACK_CHECK_DIS 0x0 // I2C master will not check ack from slave
#define ACK_VAL 0x0       // I2C ack value
#define NACK_VAL 0x1      // I2C nack value

static const char* TAG = "PCA9956";

static uint8_t calculate_iref(float r_ext, float i_mA)
{
    // Datasheet section 7.3.13.1
    return (uint8_t)((i_mA * r_ext * 4.0) / 900.0);
}

static void i2c_write(pca9956_t* pca, uint8_t reg, uint8_t* data, uint8_t N)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (pca->address << 1) | I2C_MASTER_WRITE, ACK_CHECK_DIS));
    ESP_ERROR_CHECK(i2c_master_write(cmd, &reg, 1, ACK_CHECK_DIS));
    for (uint8_t i = 0; i < N; i++) {
        ESP_ERROR_CHECK(i2c_master_write(cmd, &data[i], 1, ACK_CHECK_DIS));
    }

    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(pca->i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);

    /*vTaskDelay(1 / portTICK_PERIOD_MS);*/
}

static void i2c_read(pca9956_t* pca, uint8_t reg, uint8_t* data, uint8_t size)
{
    // Send register to read from
    i2c_write(pca, reg, NULL, 0);

    uint8_t i, block_size;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (pca->address << 1) | I2C_MASTER_READ, ACK_CHECK_DIS));
    if (size > 2)
        ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &block_size, ACK_VAL));
    for (i = 0; i < size - 1; i++)
        ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], ACK_VAL));
    ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], NACK_VAL));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(pca->i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);
}

void pca9956_single_switch(pca9956_t* pca, uint8_t ch, uint8_t duty)
{
    uint8_t led_register = PCA9956_PWM_0 + ch;

    /*ESP_LOGD(TAG, "PCA9956 LED %d new Duty Cycle: %x", ch, duty);*/
    i2c_write(pca, led_register, &duty, 1);
}

esp_err_t pca9956_error_check(pca9956_t* pca)
{
    uint8_t mode_2 = 0;
    i2c_read(pca, PCA9956_MODE_2, &mode_2, 1);
    ESP_LOGD(TAG, "PCA9956 Mode 2 register value: %x", mode_2);
    if ((mode_2 & (1 << 6)) != 0) {
        ESP_LOGE(TAG, "PCA9956 Error flag is set");

        for (int i = 0; i <= 5; ++i) {
            uint8_t eflags = 0;
            i2c_read(pca, PCA9956_ERFLAGS_0 + 0, &eflags, 1);
            ESP_LOGE(TAG, "EFLAG%d: %x", i, eflags);
        }
        return ESP_FAIL;
    }
    return ESP_OK;
}

void pca9956_set_max_current(pca9956_t* pca, uint8_t ch, float i_max_mA)
{
    uint8_t iref_register = PCA9956_IREF_0 + ch;
    uint8_t iref = calculate_iref(pca->r_ext, i_max_mA);
    ESP_LOGD(TAG, "PCA9956 LED %d new Current Limit Register: %x", ch, iref);
    i2c_write(pca, iref_register, &iref, 1);
}

pca9956_t pca9956_init(
    i2c_port_t v_i2c_num, uint8_t addr, float r_ext, gpio_num_t oe_pin)
{
    pca9956_t pca9956 = {
        .address = addr,
        .i2c_num = v_i2c_num,
        .r_ext = r_ext,
        .i_max_mA = 0.005,
        .oe_pin = oe_pin,
    };

    uint8_t iref_all = calculate_iref(pca9956.r_ext, 5.0);

    ESP_LOGD(TAG, "PCA9956 new Current Limit register value: %x", iref_all);
    i2c_write(&pca9956, PCA9956_ALL_IREF, &iref_all, 1);

    uint8_t iref0 = 0;
    i2c_read(&pca9956, PCA9956_IREF_0, &iref0, 1);
    ESP_LOGD(TAG, "PCA9956 iref register value: %x", iref0);

    // Check if Chip is present
    uint8_t mode_1 = 0;
    uint8_t mode_2 = 0;
    i2c_read(&pca9956, PCA9956_MODE_1, &mode_1, 1);
    i2c_read(&pca9956, PCA9956_MODE_2, &mode_2, 1);
    ESP_LOGD(TAG, "PCA9956 Mode 1 register value: %x", mode_1);
    ESP_LOGD(TAG, "PCA9956 Mode 2 register value: %x", mode_2);
    if ((mode_2 & 0b111) != 0b101) {
        ESP_LOGE(TAG, "Invalid content of pca9956 mode 2 register!");
        ESP_LOGE(TAG, "Chip probably broken");
        assert(false);
    }

    pca9956_error_check(&pca9956);

    // output enable:
    gpio_set_direction(oe_pin, GPIO_MODE_OUTPUT);
    gpio_set_level(oe_pin, 0);

    return pca9956;
}
