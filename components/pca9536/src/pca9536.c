//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "pca9536.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"

static const char* TAG = "pca9536";

static i2c_port_t i2c_num = I2C_NUM_1;
static uint8_t i2c_address = 0;

static char binary[20];

/**
 * @brief  Write to I2C address
 *         Send N bytes from data to register reg
 *
 * @param[in]  reg   Register address
 * @param[in]  data  Data buffer
 * @param[in]  N     Data size in bytes
 *
 * @return void
 */
static void i2c_write(uint8_t reg, uint8_t* data, uint8_t N)
{
    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (i2c_address << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN));
    ESP_ERROR_CHECK(i2c_master_write(cmd, &reg, 1, ACK_CHECK_EN));
    for (i = 0; i < N; i++)
        ESP_ERROR_CHECK(i2c_master_write(cmd, &data[i], 1, ACK_CHECK_EN));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);

    vTaskDelay(1 / portTICK_PERIOD_MS);
}

/**
 * @brief  Read from I2C address
 *         Read N bytes in data from register reg
 *
 * @param[in]  reg   Register address
 * @param[out] data  Data buffer
 * @param[in]  N     Data size in bytes
 *
 * @return void
 */
static void i2c_read(uint8_t reg, uint8_t* data, uint8_t size)
{
    // Send register to read from
    i2c_write(reg, NULL, 0);

    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (i2c_address << 1) | I2C_MASTER_READ, ACK_CHECK_EN));
    for (i = 0; i < size; i++)
        ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], ACK_VAL));
    ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], NACK_VAL));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);
}

/**
 * @brief  Set PCA9536 port configuration
 *
 * @param[out] conf Port configuration
 *
 * @return void
 */
static void pca9536_setConfiguration(uint8_t* conf)
{
    i2c_write(PCA_REG_CONF, conf, 1);
    ESP_LOGD(TAG, "GPIO write conf: %s\n", __itoa(*conf, binary, 2));
}

/**
 * @brief  Get PCA9536 port configuration
 *
 * @param[out] conf Port configuration
 *
 * @return void
 */
static void pca9536_getConfiguration(uint8_t* conf)
{
    i2c_read(PCA_REG_CONF, conf, 1);
    ESP_LOGD(TAG, "GPIO read conf: %s\n", __itoa(*conf, binary, 2));
}

/**
 * @brief  Set PCA9536 channel level
 *
 * @param[in] ch Channel
 * @param[out] level Logic level
 *
 * @return void
 */
void pca9536_set_gpio_level(uint8_t ch, uint8_t level)
{
    uint8_t out_reg;
    i2c_read(PCA_REG_OUT, &out_reg, 1);

    if (level)
        out_reg |= (0x01 << ch);
    else
        out_reg &= ~(0x01 << ch);
    i2c_write(PCA_REG_OUT, &out_reg, 1);

    ESP_LOGD(TAG, "GPIO output: %s\n", __itoa(out_reg, binary, 2));
    ESP_LOGD(TAG, "GPIO output: ch %d(%d)\n", ch, level);
}

/**
 * @brief  Get PCA9536 channel level
 *
 * @param[in] ch Channel
 * @param[out] level Logic level
 *
 * @return void
 */
void pca9536_get_gpio_level(uint8_t ch, uint8_t* level)
{
    uint8_t out_reg;
    i2c_read(PCA_REG_IN, &out_reg, 1);
    *level = (out_reg & (0x01 << ch));

    ESP_LOGD(TAG, "GPIO input: %s\n", __itoa(*level, binary, 2));
    ESP_LOGD(TAG, "GPIO input: ch %d(%d)\n", ch, *level);
}

/**
 * @brief  Init PCA9536 GPIO expander
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_FAIL Device init error
 */
esp_err_t pca9536_init(i2c_port_t v_i2c_num, uint8_t addr)
{
    i2c_num = v_i2c_num;
    i2c_address = addr;
    uint8_t conf = 0xF0;

    pca9536_setConfiguration(&conf); // Set all output
    pca9536_getConfiguration(&conf);

    if (conf != 0xF0)
        return ESP_FAIL;
    else
        return ESP_OK;
}
