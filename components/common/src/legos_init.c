#include "driver/i2c.h"
#include "esp_err.h"
#include "esp_log.h"

static const char* TAG = "Inits"; // Logging tag

esp_err_t init_i2c(unsigned pin_sda, unsigned pin_scl, i2c_port_t i2c_port_num,
    unsigned i2c_freq)
{
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    ESP_LOGD(TAG, "sda_io_num %d", pin_sda);

    conf.sda_io_num = pin_sda;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "scl_io_num %d", pin_scl);

    conf.scl_io_num = pin_scl;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "clk_speed %d", i2c_freq);

    conf.clk_flags = 0;
    conf.master.clk_speed = i2c_freq;
    ESP_LOGD(TAG, "i2c_param_config %d", conf.mode);
    ESP_ERROR_CHECK(i2c_param_config(i2c_port_num, &conf));

    ESP_LOGD(TAG, "i2c_driver_install %d", i2c_port_num);
    return (i2c_driver_install(i2c_port_num, conf.mode, 0, 0, 0));
}
