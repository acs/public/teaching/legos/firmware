#include "legos_power.h"
#include "esp_err.h"
#include "esp_log.h"
#include "sdkconfig.h"

#ifdef CONFIG_INA231
#include "ina231.h"
#elif defined CONFIG_INA233
#include "ina233.h"
#elif defined CONFIG_INA209
#include "ina209.h"
#else
#warning "No power monitor configured"
#endif

static const char* TAG = "pwr_mon";

power_readings read_power(power_monitor* pmon)
{
    float volt, curr, pwr;
#ifdef CONFIG_INA231
    volt = ina231_getBusVoltage_mV(pmon->i2c_addr);
    curr = ina231_getCurrent_mA(pmon->i2c_addr);
    pwr = ina231_getPower_mW(pmon->i2c_addr);
#elif defined CONFIG_INA233
    volt = ina233_getBusVoltage_mV(pmon->i2c_addr);
    curr = ina233_getCurrent_mA(pmon->i2c_addr);
    pwr = ina233_getPower_mW(pmon->i2c_addr);
#elif defined CONFIG_INA209
    volt = ina209_getBusVoltage_mV(pmon->i2c_addr);
    curr = ina209_getCurrent_mA(pmon->i2c_addr);
    pwr = ina209_getPower_mW(pmon->i2c_addr);
#else
    ESP_LOGE(TAG,
        "Trying to read from power monitor, but no power monitor is "
        "configured");
    assert(false);
#endif
    power_readings pwr_read
        = { .voltage = volt, .current = curr, .power = pwr };
    return pwr_read;
}

esp_err_t set_calibration(power_monitor* pmon, float r_shunt, float i_max)
{
#ifdef CONFIG_INA231
    ESP_LOGD(
        TAG, "Calibrating INA231 with r_shunt: %f, i_max: %f", r_shunt, i_max);
    return ina231_setCalibration(pmon->i2c_addr, r_shunt, i_max);
#elif defined CONFIG_INA233
    ESP_LOGD(
        TAG, "Calibrating INA233 with r_shunt: %f, i_max: %f", r_shunt, i_max);
    return ina233_setCalibration(pmon->i2c_addr, r_shunt, i_max);
#elif defined CONFIG_INA209
    ESP_LOGD(
        TAG, "Calibrating INA209 with r_shunt: %f, i_max: %f", r_shunt, i_max);
    return ina209_setCalibration(pmon->i2c_addr, r_shunt, i_max);
#else
    ESP_LOGE(TAG,
        "Trying to calibrate power monitor, but no power monitor is "
        "configured");
    assert(false);
#endif
    return ESP_FAIL;
}

power_monitor pwr_monitor_init(i2c_port_t v_i2c_num, uint8_t addr)
{
#ifdef CONFIG_INA231
    ESP_LOGD(TAG, "Initializing INA231 on addr %d", addr);
    ESP_ERROR_CHECK(ina231_init(v_i2c_num, addr));
#elif defined CONFIG_INA233
    ESP_LOGD(TAG, "Initializing INA233 on addr %d", addr);
    ESP_ERROR_CHECK(ina233_init(v_i2c_num, addr));
#elif defined CONFIG_INA209
    ESP_LOGD(TAG, "Initializing INA209 on addr %d", addr);
    ESP_ERROR_CHECK(ina209_init(v_i2c_num, addr));
#else
    ESP_LOGE(TAG,
        "Trying to initialize power monitor, but no power monitor is "
        "configured");
    assert(false);
#endif
    power_monitor pm = { .i2c_addr = addr };
    return pm;
}
