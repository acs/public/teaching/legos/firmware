//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "legos_wifi.h"
#include "esp_log.h"
/*#include "esp_netif_types.h"*/
#include "esp_system.h"
#include "esp_wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "lwip/err.h"
/*#include "lwip/ip4_addr.h"*/
#include "lwip/inet.h"
#include "lwip/sys.h"
#include "sdkconfig.h"

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define NETW_CONNECT_BIT BIT0
#define NETW_FAIL_BIT BIT1

static EventGroupHandle_t netif_event_group;
static esp_netif_t* sta_netif = NULL;
static wifi_config_t wifi_config;

static const char* TAG = "Network";

#define IS_FILE_EXT(filename, ext)                                             \
    (strcasecmp(&filename[strlen(filename) - sizeof(ext) + 1], ext) == 0)

esp_netif_ip_info_t get_ip_info(void)
{
    esp_netif_ip_info_t ip_info;
    esp_netif_get_ip_info(sta_netif, &ip_info);
    return ip_info;
}

/**
 * @brief WiFi event handler
 *        Finite State Machine
 *
 * @param[in] arg Parameters passed to the task
 * @param[in] event_base Event primitive
 * @param[in] event_id Event id
 * @param[in] event_data Event data
 *
 * @return void
 */
void network_event_handler(
    void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    ESP_LOGD(TAG, "Netw EVENT");
    if (event_base == WIFI_EVENT) {
        switch (event_id) {
        case WIFI_EVENT_STA_START:
            ESP_LOGD(TAG, "WiFi Event:\tSTA start");
            esp_wifi_connect();
            break;

        case WIFI_EVENT_STA_CONNECTED:
            ESP_LOGD(TAG, "WiFi Event:\tSTA connected");
            break;

        case WIFI_EVENT_STA_DISCONNECTED:
            ESP_LOGD(TAG, "WiFi Event:\tSTA disconnected");
            esp_wifi_connect();
            xEventGroupClearBits(netif_event_group, NETW_CONNECT_BIT);
            wifi_event_sta_disconnected_t* wifi_event
                = (wifi_event_sta_disconnected_t*)event_data;
            if (wifi_event->reason == WIFI_REASON_NO_AP_FOUND) {
                // ToDo
            }
            break;

        default:
            break;
        }
    } else if (event_base == IP_EVENT) {
        switch (event_id) {
        case IP_EVENT_STA_GOT_IP: {
            esp_netif_dns_info_t dns_info;
            ip_event_got_ip_t* event = (ip_event_got_ip_t*)event_data;
            esp_netif_t* netif = event->esp_netif;

            ESP_LOGD(TAG, "IP Event:\tSTA Connect to Server ");
            ESP_LOGD(TAG, "IP          : " IPSTR, IP2STR(&event->ip_info.ip));
            ESP_LOGD(
                TAG, "Netmask     : " IPSTR, IP2STR(&event->ip_info.netmask));
            ESP_LOGD(TAG, "Gateway     : " IPSTR, IP2STR(&event->ip_info.ip));
            esp_netif_get_dns_info(netif, 0, &dns_info);
            ESP_LOGD(
                TAG, "Name Server1: " IPSTR, IP2STR(&dns_info.ip.u_addr.ip4));
            esp_netif_get_dns_info(netif, 1, &dns_info);
            ESP_LOGD(
                TAG, "Name Server2: " IPSTR, IP2STR(&dns_info.ip.u_addr.ip4));

            xEventGroupSetBits(netif_event_group, NETW_CONNECT_BIT);
        } break;

        default:
            break;
        }
    }
}

/**
 * @brief Init STA
 *        Initialize the WiFi STA interface
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_STA()
{
    ESP_LOGD(TAG, "Init STA mode");

    wifi_sta_config_t sta
        = { .ssid = CONFIG_WIFI_STA_SSID, .password = CONFIG_WIFI_STA_PASS };
    wifi_config.sta = sta;

    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_LOGD(TAG, "STA mode credentials: SSID:[%s] password:[%s]", sta.ssid,
        sta.password);

    ESP_ERROR_CHECK(esp_wifi_start());
    return tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA, CONFIG_ENTITY);
}

esp_err_t init_wifi()
{
    ESP_LOGD(TAG, "Init Network Interface");

    netif_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    sta_netif = esp_netif_create_default_wifi_sta();
    assert(sta_netif);
#ifndef CONFIG_WIFI_DHCP
    esp_netif_dhcpc_stop(sta_netif);

    esp_netif_ip_info_t ip_info;

    ip_info.ip.addr = ipaddr_addr(CONFIG_WIFI_STATIC_IP);
    ip_info.gw.addr = ipaddr_addr(CONFIG_WIFI_GATEWAY);
    ip_info.netmask.addr = ipaddr_addr(CONFIG_WIFI_NETMASK);

    esp_netif_set_ip_info(sta_netif, &ip_info);
#endif

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(
        WIFI_EVENT, ESP_EVENT_ANY_ID, &network_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(
        IP_EVENT, ESP_EVENT_ANY_ID, &network_event_handler, NULL));

    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(init_STA());
    xEventGroupWaitBits(netif_event_group, NETW_CONNECT_BIT | NETW_FAIL_BIT,
        pdFALSE, pdFALSE, portMAX_DELAY);

    esp_netif_ip_info_t ip_info_final;
    esp_netif_get_ip_info(sta_netif, &ip_info_final);
    /*char* ip = ip4addr_ntoa(&ip_info_final.ip);*/
    ESP_LOGI(TAG, "Connected to WiFi %s - address " IPSTR, CONFIG_WIFI_STA_SSID,
        IP2STR(&ip_info_final.ip));
    return ESP_OK;
}
