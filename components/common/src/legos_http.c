//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "legos_http.h"
#include "esp_http_server.h"
#include "esp_log.h"
#include "esp_netif_ip_addr.h"
#include "legos_wifi.h"
#include "lwip/ip4_addr.h"

static httpd_handle_t server = NULL;

static const char* TAG = "http-Server";

http_callbacks* http_cbs = NULL;

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define IS_FILE_EXT(filename, ext)                                             \
    (strcasecmp(&filename[strlen(filename) - sizeof(ext) + 1], ext) == 0)

/**
 * @brief Set file type
 *        Set HTTP response content type according to file extension
 *
 * @param[in] req Pointer to http request
 * @param[in] filename Pointer to file name
 *
 * @return
 *  - ESP_OK : Request success
 *  - ESP_FAIL : Request fail
 */
static esp_err_t set_content_type_from_file(
    httpd_req_t* req, const char* filename)
{
    if (IS_FILE_EXT(filename, ".pdf")) {
        return httpd_resp_set_type(req, "application/pdf");
    } else if (IS_FILE_EXT(filename, ".html")) {
        return httpd_resp_set_type(req, "text/html");
    } else if (IS_FILE_EXT(filename, ".css")) {
        return httpd_resp_set_type(req, "text/css");
    } else if (IS_FILE_EXT(filename, ".jpeg")) {
        return httpd_resp_set_type(req, "image/jpeg");
    } else if (IS_FILE_EXT(filename, ".png")) {
        return httpd_resp_set_type(req, "image/png");
    } else if (IS_FILE_EXT(filename, ".ico")) {
        return httpd_resp_set_type(req, "image/x-icon");
    }
    /* This is a limited set only */
    /* For any other type always set as plain text */
    return httpd_resp_set_type(req, "text/plain");
}

/**
 * @brief HTTP GET handler
 *        Called upon HTTP request GET
 *
 * @param[in] req Pointer to http request
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t server_handler_get(httpd_req_t* req)
{
    ESP_LOGD(TAG, "HTTP GET  %s", req->uri);

    size_t buf_len, chunk_len, path_len = 64;
    char *buf, path[path_len];

    // Get header value string length and allocate memory
    buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
    if (buf_len > 1) {
        buf = (char*)malloc(buf_len);
        /* Copy null terminated value string into buffer */
        if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK)
            ESP_LOGD(TAG, "Found header => Host: %s", buf);
        free(buf);
    }

    // Prepare buffer
    buf_len = 8192;
    buf = (char*)calloc(buf_len, sizeof(char));

    // Handle specific requests
    if (!strcmp(req->uri, "/status")) {
        // Call entity response handler
        http_cbs->http_status_callback(buf);
        httpd_resp_set_type(req, "text/plain");
        httpd_resp_send(req, (const char*)buf, strlen(buf));
    } else {
        // Serve file
        memset(path, 0, path_len);
        strcpy(path, "/spiffs");
        if (!strcmp(req->uri, "/"))
            strcat(path, "/index.html");
        else
            strcat(path, req->uri);

        FILE* fd = fopen(path, "r");
        if (fd == NULL) {
            ESP_LOGE(TAG, "Failed to open %s", path);
            httpd_resp_send_err(
                req, HTTPD_404_NOT_FOUND, "File does not exist");
            return ESP_FAIL;
        }

        set_content_type_from_file(req, path);

        do {
            // Read file in chunks into the scratch buffer
            chunk_len = fread(buf, 1, buf_len, fd);

            if (chunk_len > 0) {
                // Send the buffer contents as HTTP response chunk
                if (httpd_resp_send_chunk(req, buf, chunk_len) != ESP_OK) {
                    fclose(fd);
                    ESP_LOGE(TAG, "File sending failed!");
                    // Abort sending file
                    httpd_resp_sendstr_chunk(req, NULL);
                    // Respond with 500 Internal Server Error
                    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                        "Failed to send file");
                    return ESP_FAIL;
                }
            }
            // Keep looping till the whole file is sent
        } while (chunk_len != 0);
        fclose(fd);

        // Respond with an empty chunk to signal HTTP response completion
        httpd_resp_send_chunk(req, NULL, 0);
    }
    free(buf);

    /* After sending the HTTP response the old HTTP request
     * headers are lost. Check if HTTP request headers can be read now. */
    if (httpd_req_get_hdr_value_len(req, "Host") == 0)
        ESP_LOGD(TAG, "Request headers lost");

    return ESP_OK;
}

/**
 * @brief HTTP POST handler
 *        Called upon HTTP request POST
 *
 * @param[in] req Pointer to http request
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t server_handler_post(httpd_req_t* req)
{
    char buf[64];
    int ret = 0;
    int remaining = req->content_len;

    while (remaining > 0) {
        // Read the data for the request
        if ((ret = httpd_req_recv(req, buf, MIN(remaining, sizeof(buf))))
            <= 0) {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                // Retry receiving if timeout occurred
                continue;
            }
            return ESP_FAIL;
        }
        remaining -= ret;
    }
    buf[req->content_len] = '\0';

    ESP_LOGD(TAG, "HTTP POST %s %s", req->uri, buf);
    // Call entity post request handler
    return http_cbs->http_post_callback(buf);
}

esp_err_t server_start(http_callbacks* callbacks)
{
    http_cbs = callbacks;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.uri_match_fn = httpd_uri_match_wildcard;
    esp_ip4_addr_t ip = get_ip_info().ip;
    ESP_LOGD(TAG, "Starting HTTP server on " IPSTR ":%d", IP2STR(&ip),
        config.server_port);

    // Start the httpd server
    if (httpd_start(&server, &config) == ESP_OK) {
        ESP_LOGD(TAG, "Registering URI handlers");

        // URI handler for generic GET requests
        httpd_uri_t uri_get = { .uri = "/*",
            .method = HTTP_GET,
            .handler = server_handler_get,
            .user_ctx = NULL };
        httpd_register_uri_handler(server, &uri_get);

        // URI handler for specific POST requests
        httpd_uri_t uri_post = { .uri = "/post",
            .method = HTTP_POST,
            .handler = server_handler_post,
            .user_ctx = NULL };
        httpd_register_uri_handler(server, &uri_post);
        return ESP_OK;
    }

    ESP_LOGE(TAG, "Could not start server");
    return ESP_FAIL;
}

esp_err_t server_stop()
{
    ESP_LOGD(TAG, "Halting HTTP server");
    // Stop the httpd server
    return httpd_stop(server);
}
