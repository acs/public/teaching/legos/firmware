//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "legos_mqtt.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "legos_common.h"
#include "legos_libs.h"
#include "mqtt_client.h"
#include "sdkconfig.h"

static uint64_t mqtt_timer_alarm;             // MQTT Thread timer
static TaskHandle_t mqtt_publisher_handle;    // MQTT Pub task handle
static TaskHandle_t mqtt_subscriber_handle;   // MQTT Sub task handle
static esp_mqtt_client_handle_t mqtt_client;  // MQTT client handle
static esp_mqtt_event_handle_t mqtt_event;    // MQTT event handle
static timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
    TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
    TIMER_DIVIDER };

static const char* TAG = "Mqtt";

const uint16_t MQTT_MAX_FAILURES = 10;
uint16_t mqtt_failures = 0;

// Stores the mqtt user callbacks. Must be configured via init_mqtt. Non-global
// state would be preferred, but mqtt_event_handler doesn't support parameters.
mqtt_callbacks_t* mqtt_cb = NULL;

/**
 * @brief MQTT subscriber task
 *        Handle incoming messages
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void mqtt_subscriber_task(void* arg)
{
    while (1) {
        // Wait for notification from mqtt_event_handler
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Read data
        mqtt_cb->read_callback(mqtt_event);
    }
    vTaskDelete(NULL);
}

/**
 * @brief MQTT publisher task
 *        Handle outgoing messages
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void mqtt_publisher_task(void* arg)
{
    while (1) {
        // Wait for notification from mqtt_timer_isr
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Publish data
        mqtt_cb->post_callback(mqtt_client);
    }
    vTaskDelete(NULL);
}

/**
 * @brief MQTT event handler
 *        Finite State Machine
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    if (event->event_id == MQTT_EVENT_ERROR) {
        mqtt_failures += 1;
        ESP_LOGE(
            TAG, "MQTT Error No %d of %d", mqtt_failures, MQTT_MAX_FAILURES);
        if (mqtt_failures > MQTT_MAX_FAILURES) {
            ESP_LOGE(TAG, "Too many successive MQTT errors. Resetting...");
            esp_restart();
        }
    } else if (event->event_id == MQTT_EVENT_SUBSCRIBED
        || event->event_id == MQTT_EVENT_PUBLISHED
        || event->event_id == MQTT_EVENT_DATA) {
        ESP_LOGI(TAG, "Got a connection MQTT EVENT: %d", event->event_id);
        mqtt_failures = 0;
    } else {
        ESP_LOGI(TAG, "MQTT EVENT: %d", event->event_id);
    }
    switch (event->event_id) {
    case MQTT_EVENT_ANY:
        break;

    case MQTT_EVENT_CONNECTED:
        ESP_LOGD(TAG, "MQTT_EVENT_CONNECTED");

        // Init entity mqtt
        mqtt_cb->init_callback(mqtt_client);

        ESP_ERROR_CHECK(
            timer_start(TIMER_GROUP_1, TIMER_0)); // Start PUBLISH Timer
        break;

    case MQTT_EVENT_DISCONNECTED:
        ESP_ERROR_CHECK(
            timer_pause(TIMER_GROUP_1, TIMER_0)); // Pause PUBLISH Timer
        ESP_LOGD(TAG, "MQTT_EVENT_DISCONNECTED");
        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGD(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        break;

    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGD(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;

    case MQTT_EVENT_PUBLISHED:
        ESP_LOGD(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;

    case MQTT_EVENT_DATA:
        ESP_LOGD(TAG, "MQTT_EVENT_DATA");
        mqtt_event = event;
        xTaskNotify(mqtt_subscriber_handle, 0x00, eNoAction);
        break;

    case MQTT_EVENT_DELETED:
        break;

    case MQTT_EVENT_BEFORE_CONNECT:
        break;

    case MQTT_EVENT_ERROR:
        ESP_LOGE(TAG, "MQTT_EVENT_ERROR: %d (socket errno: %d)",
            event->error_handle->error_type,
            event->error_handle->esp_transport_sock_errno);
        break;
    }
    return ESP_OK;
}

/**
 * @brief MQTT task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR mqtt_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG1.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_1, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(mqtt_publisher_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief Init timer for MQTT tasks
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_mqtt_timer()
{
    uint16_t delay_ms = 1000 / CONFIG_MQTT_RATE;
    mqtt_timer_alarm = (uint64_t)delay_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_1, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_1, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_1, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_1, TIMER_0, mqtt_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_1, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_1, TIMER_0, mqtt_timer_isr, NULL, 0, NULL));

    ESP_LOGD(TAG, "Refresh interval set to: %d ms", delay_ms);

    return ESP_OK;
}

esp_err_t mqtt_subscribe(esp_mqtt_client_handle_t* client, const char* topic)
{
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_service);
    int id = esp_mqtt_client_subscribe(
        mqtt_client, mqtt_topic_service, CONFIG_MQTT_QOS_LEVEL);
    if (id == -1) {
        ESP_LOGE(TAG, "Error: couldn't subscribe to topic");
        return ESP_FAIL;
    }
    return ESP_OK;
}

esp_mqtt_client_handle_t init_mqtt(
    char* client_name, mqtt_callbacks_t* callbacks)
{
    ESP_LOGD(TAG, "Init MQTT module");

    esp_mqtt_client_config_t mqtt_cfg = { .event_handle = mqtt_event_handler,
        .host = CONFIG_MQTT_HOST,
        .port = CONFIG_MQTT_PORT,
        .client_id = client_name, // CONFIG_ENTITY,
        .username = CONFIG_MQTT_USER,
        .password = CONFIG_MQTT_PASS,
        .transport = CONFIG_MQTT_TRANSPORT };

    mqtt_cb = callbacks;

    xTaskCreate(mqtt_publisher_task, "mqtt_publisher_task", 3072, NULL, 4,
        &mqtt_publisher_handle);
    xTaskCreate(mqtt_subscriber_task, "mqtt_subscriber_task", 3072, NULL, 4,
        &mqtt_subscriber_handle);

    ESP_ERROR_CHECK(init_mqtt_timer());
    mqtt_client = esp_mqtt_client_init(&mqtt_cfg);

    return esp_mqtt_client_start(mqtt_client);
}
