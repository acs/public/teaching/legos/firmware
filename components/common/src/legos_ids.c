#include "legos_ids.h"
#include "esp_log.h"
#include "inttypes.h"
#include "owb.h"
#include "string.h"
/*#include "owb_rmt.h"*/

static const char* TAG = "Node ID Bus"; // Logging tag

uint64_t bytes_to_u64(uint8_t* bytes)
{
    return bytes[0] | ((uint64_t)bytes[1] << 8) | ((uint64_t)bytes[2] << 16)
        | ((uint64_t)bytes[3] << 24) | ((uint64_t)bytes[4] << 32)
        | ((uint64_t)bytes[5] << 40) | ((uint64_t)bytes[6] << 48)
        | ((uint64_t)bytes[7] << 56);
}

void init_id_bus(id_bus* bus, gpio_num_t gpio_num)
{
    // Create a 1-Wire bus, using the RMT timeslot driver
    OneWireBus* owb;
    /*owb_rmt_driver_info rmt_driver_info;*/
    owb = owb_rmt_initialize(bus, gpio_num, RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true); // enable CRC check for ROM code
    owb_use_parasitic_power(owb, true);
}

uint64_t read_id(id_bus* bus)
{
    // Find connected device
    OneWireBus_SearchState search_state = { 0 };
    bool found = false;
    owb_search_first(&bus->bus, &search_state, &found);
    OneWireBus_ROMCode device_rom_code = search_state.rom_code;

    if (found) {
        uint64_t id = bytes_to_u64(device_rom_code.bytes);
        ESP_LOGI(TAG, "Node ID: %016jx", id);
        return ESP_OK;
    } else {
        ESP_LOGE(TAG, "can't read from owb");
        return 0;
    }
}
