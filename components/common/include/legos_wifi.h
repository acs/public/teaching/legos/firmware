//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_WIFI_H_
#define _LEGOS_WIFI_H_

#include "esp_err.h"
#include "esp_netif_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Init WiFi
 *        Initialize the WiFi network adapter
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_wifi();

/**
 * @brief Returns the current IP addres config
 *
 * @return The IP configuration
 */
esp_netif_ip_info_t get_ip_info(void);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_LEGOS_WIFI_H_
