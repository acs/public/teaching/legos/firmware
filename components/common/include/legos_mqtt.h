//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_MQTT_H_
#define _LEGOS_MQTT_H_

#include "esp_err.h"
#include "mqtt_client.h"

/**
 * @brief Connection established callback. Is executed after connection with
 * MQTT broker is established.
 *
 * @param client handle for the callback to publish messages to.
 */
typedef void (*mqtt_init_callback_t)(esp_mqtt_client_handle_t client);

/**
 * @brief Post callback. Callback for periodic publishing updates via MQTT. Is
 * executed at a regular interval (`CONFIG_MQTT_RATE` in Hz).
 *
 * @param client handle for the callback to publish messages to.
 */
typedef void (*mqtt_post_callback_t)(esp_mqtt_client_handle_t client);

/**
 * @brief Read callback. Is executed on a `MQTT_EVENT_DATA` event, i.e., when a
 * message is received.
 *
 * @param event handle containing the message information.
 */
typedef void (*mqtt_read_callback_t)(esp_mqtt_event_handle_t event);

typedef struct mqtt_callbacks {
    mqtt_init_callback_t init_callback;
    mqtt_post_callback_t post_callback;
    mqtt_read_callback_t read_callback;
} mqtt_callbacks_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Subscribe ot a MQTT topic
 *
 * @param client_name name which is registered at the MQTT broker
 * @param topic string containing the topic to subscribe to
 *
 * @return
 *  - ESP_OK on success
 *  - ESP_FAIL on ERROR
 */
esp_err_t mqtt_subscribe(esp_mqtt_client_handle_t* client, const char* topic);

/**
 * @brief Init MQTT
 *        Initialize the MQTT message protocol submodules and start the activity
 * tasks
 *
 * @param client_name name which is registered at the MQTT broker
 * @param callbacks callbacks to register which are called on certain MQTT
 * events
 *
 * @return mqtt handle for topic subscription
 */
esp_mqtt_client_handle_t init_mqtt(
    char* client_name, mqtt_callbacks_t* callbacks);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_LEGOS_MQTT_H_
