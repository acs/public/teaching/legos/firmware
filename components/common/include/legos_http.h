//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <jonathan.klimt@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_HTTP_H_
#define _LEGOS_HTTP_H_

#include "esp_err.h"
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests on `http://<ip_addr>/post`
 *
 * @param[in] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
typedef esp_err_t (*http_post_callback_t)(char* str);

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests on
 * `http://<ip_addr>/status`
 *
 * @param[out] str Message buffer for the response
 *
 * @return
 *     - ESP_OK   Success
 */
typedef esp_err_t (*http_status_callback_t)(char* str);

typedef struct http_callbacks {
    http_post_callback_t http_post_callback;
    http_status_callback_t http_status_callback;
} http_callbacks;

/**
 * @brief Start HTTP server
 *
 * @return
 *  - ESP_OK : Server started successfully
 *  - ESP_FAIL : Server failed to start
 */
esp_err_t server_start();

/**
 * @brief Stop HTTP server
 *
 * @return
 *  - ESP_OK : Server stopped successfully
 *  - ESP_ERR_INVALID_ARG : Handle argument is Null
 */
esp_err_t server_stop();

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_LEGOS_HTTP_H_
