#include "driver/i2c.h"
#include "driver/gpio.h"

/**
 * @brief Scans the I2C bus and visualizes found devices
 *
 * @param[in] port I2C port to scan
 * @param[in] gpio_sda SDA pin nr.
 * @param[in] gpio_scl SCL pin nr.
 * @param[in] frequency I2C bus frequency
 */
void i2c_bus_scan(i2c_port_t port, gpio_num_t gpio_sda, gpio_num_t gpio_scl,
    uint32_t frequency);

