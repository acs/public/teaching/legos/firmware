//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <jonathan.klimt@eonerc.rwth-aachen.de>
//  License:    MIT

/**
 * Initialization functions for LEGOS boards
 */

#ifndef _LEGOS_INIT_H_
#define _LEGOS_INIT_H_

#include "driver/i2c.h"
#include "esp_err.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/**
 * @brief Init the I2C interface
 *
 * @param pin_sda SDA pin number
 * @param pin_scl SCL pin number
 * @param i2c_port_num I2C port number
 * @param i2c_freq I2C frequency in Hz
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
esp_err_t init_i2c(unsigned pin_sda, unsigned pin_scl, i2c_port_t i2c_port_num,
    unsigned i2c_freq);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif // _LEGOS_INIT_H_
