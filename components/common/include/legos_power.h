//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <jonathan.klimt@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_POWER_H_
#define _LEGOS_POWER_H_

#include "driver/i2c.h"
#include "esp_err.h"
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct power_monitor {
    uint8_t i2c_addr;
} power_monitor;

/**
 * @brief Power monitor readings
 */
typedef struct power_readings {
    float voltage; // Bus Voltage in mV
    float current; // Current in mA
    float power;   // Power in W
} power_readings;

/**
 * @brief  Query current power readings
 *
 * @param[in] pmon  The power monitor to read from
 *
 * @return
 *     - the power readings
 */
power_readings read_power(power_monitor* pmon);

/**
 * @brief  Configure power sensor calibration
 *
 * @param[in] pmon  The power monitor to configure
 * @param[in] R_sh  Shunt resistance
 * @param[in] I_max Max expected current
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t set_calibration(power_monitor* pmon, float r_shunt, float i_max);

/**
 * @brief  Init INA233 power monitor
 *
 * @param[in] v_i2c_num I2C port
 * @param[in] addr I2C device address
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_FAIL Device init error
 */
power_monitor pwr_monitor_init(i2c_port_t v_i2c_num, uint8_t addr);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_LEGOS_POWER_H_
