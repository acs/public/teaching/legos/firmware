//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_LIBS_H_
#define _LEGOS_LIBS_H_

/**
 * @brief Library inlcudes
 *        Do not directly edit the entry below, run idf.py menuconfig
 *        instead and select the build target from the LEGOS menu
 */

#ifdef CONFIG_BRANCH
#include "branch.h"
#elif defined CONFIG_DATACENTER
#include "data_center.h"
#elif defined CONFIG_ECSTATION
#include "ec_station.h"
#elif defined CONFIG_FACTORY
#include "factory.h"
#elif defined CONFIG_HOSPITAL
#include "hospital.h"
#elif defined CONFIG_HOUSE
#include "house.h"
#elif defined CONFIG_POWERPLANT
#include "power_plant.h"
#elif defined CONFIG_SKYSCRAPER
#include "skyscraper.h"
#elif defined CONFIG_SOLARFARM
#include "solar_farm.h"
#elif defined CONFIG_STADIUM
#include "stadium.h"
#elif defined CONFIG_SUBSTATION
#include "substation.h"
#elif defined CONFIG_SUPERMARKET
#include "supermarket.h"
#elif defined CONFIG_WINDFARM
#include "wind_farm.h"
#elif defined CONFIG_TEST
#include "test.h"
#endif

#endif //_LEGOS_LIBS_H_
