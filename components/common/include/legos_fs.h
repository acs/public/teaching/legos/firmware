
//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <jonathan.klimt@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_FS_H_
#define _LEGOS_FS_H_

#include "esp_err.h"

#ifdef __cplusplus
extern "C" {
#endif

esp_err_t init_flash(void);

esp_err_t init_spiffs(void);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_LEGOS_FS_H_
