//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <jonathan.klimt@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_ID_H_
#define _LEGOS_ID_H_

#include "driver/gpio.h"
#include "esp_err.h"
#include "owb.h"

#include "inttypes.h"
#ifdef __cplusplus
extern "C" {
#endif

typedef owb_rmt_driver_info id_bus;

/**
 * @brief Initializes the OWB bus to read the ROM chips
 *
 * @param owb The bus object to initialize
 * @param gpio_num Pin of the Bus
 */
void init_id_bus(id_bus* bus, gpio_num_t gpio_num);

/**
 * @brief Reads the ID of the first object on the bus and returns it
 *
 * @param The bus object to read the IDs from
 *
 * @return The ID
 */
uint64_t read_id(id_bus* bus);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_LEGOS_ID_H_
