# Common
This component contains definitions and methods shared by all the entities. It is composed of several sub-modules:
- legos_common: platform configuration and shared definitions/methods
- legos_libs: compilation target selection
- legos_mqtt: MQTT message protocol stack and event handler
- legos_wifi: WiFi network protocol stack and event handler

## [LIB] legos_common
### Access configuration
```c
// WiFi Settings
#define WIFI_STA_SSID "LEGOS"
#define WIFI_STA_PASS "legos#wifi"

// MQTT Settings
#define MQTT_USER "LEGOS"
#define MQTT_PASS "legos#mqtt"
#define MQTT_HOST "mqtt://192.168.137.1"
#define MQTT_PORT 1883
#define MQTT_RATE 1
```

## [LIB] legos_libs
### Compilation target selection
```c
/**
 * @brief Compilation target
 *        Select the proper compilation target by replacing
 *        ENTITY with the correct entity name
 */
#define ENTITY
```

## [LIB] legos_mqtt
### MQTT Client Init
```c
esp_err_t init_mqtt()
{
    ...

    esp_mqtt_client_config_t mqtt_cfg = {
        .event_handle = mqtt_event_handler,
        .uri = MQTT_HOST,
        .port = MQTT_PORT,
        // .client_id = MQTT_ID,
        .username = MQTT_USER,
        .password = MQTT_PASS
    };

    xTaskCreate( mqtt_publisher_task,  "mqtt_publisher_task", 2048, NULL, 4, &mqtt_publisher_handle);
    xTaskCreate(mqtt_subscriber_task, "mqtt_subscriber_task", 2048, NULL, 4, &mqtt_subscriber_handle);

    ...
}
```
### MQTT Client Event FSM
```c
esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    switch (event->event_id) {
    case MQTT_EVENT_ANY:
        ...
    case MQTT_EVENT_CONNECTED:
        ...
    case MQTT_EVENT_DISCONNECTED:
        ...
    case MQTT_EVENT_SUBSCRIBED:
        ...
    case MQTT_EVENT_UNSUBSCRIBED:
        ...
    case MQTT_EVENT_PUBLISHED:
        ...
    case MQTT_EVENT_DATA:
        ...
    case MQTT_EVENT_BEFORE_CONNECT:
        ...
    case MQTT_EVENT_ERROR:
        ...
	}
    ...
}
```
### MQTT Callbacks
- **mqtt_init()**: invoked in the FSM when `MQTT_EVENT_CONNECTED`
- **mqtt_read()**: invoked in `mqtt_subscriber_task` by the FSM when `MQTT_EVENT_DATA`
- **mqtt_post()**: invoked in `mqtt_publisher_task` by `mqtt_timer_isr` every `mqtt_timer_alarm`

## [LIB] legos_wifi
### WiFi STA Init
```c
esp_err_t init_STA()
{
    ...

    wifi_sta_config_t sta = {
        .ssid = WIFI_STA_SSID,
        .password = WIFI_STA_PASS
    };
    wifi_config.sta = sta;

    ...

    return esp_wifi_start();
}
```
### WiFi Client Event FSM
```c
void network_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT) {
        switch (event_id) {
        case WIFI_EVENT_STA_START:
            ...
        case WIFI_EVENT_STA_CONNECTED:
            ...
        case WIFI_EVENT_STA_DISCONNECTED:
            ...
		}
	}
    ...
}
```
### HTTP Callbacks
- **http_read()**: to be defined
- **http_resp()**: to be defined