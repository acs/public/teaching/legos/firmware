//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

//  This library is partially based on:
//      infinityPV INA233 Arduino Library
//  Copyright:  2018, infinityPV ApS
//  Author:     rava (infinityPV ApS)
//  License:    BSD

#ifndef _INA233_H_
#define _INA233_H_

#include "driver/i2c.h"

// Datasheet    https://www.ti.com/lit/ds/symlink/ina233.pdf

// I2C Addresses
#define INA233_ADDRESS_40 (0x40) // 1000000 (A1+A0=GND)
#define INA233_ADDRESS_41 (0x41) // 1000001 (A1=GND, A0=VDD)
#define INA233_ADDRESS_42 (0x42) // 1000010 (A1=GND, A0=SDA)
#define INA233_ADDRESS_43 (0x43) // 1000011 (A1=GND, A0=SCL)
#define INA233_ADDRESS_44 (0x44) // 1000100 (A1=VDD, A0=GND)
#define INA233_ADDRESS_45 (0x45) // 1000101 (A1+A0=VDD)
#define INA233_ADDRESS_46 (0x46) // 1000110 (A1=VDD, A0=SDA)
#define INA233_ADDRESS_47 (0x47) // 1000111 (A1=VDD, A0=SCL)
#define INA233_ADDRESS_48 (0x48) // 1001000 (A1=SDA, A0=GND)
#define INA233_ADDRESS_49 (0x49) // 1001001 (A1=SDA, A0=VDD)
#define INA233_ADDRESS_4A (0x4A) // 1001010 (A1+A0=SDA)
#define INA233_ADDRESS_4B (0x4B) // 1001011 (A1=SDA, A0=SCL)
#define INA233_ADDRESS_4C (0x4C) // 1001100 (A1=SCL, A0=GND)
#define INA233_ADDRESS_4D (0x4D) // 1001101 (A1=SCL, A0=VDD)
#define INA233_ADDRESS_4E (0x4E) // 1001110 (A1=SCL, A0=SDA)
#define INA233_ADDRESS_4F (0x4F) // 1001111 (A1+A0=SCL)

// Device Registers
//      Name                Code    Function R/W     Size    Default
#define CLEAR_FAULTS                                                           \
    (0x03) // Clears the status registers and rearms the black box registers for
           // udpating      SB      0       N/A
#define RESTORE_DEFAULT_ALL                                                    \
    (0x12) // Restores internal registers to the default values
           // SB      0       N/A
#define CAPABILITY                                                             \
    (0x19) // Retrieves the device capability R       1       0xB0
#define IOUT_OC_WARN_LIMIT                                                     \
    (0x4A) // Retrieves or stores the output overcurrent warn limit threshold
           // R/W     2       0x7FF8
#define VIN_OV_WARN_LIMIT                                                      \
    (0x57) // Retrieves or stores the input overvoltage warn limit threshold R/W
           // 2       0x7FF8
#define VIN_UV_WARN_LIMIT                                                      \
    (0x58) // Retrieves or stores the input undervoltage warn limit threshold
           // R/W     2       0x0000
#define PIN_OP_WARN_LIMIT                                                      \
    (0x6B) // Retrieves or stores the output overpower warn limit threshold R/W
           // 2       0x7FF8
#define STATUS_BYTE                                                            \
    (0x78) // Retrieves information about the device operating status R       1
           // 0x00
#define STATUS_WORD                                                            \
    (0x79) // Retrieves information about the device operating status R       2
           // 0x1000
#define STATUS_IOUT                                                            \
    (0x7B) // Retrieves information about the output current status R/W,CLR 1
           // 0x00
#define STATUS_INPUT                                                           \
    (0x7C) // Retrieves information about the input status R/W,CLR 1 0x00
#define STATUS_CML                                                             \
    (0x7E) // Retrieves information about the communications status R/W,CLR 1
           // 0x00
#define STATUS_MFR_SPECIFIC                                                    \
    (0x80) // Retrieves information about the manufacturer specific device
           // status              R/W,CLR 1       0x20
#define READ_EIN                                                               \
    (0x86) // Retrieves the energy reading measurement
           // Block_R 6       0x00
#define READ_VIN                                                               \
    (0x88) // Retrieves the measurement for the VBUS voltage
           // R       2       0x0000
#define READ_IIN                                                               \
    (0x89) // Retrieves the input current measurement, supports both + and -
           // currents          R       2       0x0000
#define READ_VOUT (0x8B) // Mirrors READ_VIN R       2       0x0000
#define READ_IOUT                                                              \
    (0x8C) // Mirrors READ_IIN for compatibility
           // R       2       0x0000
#define READ_POUT                                                              \
    (0x96) // Mirrors READ_PIN for compatibility with possible VBUS connections
           // R       2       0x0000
#define READ_PIN                                                               \
    (0x97) // Retrieves the input power measurement
           // R       2       0x0000
#define MFR_ID                                                                 \
    (0x99) // Retrieves the manufacturer ID in ASCII Characters (TI)
           // Block_R 2       0x54, 0x49
#define MFR_MODEL                                                              \
    (0x9A) // Retrieves the device number in ASCII Characters (INA233)
           // Block_R 6       0x49, 0x4E, 0x41, 0x32, 0x33, 0x33
#define MFR_REVISION                                                           \
    (0x9B) // Retrieves the device revision letter and number in ASCII (e.g.A0)
           // R       2       0x41,0x30
#define MFR_ADC_CONFIG                                                         \
    (0xD0) // Configures the ADC averaging modes, conversion times, and opr.
           // modes             R/W     2       0x4127
#define MFR_READ_VSHUNT                                                        \
    (0xD1) // Retrieves the shunt voltage measurement
           // R       2       0x0000
#define MFR_ALERT_MASK                                                         \
    (0xD2) // Allows masking of device warnings
           // R/W     1       0xF0
#define MFR_CALIBRATION                                                        \
    (0xD4) // Allows the value of the current-sense resistor calibration value
           // to be input. Must be programed at power-up. Default value is set
           // to 1. 		                R/W     2       0x0001
#define MFR_DEVICE_CONFIG                                                      \
    (0xD5) // Allows the ALERT pin polarity to be changed
           // R/W     1       0x02
#define CLEAR_EIN                                                              \
    (0xD6) // Clears the energy accumulator
           // S_B     0       N/A
#define TI_MFR_ID                                                              \
    (0xE0) // Returns a unique word for the manufacturer ID
           // R       2       0x5449 (TI in ASCII)
#define TI_MFR_MODEL                                                           \
    (0xE1) // Returns a unique word for the manufacturer model
           // R       2       33 in ASCII
#define TI_MFR_REVISION                                                        \
    (0xE2) // Returns a unique word for the manufacturer revision
           // R       2       A0 in ASCII

// I2C definitions
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define ACK_CHECK_EN 0x1            // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0           // I2C master will not check ack from slave
#define ACK_VAL 0x0                 // I2C ack value
#define NACK_VAL 0x1                // I2C nack value
#define I2C_MASTER_TX_BUF_DISABLE 0 // I2C master do not need buffer
#define I2C_MASTER_RX_BUF_DISABLE 0 // I2C master do not need buffer
#define I2C_MASTER_FREQ_HZ 400000   // I2C master clock frequency

/**
 * @brief PMBus DIRECT Data Format Coefficients
 */
typedef struct {
    int16_t m;
    int8_t R;
    int8_t b;
} pmbus_data_t;

#ifdef __cplusplus
extern "C" {
#endif

float ina233_getBusVoltage_mV(uint8_t addr);
float ina233_getShuntVoltage_mV(uint8_t addr);
float ina233_getCurrent_mA(uint8_t addr);
float ina233_getPower_mW(uint8_t addr);
float ina233_getAv_Power_mW(uint8_t addr);

esp_err_t ina233_setCalibration(uint8_t addr, float r_shunt, float i_max);
void ina233_readInfo(uint8_t addr, char* model_ext);
esp_err_t ina233_init(i2c_port_t v_i2c_num, uint8_t addr);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_INA233_H_
