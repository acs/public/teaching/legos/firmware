//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

//  This library is partially based on:
//      https://codebender.cc/example/INA209/INA209_read_write_registers#INA209_read_write_registers.ino
//  Copyright:  2018, infinityPV ApS
//  Author:     rava (infinityPV ApS)
//  License:    BSD

#include "esp_log.h"
#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/gpio.h"
#include "driver/i2c.h"

#include "ina209.h"
#include <math.h>
#include <string.h>

// Device Registers

// This register resets all registers and controls shunt voltage and
//  bus voltage, ADC conversion times and averaging, as well as the
//  device operating mode.
#define INA209_CONFIG (0x00) // R/~W
// Status flags for warnings, over-/under-limits, conversion ready,
//  math overflow, and SMBus Alert.
#define INA209_STATUS (0x01) // R
// Status flags for warnings, over-/under-limits, conversion ready,
//  math overflow, and SMBus Alert.
#define INA209_SMBUS_ALERT_MASK (0x02) // R/~W
// Shunt voltage measurement data
#define INA209_SHUNT_V (0x03) // R
// Bus voltage measurement data
#define INA209_BUS_V (0x04) // R
// contains the value of the calculated power being delivered to the load
#define INA209_POWER (0x05) // R
// contains the value of the calculated current flowing through the shunt
// resistor
#define INA209_CURRENT (0x06) // R

#define INA209_SHUNT_V_NEG_PEAK (0x07) // R
#define INA209_SHUNT_V_POS_PEAK (0x08) // R
#define INA209_BUS_V_NEG_PEAK (0x09)   // R
#define INA209_BUS_V_POS_PEAK (0x0A)   // R
#define INA209_POWER_PEAK (0x0B)       // R
#define INA209_SHUNT_V_POS_WARN (0x0C) // R
#define INA209_SHUNT_V_NEG_WARN (0x0D) // R
#define INA209_POWER_WARN (0x0E)       // R
#define INA209_BUS_OV_WARN (0x0F)      // R
#define INA209_BUS_UV_WARN (0x10)      // R
#define INA209_POWER_OV (0x11)         // R
#define INA209_BUS_OV_LIMIT (0x12)     // R
#define INA209_BUS_UV_LIMIT (0x13)     // R
#define INA209_CRIT_DAC_POS (0x14)     // R
#define INA209_CRIT_DAC_NEG (0x15)     // R
#define INA209_CALIBRATION (0x16)      // R

// I2C definitions
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define ACK_CHECK_EN 0x1  // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0 // I2C master will not check ack from slave
#define ACK_VAL 0x0       // I2C ack value
#define NACK_VAL 0x1      // I2C nack value

static const char* TAG = "INA209";

static i2c_port_t i2c_num = I2C_NUM_1;
static uint8_t i2c_address = 0;

float current_lsb = -1;

uint16_t current_cal = 0;

double V_shunt_max = 0.32;

/**
 * @brief  Write to I2C address
 *         Send N bytes from data to register reg
 *
 * @param[in]  reg   Register address
 * @param[in]  data  Data buffer
 * @param[in]  N     Data size in bytes
 *
 * @return void
 */
static void i2c_write(uint8_t reg, uint8_t* data, uint8_t size)
{
    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (i2c_address << 1) | I2C_MASTER_WRITE, ACK_CHECK_DIS));
    ESP_ERROR_CHECK(i2c_master_write(cmd, &reg, 1, ACK_CHECK_DIS));
    for (i = 0; i < size; i++)
        ESP_ERROR_CHECK(
            i2c_master_write(cmd, &data[size - i - 1], 1, ACK_CHECK_DIS));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);

    vTaskDelay(1 / portTICK_PERIOD_MS);
}

/**
 * @brief  Read from I2C address
 *         Read N bytes in data from register reg
 *
 * @param[in]  reg   Register address
 * @param[out] data  Data buffer
 * @param[in]  size     Data size in bytes
 *
 * @return void
 */
static void i2c_read(uint8_t reg, uint8_t* data, uint8_t size)
{
    // Send register to read from
    i2c_write(reg, NULL, 0);

    uint8_t i, block_size;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (i2c_address << 1) | I2C_MASTER_READ, ACK_CHECK_DIS));
    if (size > 2)
        ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &block_size, ACK_VAL));
    for (i = 0; i < size - 1; i++)
        ESP_ERROR_CHECK(
            i2c_master_read_byte(cmd, &data[size - i - 1], ACK_VAL));
    ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[size - i - 1], NACK_VAL));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);
}

/**
 * @brief  Get INA209 Shunt Voltage (mV)
 *
 * @param[in] addr I2C device address
 *
 * @return vshunt
 */
float ina209_getShuntVoltage_mV(uint8_t addr)
{
    i2c_address = addr;
    int16_t value = 0;
    i2c_read(INA209_SHUNT_V, (uint8_t*)&value, 2);

    float vshunt = ((float)value) * 0.01;
    return vshunt;
}

/**
 * @brief  Get INA209 Bus Voltage (mV)
 *
 * @param[in] addr I2C device address
 *
 * @return vbus
 */
float ina209_getBusVoltage_mV(uint8_t addr)
{
    i2c_address = addr;
    int16_t value = 0;
    i2c_read(INA209_BUS_V, (uint8_t*)&value, 2);

    float vbus = ((float)value) * 0.5;
    return vbus;
}

/**
 * @brief  Get INA209 Current (mA)
 *
 * @param[in] addr I2C device address
 *
 * @return current
 */
float ina209_getCurrent_mA(uint8_t addr)
{
    i2c_address = addr;
    int16_t value = 0;
    i2c_read(INA209_CURRENT, (uint8_t*)&value, 2);

    float current_mA = ((float)value) * current_lsb * 1000;
    return current_mA;
}

/**
 * @brief  Get INA209 Power (mW)
 *
 * @param[in] addr I2C device address
 *
 * @return power
 */
float ina209_getPower_mW(uint8_t addr)
{
    i2c_address = addr;
    int16_t value = 0;
    i2c_read(INA209_POWER, (uint8_t*)&value, 2);

    float power = (float)value * current_lsb * 20.0;

    return power * 1.0;
}

/**
 * @brief  Set INA209 Calibration Registers
 *         Calculate PMBus DIRECT Data Format Coefficients
 *
 * @param[in] addr  I2C device address
 * @param[in] R_sh  Shunt resistance (R)
 * @param[in] I_max Max expected current (A)
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t ina209_setCalibration(uint8_t addr, float R_sh, float I_max)
{
    i2c_address = addr;

    // Datasheet:
    // 1) Parameter: VBUSMAX = 32 ; VSHUNTMAX = 0.32 ; RSHUNT = R_sh
    // double V_bus_max = 32;

    // Take absolute value
    R_sh = fabs(R_sh);
    I_max = fabs(I_max);

    double I_real_max = V_shunt_max / R_sh;
    if (I_real_max < I_max) {
        ESP_LOGE(TAG,
            "Calibration I_Max out-of-range, increase R_sh or decrease I_max");
        return ESP_FAIL;
    }
    double C_LSB = ceil((I_max / 32767) * pow(10, 6))
        / pow(10, 6); // Maybe round?? Wie geht das lol
    uint32_t CAL = (uint32_t)(0.04096 / (R_sh * C_LSB));
    current_cal = CAL;
    current_lsb = (float)C_LSB;

    // Check CAL is in the uint16 range
    if (CAL > 0xFFFF) {
        ESP_LOGE(TAG, "Calibration out-of-range, increase R_sh or I_max");
        return ESP_FAIL;
    }

    // Write calibration value
    i2c_write(INA209_CALIBRATION, (uint8_t*)&CAL, 2);

    return ESP_OK;
}

/**
 * @brief  Init INA209 power monitor
 *
 * @param[in] v_i2c_num I2C port
 * @param[in] addr I2C device address
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_FAIL Device init error
 */
esp_err_t ina209_init(i2c_port_t v_i2c_num, uint8_t addr)
{
    i2c_address = addr;

    ESP_LOGD(TAG, "Adresse %d", addr);
    uint8_t config[2];

    // Check if Chip is present
    i2c_read(INA209_CONFIG, config, 2);
    if (config[0] == 0x00 || config[0] == 0xFF || config[1] == 0x00
        || config[1] == 0xFF) {
        return ESP_FAIL;
    }

    /*
     * Bus Voltage Range = 32V
     * PGA (Shunt Voltage Only) = +/- 320mV
     * BADC Bus ADC Resolution/Averaging = 12bit (0011) / 32 avg (1101)
     * SADC Shunt ADC Resolution/Averaging = 12bit / 32 avg
     * Operation Mode = Shunt and Bus, Continuous
     */
    config[1] = 0b00111001;
    config[0] = 0b10011111;

    ESP_LOGD(
        TAG, "INA209 new Config register value: %x %x", config[1], config[0]);
    i2c_write(0x00, config, 2);
    return ESP_OK;
}
