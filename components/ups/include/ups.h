//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <jonathan.klimt@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _UPS_H_
#define _UPS_H_

#include <inttypes.h>

/**
 * @brief UPS - Uninterruptible power supply.
 * Simulates a UPS system (basically a battery). The functions charge() and
 * discharge() can be called periodically on this struct to simulate charging
 * and discharging.
 */
typedef struct ups_t {
    float charge_level;   // Charge level in percent (0-100)
    float charge_rate;    // Charge rate in percent per second
    float discharge_rate; // Discharge rate in percent per second
} ups;

/**
 * @brief enum with the possible power sources for a LEGOS entity
 */
typedef enum { GRID, UPS, NONE } power_source_t;

/**
 * @brief Function to be called periodically to simulate the charging of a
 * battery
 *
 * @param u ups to charge
 * @param delta_time_ms The time difference between invocations in ms.
 */
void charge(ups* u, uint16_t delta_time_ms);

/**
 * @brief Function to be called periodically to simulate the discharging of a
 * battery
 *
 * @param u Ups to discharge
 * @param delta_time_ms The time difference between invocations in ms.
 */
void discharge(ups* u, uint16_t delta_time_ms);

char* print_power_source(power_source_t pwr_source);

#endif //_UPS_H_
