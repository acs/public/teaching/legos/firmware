//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <jonathan.klimt@eonerc.rwth-aachen.de>
//  License:    MIT

#include <inttypes.h>
#include <stddef.h>
#include <ups.h>

// TODO: simulate more a more realistic battery curve

void charge(ups* u, uint16_t delta_time_ms)
{
    u->charge_level += (delta_time_ms / 1000.0) * u->charge_rate;
    if (u->charge_level > 100.0) {
        u->charge_level = 100.0;
    }
}

void discharge(ups* u, uint16_t delta_time_ms)
{
    u->charge_level -= (delta_time_ms / 1000.0) * u->discharge_rate;
    if (u->charge_level < 0.0) {
        u->charge_level = 0.0;
    }
}

char* grid_str = "grid";
char* ups_str = "ups";
char* none_str = "none";

char* print_power_source(power_source_t pwr_source)
{
    switch (pwr_source) {
    case GRID:
        return grid_str;
    case UPS:
        return ups_str;
    case NONE:
        return none_str;
    }
    // unreachable
    return NULL;
}
