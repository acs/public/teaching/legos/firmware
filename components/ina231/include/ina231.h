//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

//  This library is partially based on:
//      infinityPV INA231 Arduino Library
//  Copyright:  2018, infinityPV ApS
//  Author:     rava (infinityPV ApS)
//  License:    BSD

#ifndef _INA231_H_
#define _INA231_H_

#include "driver/i2c.h"

// Datasheet    https://www.ti.com/lit/ds/symlink/ina231.pdf

// I2C Addresses
//#define INA231_ADDRESS_40 (0x40) // 1000000 (A1+A0=GND)
//#define INA231_ADDRESS_41 (0x41) // 1000001 (A1=GND, A0=VDD)
//#define INA231_ADDRESS_42 (0x42) // 1000010 (A1=GND, A0=SDA)
//#define INA231_ADDRESS_43 (0x43) // 1000011 (A1=GND, A0=SCL)
//#define INA231_ADDRESS_44 (0x44) // 1000100 (A1=VDD, A0=GND)
//#define INA231_ADDRESS_45 (0x45) // 1000101 (A1+A0=VDD)
//#define INA231_ADDRESS_46 (0x46) // 1000110 (A1=VDD, A0=SDA)
//#define INA231_ADDRESS_47 (0x47) // 1000111 (A1=VDD, A0=SCL)
//#define INA231_ADDRESS_48 (0x48) // 1001000 (A1=SDA, A0=GND)
//#define INA231_ADDRESS_49 (0x49) // 1001001 (A1=SDA, A0=VDD)
//#define INA231_ADDRESS_4A (0x4A) // 1001010 (A1+A0=SDA)
//#define INA231_ADDRESS_4B (0x4B) // 1001011 (A1=SDA, A0=SCL)
//#define INA231_ADDRESS_4C (0x4C) // 1001100 (A1=SCL, A0=GND)
//#define INA231_ADDRESS_4D (0x4D) // 1001101 (A1=SCL, A0=VDD)
//#define INA231_ADDRESS_4E (0x4E) // 1001110 (A1=SCL, A0=SDA)
//#define INA231_ADDRESS_4F (0x4F) // 1001111 (A1+A0=SCL)

#define I2C_MASTER_TX_BUF_DISABLE 0 // I2C master do not need buffer
#define I2C_MASTER_RX_BUF_DISABLE 0 // I2C master do not need buffer
#define I2C_MASTER_FREQ_HZ 400000   // I2C master clock frequency

#ifdef __cplusplus
extern "C" {
#endif

float ina231_getBusVoltage_mV(uint8_t addr);
float ina231_getShuntVoltage_mV(uint8_t addr);
float ina231_getCurrent_mA(uint8_t addr);
float ina231_getPower_mW(uint8_t addr);

esp_err_t ina231_setCalibration(uint8_t addr, float R_sh, float I_max);
esp_err_t ina231_init(i2c_port_t v_i2c_num, uint8_t addr);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_INA231_H_
