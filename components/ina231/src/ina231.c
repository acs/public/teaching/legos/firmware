//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

//  This library is partially based on:
//      infinityPV INA231 Arduino Library
//  Copyright:  2018, infinityPV ApS
//  Author:     rava (infinityPV ApS)
//  License:    BSD

#include "esp_log.h"
#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/gpio.h"
#include "driver/i2c.h"

#include "ina231.h"
#include <math.h>
#include <string.h>

// Device Registers

// This register resets all registers and controls shunt voltage and
//  bus voltage, ADC conversion times and averaging, as well as the
//  device operating mode.
#define INA231_CONFIG (0x00) // R/~W
// Shunt voltage measurement data
#define INA231_SHUNT_V (0x01) // R
// Bus voltage measurement data
#define INA231_BUS_V (0x02) // R
// contains the value of the calculated power being delivered to the load
#define INA231_POWER (0x03) // R
// contains the value of the calculated current flowing through the shunt
// resistor
#define INA231_CURRENT (0x04) // R
// sets the full-scale range and LSB of the current and power measurements. This
// register sets the overall system calibration
#define INA231_CALIBRATION (0x05) // R/~W
// sets the alert configuration and conversion ready flag
#define INA231_MASK_ENABLE (0x06) // R/~W
// This register contains the limit value to compare to the selected alert
// function.
#define INA231_ALERT_LIMIT (0x07) // R/~W

// I2C definitions
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define ACK_CHECK_EN 0x1  // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0 // I2C master will not check ack from slave
#define ACK_VAL 0x0       // I2C ack value
#define NACK_VAL 0x1      // I2C nack value

static const char* TAG = "INA231";

static i2c_port_t i2c_num = I2C_NUM_1;
static uint8_t i2c_address = 0;

float current_lsb = -1;

/**
 * @brief  Write to I2C address
 *         Send N bytes from data to register reg
 *
 * @param[in]  reg   Register address
 * @param[in]  data  Data buffer
 * @param[in]  N     Data size in bytes
 *
 * @return void
 */
static void i2c_write(uint8_t reg, uint8_t* data, uint8_t N)
{
    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (i2c_address << 1) | I2C_MASTER_WRITE, ACK_CHECK_DIS));
    ESP_ERROR_CHECK(i2c_master_write(cmd, &reg, 1, ACK_CHECK_DIS));
    for (i = 0; i < N; i++)
        ESP_ERROR_CHECK(i2c_master_write(cmd, &data[i], 1, ACK_CHECK_DIS));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);

    vTaskDelay(1 / portTICK_PERIOD_MS);
}

/**
 * @brief  Read from I2C address
 *         Read N bytes in data from register reg
 *
 * @param[in]  reg   Register address
 * @param[out] data  Data buffer
 * @param[in]  N     Data size in bytes
 *
 * @return void
 */
static void i2c_read(uint8_t reg, uint8_t* data, uint8_t size)
{
    // Send register to read from
    i2c_write(reg, NULL, 0);

    uint8_t i, block_size;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(
        cmd, (i2c_address << 1) | I2C_MASTER_READ, ACK_CHECK_DIS));
    if (size > 2)
        ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &block_size, ACK_VAL));
    for (i = 0; i < size - 1; i++)
        ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], ACK_VAL));
    ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], NACK_VAL));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(
        i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);
}

/**
 * @brief  Get INA231 Bus Voltage (mV)
 *
 * @param[in] addr I2C device address
 *
 * @return vbus
 */
float ina231_getBusVoltage_mV(uint8_t addr)
{
    i2c_address = addr;
    uint8_t value_raw[2] = { 0, 0 };
    i2c_read(INA231_BUS_V, value_raw, 2);
    int16_t value = (uint16_t)value_raw[0] << 8 | (uint16_t)value_raw[1];
    float vbus = (float)value * 0.00125;
    return vbus;
}

/**
 * @brief  Get INA231 Shunt Voltage (mV)
 *
 * @param[in] addr I2C device address
 *
 * @return vshunt
 */
float ina231_getShuntVoltage_mV(uint8_t addr)
{
    i2c_address = addr;
    int16_t value = 0;
    i2c_read(INA231_SHUNT_V, (uint8_t*)&value, 2);
    float vshunt = (float)value * 0.0025;
    return vshunt;
}

/**
 * @brief  Get INA231 Current (mA)
 *
 * @param[in] addr I2C device address
 *
 * @return current
 */
float ina231_getCurrent_mA(uint8_t addr)
{
    i2c_address = addr;
    /*int16_t value = 0;*/
    uint8_t value_raw[2] = { 0, 0 };
    i2c_read(INA231_CURRENT, value_raw, 2);
    int16_t value = (uint16_t)value_raw[0] << 8 | (uint16_t)value_raw[1];
    float current_mA = (float)value * 0.1;
    return current_mA;
}

/**
 * @brief  Get INA231 Power (mW)
 *
 * @param[in] addr I2C device address
 *
 * @return power
 */
float ina231_getPower_mW(uint8_t addr)
{
    i2c_address = addr;
    int16_t value = 0;
    i2c_read(INA231_POWER, (uint8_t*)&value, 2);
    float power = (float)value * current_lsb * 25.0;

    return power * 1.0;
}

/**
 * @brief  Set INA231 Calibration Registers
 *         Calculate PMBus DIRECT Data Format Coefficients
 *
 * @param[in] addr  I2C device address
 * @param[in] R_sh  Shunt resistance (R)
 * @param[in] I_max Max expected current (A)
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t ina231_setCalibration(uint8_t addr, float R_sh, float I_max)
{
    i2c_address = addr;

    // Take absolute value
    R_sh = fabs(R_sh);
    I_max = fabs(I_max);
    ESP_LOGE(TAG, "asdf");

    // Datasheet P.15:
    // CAL = 0.00512 / (Current_LSB * R_shunt)
    // Current_LSB = I_max / (2^15)

    // Determine LSB and calibration value
    double C_LSB = I_max / pow(2, 15);
    uint32_t CAL = (uint16_t)(0.00512 / (R_sh * C_LSB));
    current_lsb = (float)C_LSB;
    ESP_LOGE(TAG, "CAL: %d", CAL);

    // Check CAL is in the uint16 range
    if (CAL > 0xFFFF) {
        ESP_LOGE(TAG, "Calibration out-of-range, increase R_sh or I_max");
        return ESP_FAIL;
    }

    // Write calibration value
    i2c_write(INA231_CALIBRATION, (uint8_t*)&CAL, 2);

    return ESP_OK;
}

/**
 * @brief  Init INA231 power monitor
 *
 * @param[in] v_i2c_num I2C port
 * @param[in] addr I2C device address
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_FAIL Device init error
 */
esp_err_t ina231_init(i2c_port_t v_i2c_num, uint8_t addr)
{
    i2c_address = addr;
    uint8_t config[2];

    i2c_read(INA231_CONFIG, config, 2);
    ESP_LOGD(TAG, "INA231 Config register value: %x %x", config[1], config[0]);
    if (config[0] == 0x00 || config[0] == 0xFF || config[1] == 0x00
        || config[1] == 0xFF) {
        return ESP_FAIL;
    }
    config[0] |= 0b00000110; // Average over 64 samples

    ESP_LOGD(
        TAG, "INA231 new Config register value: %x %x", config[1], config[0]);
    i2c_write(0x00, config, 2);
    return ESP_OK;
}
