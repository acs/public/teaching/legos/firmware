//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _MS4_H_
#define _MS4_H_

#include "driver/i2c.h"

// Datasheet
// https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1-1773935-7_AmbiMate_MS4_Series%7F1908%7Fpdf%7FEnglish%7FENG_DS_1-1773935-7_AmbiMate_MS4_Series_1908.pdf%7F2316852-1

// I2C Address
#define MS4_ADDRESS_2A (0x2A) // 00101010

// MS4 Options
#define MS4_OPT_CO2 " + CO2"
#define MS4_OPT_AUDIO " + AUDIO"

// Device Registers
//      Name            Code    Function                            R/W     Size
//      Default
#define MS4_REG_STATUS                                                         \
    (0x00) // Stores the pir status            R       1       N/A
#define MS4_REG_TEMP                                                           \
    (0x01) // Stores the temperature           R       2       0x00
#define MS4_REG_RH                                                             \
    (0x03) // Stores the humidity              R       2       N/A
#define MS4_REG_LIGHT                                                          \
    (0x05) // Stores the light                 R       2       N/A
#define MS4_REG_AUDIO                                                          \
    (0x07) // Stores the audio                 R       2       N/A
#define MS4_REG_BATT                                                           \
    (0x09) // Stores the battery level         R       2       N/A
#define MS4_REG_ECO2                                                           \
    (0x0B) // Stores the eCO2                  R       2       N/A
#define MS4_REG_VOC                                                            \
    (0x0D) // Stores the VOC                   R       2       N/A

#define MS4_REG_FW                                                             \
    (0x80) // Stores the firware version       R       1       N/A
#define MS4_REG_FWSUB                                                          \
    (0x81) // Stores the firmware sub-version  R       1       N/A
#define MS4_REG_OPT                                                            \
    (0x82) // Stores the optional sensors      R       1       N/A

#define MS4_REG_SCAN                                                           \
    (0xC0) // Init the conversion              R       1       N/A
#define MS4_REG_RESET                                                          \
    (0xF0) // Reset the processor              R       1       N/A

// MS4 Scan Configuration Bits
#define MS4_SCAN_STATUS (0) // Status
#define MS4_SCAN_TEMP (1)   // Temperature
#define MS4_SCAN_RH (2)     // Humidity
#define MS4_SCAN_LIGHT (3)  // Light
#define MS4_SCAN_AUDIO (4)  // Audio
#define MS4_SCAN_BATT (5)   // Battery level
#define MS4_SCAN_GAS (6)    // Gas

// MS4 Events
#define MS4_EVENT_PIR (0x80)    // PIR
#define MS4_EVENT_MOTION (0x01) // MOTION
#define MS4_EVENT_AUDIO (0x02)  // AUDIO

// I2C definitions
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define ACK_CHECK_EN 0x1            // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0           // I2C master will not check ack from slave
#define ACK_VAL 0x0                 // I2C ack value
#define NACK_VAL 0x1                // I2C nack value
#define I2C_MASTER_TX_BUF_DISABLE 0 // I2C master do not need buffer
#define I2C_MASTER_RX_BUF_DISABLE 0 // I2C master do not need buffer
#define I2C_MASTER_FREQ_HZ 100000   // I2C master clock frequency

/**
 * @brief Ambimate sensor readings
 */
typedef struct {
    uint8_t status; // MS4 event
    float temp; // 5 °C to + 50 °C + 1 °C Accuracy, 1 second acquisition rate
    float hum;  // 5% to 95% RH, 3% accuracy, 1 second acquisition rate
    uint16_t light; // 0-1024 Lux, 1 second acquisition rate
    float batt_v;
    uint16_t co2_ppm; // 400-8192ppm, 60 second acquisition rate, (an equivalent
                      // eCO2 measurement based on total VOC concentrations)
    uint16_t voc_ppb; // 0-1187ppb, 60 second acquisition rate
} ms4_t;

#ifdef __cplusplus
extern "C" {
#endif

void ms4_readAll();
esp_err_t ms4_init(i2c_port_t v_i2c_num, uint8_t addr, ms4_t* data);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_MS4_H_
