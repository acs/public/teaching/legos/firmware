//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Matthias Marcus Nowak <marcus.nowak@rwth-aachen.de>
//  License:    MIT

#ifndef _VIRTUAL_LOAD_H_
#define _VIRTUAL_LOAD_H_

#include "driver/dac.h"
#include "driver/gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief  Virtual load device handle. Should be created with virtual_load_init
 *
 * @param[in] load_v Maximum voltage across the virtual load and dac pin - 3V
 * @param[in] load_r Load Resistor Value
 * @param[in] load_max_a Software limited maximum current
 * @param[in] dac_channel DAC Channel used for virtual load output
 * @param[in] disable_pin Pin used for disable function
 */
typedef struct virtual_load_t {
    float load_v;              // Voltage across the load circuit
    float load_r;              // Load Resistor Value
    float load_max_a;          // Software limit for maximum current
    dac_channel_t dac_channel; // DAC Pin to set the Virtual Load
    gpio_num_t disable_pin; // Pin connected to the disable pin from the OpAmp
} virtual_load_t;

/**
 * @brief  Virtual load init
 *
 * @param[in] v_load_v Maximum voltage across the virtual load and dac pin - 3V
 * @param[in] v_load_r Load Resistor Value
 * @param[in] v_load_max_a Software limited maximum current
 * @param[in] v_dac_channel DAC Channel used for virtual load output
 * @param[in] v_disable_pin Pin used for disable function
 *
 * @return handle struct for the configured virtual load
 */
virtual_load_t virtual_load_init(float v_load_v, float v_load_r,
    float v_load_max_a, dac_channel_t v_dac_channel, gpio_num_t v_disable_pin);

/**
 * @brief  Set the load in watts
 *
 * @param[in] vl device handle
 * @param[in] v_watt Load watt value
 */
void virtual_load_set_watt(virtual_load_t* vl, float v_watt);

/**
 * @brief  Set the load in amps
 *
 * @param[in] vl device handle
 * @param[in] v_current Load amperage value
 */
void virtual_load_set_current(virtual_load_t* vl, float v_current);

/**
 * @brief  Set the load in total % - Unsafe, without current limiting
 *
 * @param[in] vl device handle
 * @param[in] c_percent Load percent Value
 */
void virtual_load_set_percent(virtual_load_t* vl, float v_percent);

/**
 * @brief  Disable the load in hardware
 *
 * @param[in] vl device handle
 * @param[in] v_disable true if load should be disabled, true for enabled
 */
void virtual_load_disable(virtual_load_t* vl, bool v_disable);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_VIRTUAL_LOAD_H_
