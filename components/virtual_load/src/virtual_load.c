//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Matthias Marcus Nowak <marcus.nowak@rwth-aachen.de>
//  License:    MIT

#include "esp_log.h"
#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/gpio.h"
#include "driver/i2c.h"

#include "virtual_load.h"
#include <math.h>
#include <string.h>

static const char* TAG = "VIRTUAL_LOAD";

virtual_load_t virtual_load_init(float v_load_v, float v_load_r,
    float v_load_max_a, dac_channel_t v_dac_channel, gpio_num_t v_disable_pin)
{
    // Readjust current limit if it was set too high
    if (v_load_max_a > (v_load_v / v_load_r))
        v_load_max_a = (v_load_v / v_load_r);

    virtual_load_t virtual_load = {
        .load_v = v_load_v,
        .load_r = v_load_r,
        .load_max_a = v_load_max_a,
        .dac_channel = v_dac_channel,
        .disable_pin = v_disable_pin,
    };

    gpio_set_direction(virtual_load.disable_pin, GPIO_MODE_OUTPUT);
    gpio_set_level(virtual_load.disable_pin, 0); // disable on startup

    dac_output_enable(virtual_load.dac_channel);
    dac_output_voltage(virtual_load.dac_channel, 0);

    return virtual_load;
}

void virtual_load_set_watt(virtual_load_t* vl, float v_watt)
{
    float resulting_current = v_watt / vl->load_v;

    virtual_load_set_current(vl, resulting_current);
}

void virtual_load_set_current(virtual_load_t* vl, float v_current)
{
    if (v_current > vl->load_max_a)
        v_current = vl->load_max_a;

    float dac_output_percent = (vl->load_r * v_current) / vl->load_v;

    virtual_load_set_percent(vl, dac_output_percent);
}

void virtual_load_set_percent(virtual_load_t* vl, float v_percent)
{ // this is uncontrolled by the max load current
    dac_output_voltage(vl->dac_channel, (int)(v_percent * 255));
}

void virtual_load_disable(virtual_load_t* vl, bool v_disable)
{
    gpio_set_level(vl->disable_pin, !v_disable);
}