# ![LEGOS](docs/legos_logo.png) <br/> Lite Emulator of Grid Operations

## Firmware
The operating code used in the [**LEGOS**](https://git.rwth-aachen.de/acs/public/teaching/legos/concept) platform for programming the building blocks of the [**Distribution layer**](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/distribution_layer.md) and the entities of the [**Application layer**](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/application_layer.md) was developed using the official Espressif IoT Development Framework [**ESP-IDF**](https://github.com/espressif/esp-idf) (version [*4.1*](https://github.com/espressif/esp-idf/releases/v4.1) release build).

### Documentation

A automatically generated docu can be found at:
https://acs.pages.rwth-aachen.de/public/teaching/legos/firmware/pages.html

### Architecture
```mermaid
flowchart TB
    subgraph main[main]
    start([Start]) --> init_entity[[Init Entity]]
    init_entity --> init_network[[Init Wi-Fi]]
    init_network --> init_mqtt[[Init MQTT]]
    init_mqtt --> loop((yield))
    end

    subgraph entity[entity];
    init_entity --> init_modules[[Init peripherals]];
    init_modules --> init_tasks((Main tasks));
    end

    subgraph network[wifi]
    init_network --> init_sta[[Init mode STA]]
    init_sta --> init_server[Start web server]
    init_server --> init_netfsm((Event FSM))
    end

    subgraph mqtt[mqtt]
    init_mqtt --> init_client[[Init MQTT client]]
    init_client --> sub_topic[Subscribe topics]
    sub_topic --> init_mqttfsm((Event FSM))
    end

    init_mqttfsm o-->|MQTT callbacks| init_tasks
    init_netfsm o-->|HTTP callbacks| init_tasks
```
The firmware architecture can be divided in 3 sub-blocks called in the main in the following order:

1. **entity**: application specific code
    - init entity peripherals
    - handle period tasks
    - provide mqtt callbacks
    - provide http callbacks
2. **wifi**: application independent code
    - init wifi STA mode
    - provide network stack
    - invoke http callbacks
3. **mqtt**: application independent code
    - init MQTT 
    - provide protocol stack
    - invoke mqtt callbacks

### Building
Select the desired compilation target from the LEGOS menu.
```
cd <project-dir>
idf.py menuconfig
```

Build and flash the code for the target entity. Replace port with the programmer port (eg. COM3)
```
idf.py -p <port> -b 921600 build flash
```

Open the debug terminal if required.
```
idf.py monitor
```

## Repository structure
All the entities in LEGOS share a common behavior as IoT devices, which is customized for the specific profile using libraries arranged in separate component folders:

- **components**
    - [common](components/common/common.md): library for implementing shared entity features
        - legos_common: shared settings and functions
        - legos_id:
        - legos_libs: custom build profile selection
        - legos_mqtt: mqtt thread finite state machine
        - legos_netw: wifi thread finite state machine
    - [branch](components/branch/branch.md): library for monitoring, controlling and visualizing the branch power flow
    - [datacenter](components/data_center/data_center.md): library for monitoring the power consumption, switching to ups upon fault and controlling the server activity
    - [ecstation](components/ec_station/ec_station.md): library for monitoring the power consumption and emulate electric-car charging profiles based on RFID data stored in the vehicle
    - [factory](components/factory/factory.md): library for monitoring the power consumption and controlling the factory electrical load due to manufacturing activities
    - [hospital](components/hospital/hospital.md): library for monitoring the power consumption, switching to ups upon fault and controlling the hospital alert status
    - [house](components/house/house.md): library for monitoring the power consumption, combining production from PVs and controlling household appliances
    - [powerplant](components/power_plant/power_plant.md): library for monitoring the power generation from traditional fossil-fuel
    - [skyscraper](components/skyscraper/skyscraper.md): library for monitoring the power consumption, controlling a HVAC system and implementing a SCADA/Cyber-Security center
    - [solarfarm](components/solar_farm/solar_farm.md): library for monitoring the power generation from renewable energy using solar cells, optimized via 1-axis solar tracker
    - [stadium](components/stadium/stadium.md): library for monitoring the power consumption due to occasional football matches
    - [substation](components/substation/substation.md): library for low-level automation of an electrical grid, provides bus trip/close operations upon fault
    - [supermarket](components/supermarket/supermarket.md): library for monitoring the power consumption, combining electrical and thermal energy generation from gas
    - [windfarm](components/wind_farm/wind_farm.md): library for monitoring the power generation from renewable energy using wind
    ---
    - [ms4](components/ms4/ms4.md): library for interfacing Ambimate Sensor Modules MS4 by TE Connectivity
    - [owb](components/owb/owb.md): library for interfacing devices over 1-Wire technology by Maxim Integrated
    - [ina233](components/ina233/ina233.md): library for interfacing INA233 power monitors by Texas Instruments
    - [mfrc522](components/mfrc522/mfrc522.md): library for interfacing RFID shields based on MFRC522 by NXP Semiconductors
    - [pca9536](components/pca9536/pca9536.md): library for interfacing PCA9536 I/O expanders by Texas Instruments
    - [tmp100](components/tmp100/tmp100.md): library for interfacing TMP10x temperature sensors by Texas Instruments

- **main**
    - *main.c*: entry point function for code execution

## Copyright

2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="LICENSE.md"><img alt="MIT License" style="border-width:0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/License_icon-mit-88x31-2.svg/88px-License_icon-mit-88x31-2.svg.png" /></a><br />This work is licensed under a <a rel="license" href="LICENSE.md">MIT License</a>.

## Funding
<a rel="funding" href="https://fismep.de/"><img alt="FISMEP" style="border-width:0" src="docs/fismep_logo.png" width="78" height="63"/></a><br />This work was supported by <a rel="fismep" href="https://fismep.de/">FIWARE for Smart Energy Platform</a> (FISMEP), a German project funded by the *Federal Ministry for Economic Affairs and Energy (BMWi)* and the *ERA-Net Smart Energy Systems* Programme under Grant 0350018A.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Dr. Carlo Guarnieri Calò Carducci](mailto:cguarnieri@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  

