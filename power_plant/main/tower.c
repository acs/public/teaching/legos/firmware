//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <klimt.jonathan@eonerc.rwth-aachen.de>
//  License:    MIT

#include "tower.h"
#include "esp_err.h"
#include "esp_log.h"
#include "pca9956.h"
#include <stdbool.h>

static const char* TAG = "PCA9956";

static void set_max_current(pca9956_t* pca, tower_leds* leds)
{
    for (int i = 0; i < 6; ++i) {
        pca9956_set_max_current(pca, leds->led_pins[i], leds->max_current);
    }
}

static void led_set_brightness(
    pca9956_t* pca, tower_leds* leds, uint8_t brightness)
{
    for (int i = 0; i < 6; ++i) {
        pca9956_single_switch(pca, leds->led_pins[i], brightness);
    }
}

void tower_set_brightness(
    plant_tower* tower, uint8_t brightness, led_color color)
{
    tower_leds* cur_tower;
    switch (color) {
    case GREEN:
        cur_tower = &tower->green_leds;
        break;
    case YELLOW:
        cur_tower = &tower->yellow_leds;
        break;
    case RED:
        cur_tower = &tower->red_leds;
        break;
    default:
        ESP_LOGE(TAG, "invalid tower color");
        assert(false);
    }
    led_set_brightness(&tower->pca, cur_tower, brightness);
}

plant_tower tower_init(pca9956_t pca)
{
    // Values from LED datasheet
    float brightness = 100.0; // mcda
    float i_red = (brightness / 140.0) * 20;
    float i_green = (brightness / 560.0) * 20;
    float i_yellow = (brightness / 120.0) * 20;

    tower_leds leds_green = { .max_current = i_green,
        .led_pins = {
            0,  // g2
            5,  // g4
            8,  // g3
            9,  // g5
            14, // g1
            17  // g0
        } };
    tower_leds leds_yellow = { .max_current = i_yellow,
        .led_pins = {
            1,  // y2
            4,  // y4
            7,  // y5
            11, // y3
            13, // y1
            16  // y0
        } };
    tower_leds leds_red = { .max_current = i_red,
        .led_pins = {
            2,  // r3
            3,  // r0
            6,  // r4
            10, // r2
            12, // r5
            15  // r1
        } };

    set_max_current(&pca, &leds_green);
    set_max_current(&pca, &leds_yellow);
    set_max_current(&pca, &leds_red);

    plant_tower tower = { leds_green, leds_yellow, leds_red, pca };

    return tower;
}
