#include "driver/adc.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "driver/ledc.h"
#include "driver/spi_master.h"
#include "driver/timer.h"
#include "driver/touch_pad.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "hal/gpio_types.h"
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_power.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "pca9956.h"
#include "sdkconfig.h"
#include "tower.h"
#include <stdint.h>

#define PIN_EEPROM GPIO_NUM_0 // 1 Wire EEPROM

#define PIN_TXD GPIO_NUM_1 // Data communication Programming
#define PIN_RXD GPIO_NUM_3

#define PIN_SDA GPIO_NUM_17 // I2C Pins
#define PIN_SCL GPIO_NUM_5

#define PIN_MOSI GPIO_NUM_4 // SPI Pins
#define PIN_CLK GPIO_NUM_21
#define PIN_CS_V GPIO_NUM_19 // CS Voltage Poti
#define PIN_CS_I GPIO_NUM_18 // CS Current Poti

#define SENS_TEMP ADC1_CHANNEL_6 // LDO Temperature
#define PIN_TURBO GPIO_NUM_2     // Alternator
#define IRQ_INA GPIO_NUM_16      // INA233 Interrupt

#define THRESHOLD_YELLOW_MW 1.50
#define THRESHOLD_RED_MW 2.50

// ---------- Touch Definitions ----------
#define PIN_TOUCH_S1 TOUCH_PAD_NUM3 // GPIO_NUM_15
#define PIN_TOUCH_S2 TOUCH_PAD_NUM5 // GPIO_NUM_12
#define PIN_TOUCH_S3 TOUCH_PAD_NUM6 // GPIO_NUM_14
#define PIN_TOUCH_S4 TOUCH_PAD_NUM7 // GPIO_NUM_27
#define PIN_TOUCH_S5 TOUCH_PAD_NUM8 // GPIO_NUM_33
#define PIN_TOUCH_S6 TOUCH_PAD_NUM9 // GPIO_NUM_32
#define PIN_TOUCH_M TOUCH_PAD_NUM4  // GPIO_NUM_13

#define TOUCH_THRESH_NO_USE (0)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

enum pwm_channels {
    PWM_MOTOR = 0, // GPIO_NUM_15
    PWM_MAX
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
// TODO: Protect with mutex
typedef struct Powerplant {
    uint64_t id;             // Unique port identifier
    uint8_t setpoint_vbus;   // Voltage output level
    uint8_t setpoint_imax;   // Current output level
    float ldo_temp;          // LDO temperature
    power_readings power_in; // Output telemetry
} power_plant_status;

static power_plant_status entity = { .id = 0,
    .setpoint_vbus = 190,
    .setpoint_imax = 127,
    .ldo_temp = 0.0,
    // TODO: this is Powerout
    .power_in = { 0.0, 0.0, 0.0 } };

typedef struct power_plant_control {
    power_monitor power_mon;
    id_bus id_bus;
    plant_tower tower;
    float power_averaged;
    int counter;
    float power_values[20];
    // TODO: GPIO
} power_plant_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/power_plant";
static const char mqtt_topic_sub[] = "legos/power_plant/cmd";

static const char* TAG = "PowerPlant"; // Logging tag

spi_device_handle_t AD8400_VBUS;  // SPI handle to Digital Pot
spi_device_handle_t AD8400_IMAX;  // SPI handle to Digital Pot
static uint8_t setpoint_vbus_old; // Voltage setpoint
static uint8_t setpoint_imax_old; // Current setpoint

static uint16_t entity_timer_ms = 100;        // Main-tasks Thread interval
static uint64_t entity_timer_alarm;           // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;       // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
    TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
    TIMER_DIVIDER };

static uint16_t touchpad_thresh = 600; // Touchpad sensitivity threshold
static uint16_t touchpad_offset[6];
static uint16_t touchpad_actual[6];
static touch_pad_t touch_slider[6] = { PIN_TOUCH_S1, PIN_TOUCH_S2, PIN_TOUCH_S3,
    PIN_TOUCH_S4, PIN_TOUCH_S5, PIN_TOUCH_S6 };

// LEDC channels configuration
ledc_channel_config_t pwm_channel[PWM_MAX] = {
    { .channel = 0,
        .duty = 0,
        .gpio_num = CONFIG_MOTOR_GPIO_PIN, // Altenator
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 5000,                      // PWM frequency
    .speed_mode = PWM_LS_MODE,            // PWM timer mode
    .timer_num = PWM_LS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

// SPI bus configuration
static spi_bus_config_t buscfg = {
    .miso_io_num = -1,       // -1 if not used
    .mosi_io_num = PIN_MOSI, // SPI MOSI
    .sclk_io_num = PIN_CLK,  // SPI CLK
    .quadwp_io_num = -1,     // -1 if not used
    .quadhd_io_num = -1      // -1 if not used
};

// SPI device configuration
spi_device_interface_config_t AD8400_VBUS_cfg = {
    .clock_speed_hz = 1000000, // SPI Clock out at 1 MHz
    .mode = 0,                 // SPI Mode 0
    .spics_io_num = PIN_CS_V,  // SPI CS
    .queue_size = 1,           // SPI queue dimension
                     //.pre_cb=spi_pre_transfer_callback,    // Specify
                     // pre-transfer callback to handle D/C line
};

// SPI device configuration
spi_device_interface_config_t AD8400_IMAX_cfg = {
    .clock_speed_hz = 1000000, // SPI Clock out at 1 MHz
    .mode = 0,                 // SPI Mode 0
    .spics_io_num = PIN_CS_I,  // SPI CS
    .queue_size = 1,           // SPI queue dimension
                     //.pre_cb=spi_pre_transfer_callback,    // Specify
                     // pre-transfer callback to handle D/C line
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 *
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel,
        duty, fade_time_ms);
    ledc_fade_start(
        pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG, "PWM fade\tch:%d@%d in %d ms", ch, duty, fade_time_ms);
}

/**
 * @brief Set PWM channel dutycycle
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 *
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG, "PWM switch\tch:%d@%d", ch, duty);
}

/**
 * @brief Set Digital Potentiometer
 *
 * @param[in] handle SPI device handle
 * @param[in] value Value
 *
 * @return void
 */
void set_digital_pot(spi_device_handle_t handle, uint8_t value)
{
    uint8_t buffer[2];

    // Serial Data-Word Format [A1 A0 D7 D6 D5 D4 D3 D2, D1 D0]
    buffer[0] = value >> 2; // MSB zero padding
    buffer[1] = value << 6;

    spi_transaction_t t;
    memset(&t, 0, sizeof(t)); // Zero out the transaction
    t.length = 10;            // Command is 10 bits
    t.tx_buffer = &buffer;    // The data is the cmd itself
    // t.tx_buffer=&cmd;                                        // The data is
    // the cmd itself
    ESP_ERROR_CHECK(spi_device_transmit(handle, &t));
}

/**
 * @brief Read Touchpad level
 *
 * @param[in] ch Touchpad channel
 *
 * @return bool
 */
bool read_touch(touch_pad_t ch)
{
    uint16_t touch_value;
    touch_pad_read_filtered(ch, &touch_value);

    return (touch_value < touchpad_thresh);
}

/**
 * @brief Read Touch-slider level
 *
 * @param[in,out] touch_value Touch-slider measurement centroid
 *
 * @return bool
 */
bool read_slider(uint8_t* touch_value)
{
    // Update touch readings
    for (int i = 0; i < 6; i++)
        touch_pad_read_filtered(touch_slider[i], &touchpad_actual[i]);

    uint32_t sum_w = 0;
    uint32_t sum = 0;
    uint32_t dtouch = 0;

    for (int i = 0; i < 6; i++) {
        dtouch = abs(touchpad_offset[i] - touchpad_actual[i]);
        sum_w += dtouch * i * 51;
        sum += dtouch;
    }
    if (sum == 0)
        *touch_value
            = 0; // Avoid Divide by 0 if the base-reading hasn't changed
    else
        *touch_value = 256 - (sum_w / sum);

    return (sum > touchpad_thresh);
}

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief Entity main task
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void entity_main_tasks(void* arg)
{
    power_plant_control* control = (power_plant_control*)arg;
    setpoint_vbus_old = 0;
    setpoint_imax_old = 0;

    uint8_t slider_pos = 0;

    while (1) {
        // Wait timer
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
        entity.power_in = read_power(&control->power_mon);
        ESP_LOGD(TAG, "Telemetry\tbus: %f mV,shunt: %f mA, pwr: %f W",
            entity.power_in.voltage, entity.power_in.current,
            entity.power_in.power);

        // average current readings to smooth motor transitions
        control->power_values[control->counter] = entity.power_in.power;
        control->counter = (control->counter + 1) % 20;
        float total_power = 0;
        for (int i = 0; i < 20; ++i) {
            total_power += control->power_values[i];
        }
        control->power_averaged = total_power / 20;
        ESP_LOGD(TAG, "Average power: %f", control->power_averaged);

        if (fabs(control->power_averaged) < THRESHOLD_YELLOW_MW) {
            uint8_t perc = (uint8_t)(fabs(control->power_averaged)
                / THRESHOLD_YELLOW_MW * 255.0);
            tower_set_brightness(&control->tower, 255 - perc, GREEN);
            tower_set_brightness(&control->tower, perc, YELLOW);
            tower_set_brightness(&control->tower, 0, RED);
            pwm_switch(PWM_MOTOR, 2192 + 3000 * perc);
        } else if (fabs(control->power_averaged) < THRESHOLD_RED_MW) {
            uint8_t perc = (uint8_t)((fabs(control->power_averaged)
                                         - THRESHOLD_YELLOW_MW)
                / (THRESHOLD_RED_MW - THRESHOLD_YELLOW_MW) * 255.0);
            tower_set_brightness(&control->tower, 0, GREEN);
            tower_set_brightness(&control->tower, 255 - perc, YELLOW);
            tower_set_brightness(&control->tower, perc, RED);
            pwm_switch(PWM_MOTOR, 5192 + 2999 * perc);
        } else {
            tower_set_brightness(&control->tower, 0, GREEN);
            tower_set_brightness(&control->tower, 0, YELLOW);
            tower_set_brightness(&control->tower, 255, RED);
            pwm_switch(PWM_MOTOR, 8191);
        }

        // Get LDO temperature
        // TODO: USE THIS VALUE
        entity.ldo_temp
            = ((float)adc1_get_raw(SENS_TEMP) * 2.685547e-4 + 7.5e-2)
            / 1e-2; // (ADC*1.1 V / 2^12 + 75 mV) / 10 mV/°C

        // Get touchpad status
        if (read_touch(PIN_TOUCH_M)) {
            // todo
        }
        if (read_slider(&slider_pos)) {
            entity.setpoint_imax = slider_pos;
        }

        // Update bus voltage
        if (entity.setpoint_vbus != setpoint_vbus_old) {
            setpoint_vbus_old = entity.setpoint_vbus;
            set_digital_pot(AD8400_VBUS, entity.setpoint_vbus);
        }

        // Update current limit
        if (entity.setpoint_imax != setpoint_imax_old) {
            setpoint_imax_old = entity.setpoint_imax;
            set_digital_pot(AD8400_IMAX, entity.setpoint_imax);
        }
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init GPIO
 *
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    // Init ADC channel for Temperature monitor
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(SENS_TEMP, ADC_ATTEN_DB_0);

    // INA IRQ Missing
    return ESP_OK;
}

/**
 * @brief Init PWM channels
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer0 for low speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < PWM_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0)); // Initialize fade service
}

/**
 * @brief Init the SPI Interface
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_spi()
{
    ESP_ERROR_CHECK(
        spi_bus_initialize(HSPI_HOST, &buscfg, 0)); // Initialize the SPI bus
    ESP_ERROR_CHECK(spi_bus_add_device(HSPI_HOST, &AD8400_VBUS_cfg,
        &AD8400_VBUS)); // Attach the Poti to the SPI bus
    ESP_ERROR_CHECK(
        spi_bus_add_device(HSPI_HOST, &AD8400_IMAX_cfg, &AD8400_IMAX));

    return ESP_OK;
}

/**
 * @brief Init Touch Channels
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_touch()
{
    ESP_ERROR_CHECK(
        touch_pad_init()); // The default fsm mode is software trigger mode.

    ESP_ERROR_CHECK(touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5,
        TOUCH_HVOLT_ATTEN_1V)); // set ref voltage vor charging/discharging
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S1, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S2, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S3, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S4, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S5, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S6, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_M, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD));

    // Update non-touch status
    for (int i = 0; i < 6; i++)
        touch_pad_read_filtered(touch_slider[i], &touchpad_offset[i]);

    return ESP_OK;
}

/**
 * @brief Init timer for main entity task
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    entity_timer_alarm = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_fan_and_motor()
{
    // motor
#ifdef CONFIG_MOTOR_ENABLED
    gpio_reset_pin(CONFIG_MOTOR_GPIO_PIN);
    gpio_set_direction(CONFIG_MOTOR_GPIO_PIN, GPIO_MODE_OUTPUT);
    gpio_set_level(CONFIG_MOTOR_GPIO_PIN, 1);
#else
    gpio_set_level(CONFIG_MOTOR_GPIO_PIN, 0);
#endif

    // fan
#ifdef CONFIG_FAN_ENABLED
    gpio_reset_pin(CONFIG_FAN_GPIO_PIN);
    gpio_set_direction(CONFIG_FAN_GPIO_PIN, GPIO_MODE_OUTPUT);
    gpio_set_level(CONFIG_FAN_GPIO_PIN, 1);
#else
    gpio_set_level(CONFIG_FAN_GPIO_PIN, 0);
#endif
}

void init_entity(power_plant_control* control)
{
    ESP_LOGD(TAG, "Init GPIO");
    init_gpio();
    ESP_LOGD(TAG, "Init ID bus");
    init_id_bus(&control->id_bus, PIN_EEPROM);
    entity.id = read_id(&control->id_bus);
    if (entity.id == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init I2C");
    init_i2c(CONFIG_PWR_MON_PIN_SDA, CONFIG_PWR_MON_PIN_SCL,
        CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_I2C_FREQ);

    ESP_LOGD(TAG, "Init SPI");
    init_spi();
    ESP_LOGD(TAG, "Init Touchfield");
    init_touch();

    ESP_LOGD(TAG, "Init LEDs");
    pca9956_t pca = pca9956_init(CONFIG_PWR_MON_I2C_BUS, 0x19, 2000, 25);
    control->tower = tower_init(pca);
    // Startup "animation"
    for (int i = 0; i < 250; i += 5) {
        tower_set_brightness(&control->tower, i, GREEN);
        vTaskDelay(10 / portTICK_PERIOD_MS); // Yield task
    }

    control->power_mon
        = pwr_monitor_init(CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon,
        atof(CONFIG_PWR_MON_R_SHUNT), atof(CONFIG_PWR_MON_I_MAX)));

    control->counter = 0;

    ESP_LOGD(TAG, "Starting main task");

    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());

    ESP_LOGD(TAG, "Init Fan and Motor");
    init_fan_and_motor();
    ESP_LOGD(TAG, "Init PWM");
    init_pwm();
}

//
//---- EXTERNAL FUNCTIONS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t powerplant_http_resp(char* str)
{
    sprintf(str, "power|%.2f;load|%.2f", entity.power_in.power / 1000,
        entity.power_in.current / 4000.0);

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t powerplant_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "max_load", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.setpoint_imax = 2 * (val - 0.5) * 255;
            ESP_LOGD(TAG, "MAX LOAD");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void powerplant_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_service));
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_sub));
}

float setpoint_to_current_mA(uint8_t setpoint)
{
    // t	7
    // m	1635
    float t = 7;
    float m = 1635;
    return setpoint * t + m;
}

void powerplant_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    char data[128];
    sprintf(data,
        "{\"current_out\": %6.1f, \"voltage\": %4.2f, \"current_limit\": "
        "%3.1f}",
        -entity.power_in.current, entity.power_in.voltage,
        setpoint_to_current_mA(entity.setpoint_imax));

    esp_mqtt_client_publish(
        mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);

    memset(data, 0, 64);
    sprintf(data, "fuel|%.2f", entity.power_in.current / 4000.0);
    esp_mqtt_client_publish(mqtt_client, mqtt_topic_service, data, 0, 0, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_service, data);
}

void powerplant_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }
    }

    if (!strcmp(topic, mqtt_topic_sub)) {
        if (msg_unmarshall(data, "vbus", &msg_payload)) {
            uint8_t val = atoi(msg_payload.val);
            entity.setpoint_vbus = val;
            ESP_LOGD(TAG, "BUS VOLTAGE");
            return;
        }

        if (msg_unmarshall(data, "imax", &msg_payload)) {
            uint8_t val = atoi(msg_payload.val);
            entity.setpoint_imax = val;
            ESP_LOGD(TAG, "BUS CURRENT");
            return;
        }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    power_plant_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY);
    mqtt_callbacks_t mqtt_cb
        = { .init_callback = powerplant_mqtt_subscribe_topics,
              .post_callback = powerplant_mqtt_post,
              .read_callback = powerplant_mqtt_read };
    init_mqtt(CONFIG_ENTITY, &mqtt_cb); // Init MQTT message protocol

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
