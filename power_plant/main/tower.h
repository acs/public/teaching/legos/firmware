//  Copyright:  2022, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Jonathan Klimt <klimt.jonathan@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _TOWER_
#define _TOWER_

#include "pca9956.h"

typedef enum led_color { GREEN, YELLOW, RED } led_color;

typedef struct tower_leds {
    float max_current;
    int led_pins[6];
} tower_leds;

typedef struct plant_tower {
    tower_leds green_leds;
    tower_leds yellow_leds;
    tower_leds red_leds;
    pca9956_t pca;
} plant_tower;

plant_tower tower_init(pca9956_t pca);

void tower_set_brightness(
    plant_tower* tower, uint8_t brightness, led_color color);

#endif // _TOWER
