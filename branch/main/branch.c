//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/spi_master.h"
#include "driver/timer.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
/*#include "freertos/FreeRTOS.h"*/
/*#include "freertos/event_groups.h"*/
/*#include "freertos/queue.h"*/
/*#include "freertos/semphr.h"*/
/*#include "freertos/task.h"*/
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_power.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "sdkconfig.h"

// TODO: Make this menuconfig
#define STRINGIFY(stri) #stri
#define ID STRINGIFY(4)
#define CLIENT_NAME "branch" ID

// ---------- GPIO Definitions ----------
#define PIN_EEPROM_A GPIO_NUM_5 // 1 Wire EEPROM
#define PIN_EEPROM_B GPIO_NUM_15

#define PIN_LED_1A GPIO_NUM_9 // LED Branch A
#define PIN_LED_2A GPIO_NUM_10
#define PIN_LED_3A GPIO_NUM_18
#define PIN_LED_4A GPIO_NUM_23
#define PIN_LED_5A GPIO_NUM_19
#define PIN_LED_6A GPIO_NUM_22
#define PIN_LED_7A GPIO_NUM_13

#define PIN_LED_1B GPIO_NUM_12 // LED Branch B
#define PIN_LED_2B GPIO_NUM_32
#define PIN_LED_3B GPIO_NUM_33
#define PIN_LED_4B GPIO_NUM_25
#define PIN_LED_5B GPIO_NUM_26
#define PIN_LED_6B GPIO_NUM_27
#define PIN_LED_7B GPIO_NUM_14

#define PIN_BUS_BREAK GPIO_NUM_21 // Bus control  enable (0) / disable (1)
#define PIN_BUS_FAULT GPIO_NUM_2  // Bus fault    enable (1) / disable (0)

#define IRQ_INA_A GPIO_NUM_35 // Interrupt Input of INA233
#define IRQ_INA_B GPIO_NUM_34 // Interrupt Input of INA233

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

enum led_branch_a {
    LED_BRANCH_1A = 0, // GPIO_NUM_9  Branch A LEDs
    LED_BRANCH_2A,     // GPIO_NUM_10
    LED_BRANCH_3A,     // GPIO_NUM_18
    LED_BRANCH_4A,     // GPIO_NUM_23
    LED_BRANCH_5A,     // GPIO_NUM_19
    LED_BRANCH_6A,     // GPIO_NUM_22
    LED_BRANCH_7A,     // GPIO_NUM_13
    LED_MAX_A,
};

enum led_branch_b {
    LED_BRANCH_1B = LED_MAX_A, // GPIO_NUM_12 Branch B LEDs
    LED_BRANCH_2B,             // GPIO_NUM_32
    LED_BRANCH_3B,             // GPIO_NUM_33
    LED_BRANCH_4B,             // GPIO_NUM_25
    LED_BRANCH_5B,             // GPIO_NUM_26
    LED_BRANCH_6B,             // GPIO_NUM_27
    LED_BRANCH_7B,             // GPIO_NUM_14
    LED_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Switch status structure
 */
typedef struct {
    bool uv; // Under-voltage
    bool ov; // Over-voltage
    bool uc; // Under-current
    bool oc; // Over-current
    bool lc; // Leakage-current
} fault_t;

/**
 * @brief Switch status structure
 */
typedef struct {
    bool sc; // Parallel Short-circuit
    bool oc; // Series Open-circuit
} switch_t;

/**
 * @brief Power-flow to LED speed mapping mode
 */
typedef enum {
    FLOW_LINEAR = 0,
    FLOW_QUADRATIC,
    FLOW_CUBIC,
} led_mapping_t;

/**
 * @brief Entity status structure
 */
struct Branch {
    uint64_t id_a;        // Unique A port identifier
    uint64_t id_b;        // Unique B port identifier
    fault_t fault;        // Fault status
    switch_t switchgear;  // Switchgear status
    float led_intensity;  // Normalized global brightness
    power_readings bus_a; // Bus A telemetry
    power_readings bus_b; // Bus B telemetry
} entity;

typedef struct branch_control {
    power_monitor power_mon_a;
    power_monitor power_mon_b;
    id_bus id_bus_a;
    id_bus id_bus_b;
    // TODO: GPIO
} branch_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/grid";
static const char mqtt_topic_sub[] = "legos/branch/cmd";

static const char* TAG = "Branch"; // Logging tag
/*static const char* ID = "#13";     // Branch ID*/

static uint16_t pwrflow_timer_ms = 4;    // Power-flow Thread interval
static uint64_t pwrflow_timer_alarm;     // Power-flow Thread timer
static TaskHandle_t pwrflow_task_handle; // Power-flow Thread task handle

static uint16_t entity_timer_ms = 100;        // Main-tasks Thread interval
static uint64_t entity_timer_alarm;           // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;       // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
    TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
    TIMER_DIVIDER };

static float led_intensity;          // LED intensity absolute
static float Imax;                   // Bus current max
static float In_a, In_b;             // Bus current normalized
static int16_t Id_a, Id_b;           // Bus current discretized
static uint8_t led_max_a, led_max_b; // Bus LED update counter
static uint8_t remote_override = 0;

// LEDC channel configuration
static ledc_channel_config_t pwm_channel[LED_MAX] = {
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_LED_1A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LED_2A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 2,
        .duty = 0,
        .gpio_num = PIN_LED_3A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 3,
        .duty = 0,
        .gpio_num = PIN_LED_4A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 4,
        .duty = 0,
        .gpio_num = PIN_LED_5A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 5,
        .duty = 0,
        .gpio_num = PIN_LED_6A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 6,
        .duty = 0,
        .gpio_num = PIN_LED_7A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_LED_1B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LED_2B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 2,
        .duty = 0,
        .gpio_num = PIN_LED_3B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 3,
        .duty = 0,
        .gpio_num = PIN_LED_4B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 4,
        .duty = 0,
        .gpio_num = PIN_LED_5B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 5,
        .duty = 0,
        .gpio_num = PIN_LED_6B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 6,
        .duty = 0,
        .gpio_num = PIN_LED_7B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 5000,                      // PWM frequency
    .speed_mode = PWM_LS_MODE,            // PWM timer mode
    .timer_num = PWM_LS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 *
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel,
        duty, fade_time_ms);
    ledc_fade_start(
        pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG, "PWM fade\tch:%d@%d in %d ms", ch, duty, fade_time_ms);
}

/**
 * @brief Set PWM channel dutycycle
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 *
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG, "PWM switch\tch:%d@%d", ch, duty);
}

/**
 * @brief Update LED power flow coefficients
 *
 * @param[in] mode LED mapping mode (Linear, Quadratic, Cubic)
 *
 * @return void
 */
void led_update_flow(led_mapping_t mode)
{
    In_a = (fabs(entity.bus_a.current) < Imax)
        ? (entity.bus_a.current / Imax)
        : (entity.bus_a.current / fabs(entity.bus_a.current));
    In_b = (fabs(entity.bus_b.current) < Imax)
        ? (entity.bus_b.current / Imax)
        : (entity.bus_b.current / fabs(entity.bus_b.current));

    Id_a = In_a * 255.0;
    Id_b = In_b * 255.0;

    switch (mode) {
    case FLOW_LINEAR: {
        led_max_a = (Id_a > 0) ? (259 - Id_a) : (259 + Id_a);
        // TODO: Synchronize when the difference is small
        led_max_b = (Id_b > 0) ? (259 - Id_b) : (259 + Id_b);
    } break;

    case FLOW_QUADRATIC: {
        led_max_a = (Id_a > 0) ? ((In_a - 1) * (In_a - 1) * 251 + 4)
                               : ((In_a + 1) * (In_a + 1) * 251 + 4);
        if (abs(Id_a + Id_b) < 2) {
            // If the difference is small (no short) then synchronise the visual
            // effect
            led_max_b = led_max_a;
        } else {
            led_max_b = (Id_b > 0) ? ((In_b - 1) * (In_b - 1) * 251 + 4)
                                   : ((In_b + 1) * (In_b + 1) * 251 + 4);
        }
    } break;

    case FLOW_CUBIC: {
        led_max_a = (Id_a > 0)
            ? ((1 - In_a) * (1 - In_a) * (1 - In_a) * 251 + 4)
            : ((In_a + 1) * (In_a + 1) * (In_a + 1) * 251 + 4);
        // TODO: Synchronize when the difference is small
        led_max_b = (Id_b > 0)
            ? ((1 - In_b) * (1 - In_b) * (1 - In_b) * 251 + 4)
            : ((In_b + 1) * (In_b + 1) * (In_b + 1) * 251 + 4);
    } break;

    default:
        break;
    }
}

/**
 * @brief Update LED power flow task
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void led_pwrflow_task(void* arg)
{
    uint8_t led_count_a = 0;
    uint8_t led_count_b = 0;
    float led_seq_null[7] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    float led_seq_pos_a[7] = { 0.0, 0.0, 0.0, 156E-4, 625E-4, 25E-2, 1.0 };
    float led_seq_pos_b[7] = { 0.0, 0.0, 0.0, 156E-4, 625E-4, 25E-2, 1.0 };
    float led_seq_neg_a[7] = { 1.0, 25E-2, 625E-4, 156E-4, 0.0, 0.0, 0.0 };
    float led_seq_neg_b[7] = { 1.0, 25E-2, 625E-4, 156E-4, 0.0, 0.0, 0.0 };
    float *led_seq_a, *led_seq_b;

    while (1) {
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Check power flow direction
        if (Id_a == 0)
            led_seq_a = led_seq_null;
        else if (Id_a < 0)
            led_seq_a = led_seq_pos_a;
        else
            led_seq_a = led_seq_neg_a;

        if (Id_b == 0)
            led_seq_b = led_seq_null;
        else if (Id_b > 0)
            led_seq_b = led_seq_pos_b;
        else
            led_seq_b = led_seq_neg_b;

        // Show power flow bus A
        if (++led_count_a >= led_max_a) {
            led_count_a = 0;

            // Rotate LED intensity sequence
            if (Id_a > 0) {
                float temp = led_seq_a[0];
                led_seq_a[0] = led_seq_a[1];
                led_seq_a[1] = led_seq_a[2];
                led_seq_a[2] = led_seq_a[3];
                led_seq_a[3] = led_seq_a[4];
                led_seq_a[4] = led_seq_a[5];
                led_seq_a[5] = led_seq_a[6];
                led_seq_a[6] = temp;
            } else if (Id_a < 0) {
                float temp = led_seq_a[6];
                led_seq_a[6] = led_seq_a[5];
                led_seq_a[5] = led_seq_a[4];
                led_seq_a[4] = led_seq_a[3];
                led_seq_a[3] = led_seq_a[2];
                led_seq_a[2] = led_seq_a[1];
                led_seq_a[1] = led_seq_a[0];
                led_seq_a[0] = temp;
            }

            for (int ch = 0; ch < LED_MAX_A; ch++)
                pwm_switch(ch, led_seq_a[ch] * led_intensity);
        }

        if (abs(Id_a + Id_b) < 2) {
            // Sync LEDs when there is no real difference in consumption
            led_seq_b = led_seq_a;
            for (int ch = LED_MAX_A; ch < LED_MAX; ch++)
                pwm_switch(ch, led_seq_b[ch - LED_MAX_A] * led_intensity);
        } else if (++led_count_b >= led_max_b) {
            // Show power flow bus B
            led_count_b = 0;

            // Rotate LED intensity sequence
            if (Id_b > 0) {
                float temp = led_seq_b[6];
                led_seq_b[6] = led_seq_b[5];
                led_seq_b[5] = led_seq_b[4];
                led_seq_b[4] = led_seq_b[3];
                led_seq_b[3] = led_seq_b[2];
                led_seq_b[2] = led_seq_b[1];
                led_seq_b[1] = led_seq_b[0];
                led_seq_b[0] = temp;
            } else if (Id_b < 0) {
                float temp = led_seq_b[0];
                led_seq_b[0] = led_seq_b[1];
                led_seq_b[1] = led_seq_b[2];
                led_seq_b[2] = led_seq_b[3];
                led_seq_b[3] = led_seq_b[4];
                led_seq_b[4] = led_seq_b[5];
                led_seq_b[5] = led_seq_b[6];
                led_seq_b[6] = temp;
            }

            for (int ch = LED_MAX_A; ch < LED_MAX; ch++)
                pwm_switch(ch, led_seq_b[ch - LED_MAX_A] * led_intensity);
        }
    }
}

void detect_fault_status()
{
    // Check over-voltage
    if ((entity.bus_a.voltage > 3.6) || (entity.bus_b.voltage > 3.6))
        entity.fault.ov = true;
    else
        entity.fault.ov = false;

    // Check under-voltage
    if ((entity.bus_a.voltage < 3.0) || (entity.bus_b.voltage < 3.0))
        entity.fault.uv = true;
    else
        entity.fault.uv = false;

    // Check over-current
    if ((fabs(entity.bus_a.current) > 4000.0)
        || (fabs(entity.bus_b.current) > 4000.0))
        entity.fault.oc = true;
    else
        entity.fault.oc = false;

    // Check under-current
    if ((fabs(entity.bus_a.current) < 2.5)
        || (fabs(entity.bus_b.current) < 2.5))
        entity.fault.uc = true;
    else
        entity.fault.uc = false;

    // Check leakage-current
    double delta = fabs(entity.bus_a.current + entity.bus_b.current);
    if (delta > 5)
        entity.fault.lc = true;
    else
        entity.fault.lc = false;
}

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief LED power flow task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR pwrflow_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t1 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_1, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(pwrflow_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

void entity_main_tasks(void* arg)
{
    branch_control* control = (branch_control*)arg;
    Imax = 3500.0; // LED saturation current [mA]

    while (1) {
        // Wait timer
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
        entity.bus_a = read_power(&control->power_mon_a);
        entity.bus_b = read_power(&control->power_mon_b);

        // Get absolute LED intensity
        led_intensity = entity.led_intensity * LED_INTENSITY_100;

        // Check fault status
        detect_fault_status();

        // Update LED power flow speed
        led_update_flow(FLOW_QUADRATIC);

        // Update switches
        if (remote_override) {
            if ((remote_override - 1) == 0) {
                gpio_set_level(PIN_BUS_BREAK, 0);
                gpio_set_level(PIN_BUS_FAULT, 0);
            } else {
                remote_override--;
            }
        }
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init GPIO
 *
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = ((1ULL << PIN_BUS_BREAK) | (1ULL << PIN_BUS_FAULT));
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);

    gpio_set_level(PIN_BUS_BREAK, 0);
    gpio_set_level(PIN_BUS_FAULT, 0);

    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = ((1ULL << IRQ_INA_A) | (1ULL << IRQ_INA_B));
    gpio_config(&io_conf);

    return ESP_OK;
}

/**
 * @brief Init PWM channels
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer0 for low speed channels
    // Prepare and set configuration of timer1 for high speed channels
    pwm_timer.speed_mode = PWM_HS_MODE;
    pwm_timer.timer_num = PWM_HS_TIMER;
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer1 for high speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < LED_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0)); // Init fade service
}

/**
 * @brief Init timer for LED power flow
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_pwrflow_timer()
{
    pwrflow_timer_alarm = (uint64_t)pwrflow_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_1, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_1));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_1, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_1, pwrflow_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_1));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_1, pwrflow_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_1));

    ESP_LOGD(TAG, "Power flow interval set to: %d ms", pwrflow_timer_ms);

    return ESP_OK;
}

/**
 * @brief Init timer for main entity task
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    Id_a = 0;
    Id_b = 0;

    entity_timer_alarm = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGD(TAG, "Entity interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_entity(branch_control* control)
{
    // Set default LED intensity
    entity.led_intensity = 0.5;

    ESP_LOGD(TAG, "Init GPIO");
    init_gpio();
    ESP_LOGD(TAG, "Init PWM");
    init_pwm();

    ESP_LOGD(TAG, "Init ID bus A");
    init_id_bus(&control->id_bus_a, PIN_EEPROM_A);
    entity.id_a = read_id(&control->id_bus_a);
    if (entity.id_b == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init ID bus B");
    init_id_bus(&control->id_bus_b, PIN_EEPROM_B);
    entity.id_b = read_id(&control->id_bus_b);
    if (entity.id_b == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init I2C");
    init_i2c(CONFIG_PIN_SDA, CONFIG_PIN_SCL, CONFIG_I2C_BUS, CONFIG_I2C_FREQ);

    ESP_LOGD(TAG, "Init power monitor A");
    control->power_mon_a
        = pwr_monitor_init(CONFIG_PWR_MON_A_I2C_BUS, CONFIG_PWR_MON_A_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon_a,
        atof(CONFIG_PWR_MON_A_R_SHUNT), atof(CONFIG_PWR_MON_A_I_MAX)));

    ESP_LOGD(TAG, "Init power monitor B");
    control->power_mon_b
        = pwr_monitor_init(CONFIG_PWR_MON_B_I2C_BUS, CONFIG_PWR_MON_B_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon_b,
        atof(CONFIG_PWR_MON_B_R_SHUNT), atof(CONFIG_PWR_MON_B_I_MAX)));

    ESP_LOGD(TAG, "Starting powerflow visualization task");
    xTaskCreate(led_pwrflow_task, "led_pwrflow_task", 2048, NULL, 5,
        &pwrflow_task_handle);
    ESP_ERROR_CHECK(init_pwrflow_timer());

    ESP_LOGD(TAG, "Starting main task");
    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());
}

//
//---- EXTERNAL CALLBACKS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t branch_http_resp(char* str)
{
    sprintf(str, "bus_a|%.2f;bus_b|%.2f", entity.bus_a.power / 1000,
        entity.bus_b.power / 1000);

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t branch_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.led_intensity = val;
            ESP_LOGD(TAG, "LIGHT");
        }
        return ESP_OK;
    }

    if (msg_unmarshall(str, "break", &msg_payload)) {
        uint8_t val = atoi(msg_payload.val);
        if (val <= 1) {
            remote_override = 255; // 25 s
            gpio_set_level(PIN_BUS_BREAK, val);
            ESP_LOGD(TAG, "LINE SWITCH");
        }
        return ESP_OK;
    }

    if (msg_unmarshall(str, "fault", &msg_payload)) {
        uint8_t val = atoi(msg_payload.val);
        if (val <= 1) {
            remote_override = 150; // 15 s
            gpio_set_level(PIN_BUS_FAULT, val);
            ESP_LOGD(TAG, "FAULT SWITCH");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void branch_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_service));
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_sub));
}

void branch_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    char data[128];
    sprintf(data,
        "{\"currentA\": %5.1f, \"voltageA\": %4.2f, \"currentB\": %5.1f, "
        "\"voltageB\": %4.2f}",
        // todo: comma
        entity.bus_a.current, entity.bus_a.voltage, entity.bus_b.current,
        entity.bus_b.voltage);
    char mqtt_subtopic[32];
    strcpy(mqtt_subtopic, mqtt_topic_pub);
    strcat(mqtt_subtopic, "/");
    strcat(mqtt_subtopic, ID);

    esp_mqtt_client_publish(
        mqtt_client, mqtt_subtopic, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
}

void branch_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }

        if (msg_unmarshall(data, "light", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0)) {
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return;
        }
    }

    if (!strcmp(topic, mqtt_topic_sub)) {
        if (msg_unmarshall(data, "open", &msg_payload))
            if (!strcmp(msg_payload.val, ID)) {
                ESP_LOGD(TAG, "OPEN");
                gpio_set_level(PIN_BUS_BREAK, 1);
                return;
            }

        if (msg_unmarshall(data, "close", &msg_payload))
            if (!strcmp(msg_payload.val, ID)) {
                ESP_LOGD(TAG, "CLOSE");
                gpio_set_level(PIN_BUS_BREAK, 0);
                return;
            }

        if (msg_unmarshall(data, "fault", &msg_payload))
            if (!strcmp(msg_payload.val, ID)) {
                remote_override = 5; // 5 s
                ESP_LOGD(TAG, "FAULT");
                gpio_set_level(PIN_BUS_FAULT, 1);
                return;
            }

        if (msg_unmarshall(data, "clear", &msg_payload))
            if (!strcmp(msg_payload.val, ID)) {
                ESP_LOGD(TAG, "CLEAR");
                gpio_set_level(PIN_BUS_FAULT, 0);
                return;
            }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    branch_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY CONFIG_ID);
    mqtt_callbacks_t mqtt_cb = { .init_callback = branch_mqtt_subscribe_topics,
        .post_callback = branch_mqtt_post,
        .read_callback = branch_mqtt_read };
    init_mqtt(CONFIG_ENTITY CONFIG_ID, &mqtt_cb); // Init MQTT message protocol

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
