idf_component_register(SRCS "hospital.c" "leds.c"
                    INCLUDE_DIRS ".")

# Create a SPIFFS image from the contents of the entity-specific 'html' folder
spiffs_create_partition_image(storage ../html FLASH_IN_PROJECT)
