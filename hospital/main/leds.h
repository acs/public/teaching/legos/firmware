#include "pca9956.h"

typedef struct ups_leds {
    pca9956_t* controller;
    uint8_t anim_step;
} ups_leds_t;

typedef struct car_leds_t {
    pca9956_t* controller;
} car_leds_t;

ups_leds_t init_ups_leds(pca9956_t* pca);

void ups_leds_set_perc(ups_leds_t* ups_leds, uint8_t perc, bool inv);

void hospital_leds_startup_animation(
    ups_leds_t* ups_leds, car_leds_t* car_leds);

car_leds_t init_car_leds(pca9956_t* pca);

void car_leds_enable(car_leds_t* car_leds, bool enable);