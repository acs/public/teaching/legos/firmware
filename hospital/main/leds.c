#include "leds.h"
#include "esp_log.h"

static const char* TAG = "Data center LEDs"; // Logging tag

int ups_led_pins[12] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
int car_led_pins[5] = { 12, 13, 14, 15, 16 };
float i_white = (100 / 180.0);

ups_leds_t init_ups_leds(pca9956_t* pca)
{
    for (int i = 0; i < 12; ++i) {
        pca9956_set_max_current(pca, ups_led_pins[i], i_white);
    }
    ups_leds_t ups_leds = {
        .controller = pca,
        .anim_step = 0,
    };
    return ups_leds;
}

void ups_leds_set_perc(ups_leds_t* ups_leds, uint8_t perc, bool inv)
{
    int max_led = (12.0 / 100.0) * (float)perc;
    /*ESP_LOGD(TAG, "max_led %d", max_led);*/
    for (int i = 0; i < max_led; ++i) {
        uint8_t duty = 255;
        if (inv) {
            duty = 0;
        }
        pca9956_single_switch(ups_leds->controller, ups_led_pins[i], duty);
    }
    for (int i = max_led; i < 12; ++i) {
        uint8_t duty = 0;
        if (inv) {
            duty = 255;
        }
        pca9956_single_switch(ups_leds->controller, ups_led_pins[i], duty);
    }
    if (max_led != 12) {
        uint8_t last_led_duty
            = ((12.0 / 100.0) * (float)perc - (float)max_led) * 255;
        /*ESP_LOGD(TAG, "last_led_duty %d", last_led_duty);*/
        if (inv) {
            last_led_duty = 255 - last_led_duty;
        }
        pca9956_single_switch(
            ups_leds->controller, ups_led_pins[max_led], last_led_duty);
    }
}

void hospital_leds_startup_animation(ups_leds_t* ups_leds, car_leds_t* car_leds)
{
    // UPS
    const int speed = 10;
    for (int i = 0; i < 100; i += 2) {
        ups_leds_set_perc(ups_leds, i, false);
        vTaskDelay(speed / portTICK_PERIOD_MS); // Yield task
    }
    for (int i = 0; i < 100; i += 2) {
        ups_leds_set_perc(ups_leds, i, true);
        vTaskDelay(speed / portTICK_PERIOD_MS); // Yield task
    }
    for (int i = 0; i < 12; ++i) {
        pca9956_single_switch(ups_leds->controller, ups_led_pins[i], 0);
    }
    // CARLEDS
    car_leds_enable(car_leds, true);
    vTaskDelay(500 / portTICK_PERIOD_MS);
    car_leds_enable(car_leds, false);
}

car_leds_t init_car_leds(pca9956_t* pca)
{
    for (int i = 0; i < 5; ++i) {
        pca9956_set_max_current(pca, ups_led_pins[i], i_white);
    }
    car_leds_t car_leds = {
        .controller = pca,
    };
    return car_leds;
}

void car_leds_enable(car_leds_t* car_leds, bool enable)
{
    uint8_t duty;
    if (enable) {
        duty = 255;
    } else {
        duty = 0;
    }
    for (int i = 0; i < 5; ++i) {
        pca9956_single_switch(car_leds->controller, car_led_pins[i], duty);
    }
}