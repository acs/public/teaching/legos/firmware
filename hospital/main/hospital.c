#include "driver/gpio.h"
#include "driver/i2c.h"
#include "driver/ledc.h"
/*#include "driver/spi_master.h"*/
#include "driver/timer.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
/*#include "freertos/event_groups.h"*/
/*#include "freertos/queue.h"*/
#include "freertos/semphr.h"
#include "freertos/task.h"
/*#include "ina233.h"*/
#include "hal/dac_types.h"
#include "leds.h"
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_power.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "sdkconfig.h"
#include "ups.h"
#include "virtual_load.h"

#include <stdio.h>

#define STATIC_IP 55

// ---------- GPIO Definitions ----------
#define PIN_EEPROM GPIO_NUM_0 // 1 Wire EEPROM

/*#define PIN_SDA GPIO_NUM_33 // I2C Pins*/
/*#define PIN_SCL GPIO_NUM_25*/

#define PIN_SRC GPIO_NUM_15 // Power source selection (0 GRID, 1 UPS)

#define PIN_SENS_HELI GPIO_NUM_17 // Magnetic switches
#define PIN_SENS_CAR GPIO_NUM_5
#define PIN_LED_R0 GPIO_NUM_4 // LED rooms
#define PIN_LED_R1 GPIO_NUM_16

#define PIN_UPS_1 GPIO_NUM_2 // LED UPS
#define PIN_UPS_2 GPIO_NUM_13
#define PIN_UPS_3 GPIO_NUM_12

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

enum led_emergency {
    LED_ROOM_0 = 0, // GPIO_NUM_4 Room lights
    LED_ROOM_1,
    LED_ROOM_MAX,
};

enum led_ups {
    LED_UPS_1 = LED_ROOM_MAX, // GPIO_NUM_2  UPS LEDs
    LED_UPS_2,                // GPIO_NUM_13
    LED_UPS_3,                // GPIO_NUM_12
    LED_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Alert level
 */
typedef enum {
    EMERGENCY_NONE = 0,
    EMERGENCY_CAR,
    EMERGENCY_HELI,
    EMERGENCY_BOTH,
} emergency_t;

/**
 * @brief Entity status structure
 */
// TODO: Protect with mutex
typedef struct Hospital {
    uint64_t id;                 // Unique port identifier
    power_source_t power_source; // Power source (0 GRID, 1 UPS)
    float led_intensity;         // Normalized global brightness
    emergency_t status;          // Emergency sensors status
    power_readings power_in;     // Input telemetry
    ups ups;                     // Uninterruptible Power Supply
} hospital_status;

// TODO: Better name
static hospital_status entity = { .id = 0,
    .power_source = GRID,
    .led_intensity = 0.5,
    .status = EMERGENCY_NONE,
    .power_in = { 0.0, 0.0, 0.0 } };

typedef struct hospial_overrides {
    emergency_t remote;
} hospial_overrides;

hospial_overrides overrides = {
    .remote = EMERGENCY_NONE,
};

typedef struct hospital_control {
    power_monitor power_mon;
    id_bus id_bus;
    pca9956_t pca;
    ups_leds_t ups_led;
    car_leds_t car_led;
    virtual_load_t virtual_load;
    // TODO: GPIO
} hospital_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/hospital";
static const char mqtt_topic_sub[] = "legos/hospital/cmd";

static const char* TAG = "Hospital"; // Logging tag

static uint16_t entity_timer_ms = 100;  // Main-tasks Thread interval
static TaskHandle_t entity_task_handle; // Main-tasks Thread task handle

// LEDC channels configuration
static ledc_channel_config_t pwm_channel[LED_MAX] = {
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_LED_R0, // LED_ROOM_0
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LED_R1, // LED_ROOM_1
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 5000,                      // PWM frequency
    .speed_mode = PWM_LS_MODE,            // PWM timer mode
    .timer_num = PWM_LS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 *
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel,
        duty, fade_time_ms);
    ledc_fade_start(
        pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG, "PWM fade\tch:%d@%d in %d ms", ch, duty, fade_time_ms);
}

/**
 * @brief Set PWM channel dutycycle
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 *
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG, "PWM switch\tch:%d@%d", ch, duty);
}

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief Entity main task
 *
 * @param[in] arg hospital struct
 *
 * @return void
 */
void entity_main_tasks(void* arg)
{
    hospital_control* control = (hospital_control*)arg;
    // Initialize random seed
    srand(time(NULL));

    while (1) {
        // Wait for timer notification
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
        entity.power_in = read_power(&control->power_mon);
        ESP_LOGD(TAG, "Telemetry\tbus: %f V,shunt: %f mA, pwr: %f mW",
            entity.power_in.voltage, entity.power_in.current,
            entity.power_in.power);

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity * LED_INTENSITY_100;

        // Get emergency status
        entity.status = (!gpio_get_level(PIN_SENS_CAR))
            | ((!gpio_get_level(PIN_SENS_HELI)) << 0x01) | overrides.remote;

        // switch LEDs acccording to status
        if (entity.ups.charge_level > 0.01) {
            switch (entity.status) {
            case EMERGENCY_NONE:
                car_leds_enable(&control->car_led, false);
                pwm_switch(LED_ROOM_1, LED_INTENSITY_0);
                break;

            case EMERGENCY_CAR:
                car_leds_enable(&control->car_led, true);
                pwm_switch(LED_ROOM_1, LED_INTENSITY_0);
                break;

            case EMERGENCY_HELI:
                car_leds_enable(&control->car_led, false);
                pwm_switch(LED_ROOM_1, led_intensity);
                break;

            case EMERGENCY_BOTH:
                car_leds_enable(&control->car_led, true);
                pwm_switch(LED_ROOM_1, led_intensity);
                break;
            }
        } else {
            car_leds_enable(&control->car_led, false);
            pwm_switch(LED_ROOM_1, LED_INTENSITY_0);
        }

        // check bus voltage.
        // voltage above 1600 mV is considered as active power supply
        if (entity.power_in.voltage > 1600) {
            entity.power_source = GRID;
            charge(&entity.ups, 100);
        } else {
            entity.power_source = UPS;
            discharge(&entity.ups, 100);
        }

        // virtual load
        float load_percent = 0;
        float ups_charge_rate = 0.2;
        if (entity.ups.charge_level < 99) { // more load while charging
            load_percent += 0.05;
        }
        if (entity.status == EMERGENCY_CAR) {
            load_percent += 0.05;
            ups_charge_rate += 0.2;
        }
        if (entity.status == EMERGENCY_HELI) {
            load_percent += 0.05;
            ups_charge_rate += 0.2;
        }
        if (entity.status == EMERGENCY_BOTH) {
            load_percent += 0.1;
            ups_charge_rate += 0.4;
        }
        if (entity.ups.charge_level > 0.1) {
            virtual_load_set_percent(&control->virtual_load, load_percent);
        }

        // set charge/discharge rate of ups according to load
        entity.ups.charge_rate = 5 * (1 - 2 * ups_charge_rate);
        entity.ups.discharge_rate = 10 * 2 * ups_charge_rate;

        ESP_LOGD(TAG, "Voltage in mA: %f", entity.ups.charge_level);

        // ups led
        ups_leds_set_perc(&control->ups_led, entity.ups.charge_level, false);
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init GPIO
 *
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    ESP_ERROR_CHECK(gpio_set_direction(PIN_SRC, GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(gpio_set_direction(PIN_SENS_HELI, GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(gpio_set_direction(PIN_SENS_CAR, GPIO_MODE_INPUT));

    return ESP_OK;
}

/**
 * @brief Init PWM channels
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
// TODO: pull pwm_timer out of this and make it a global lib function
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer0 for low speed channels
    // Prepare and set configuration of timer1 for high speed channels
    pwm_timer.speed_mode = PWM_HS_MODE;
    pwm_timer.timer_num = PWM_HS_TIMER;
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer1 for high speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < LED_MAX; ch++) {
        ESP_LOGD(TAG, "config channel %d", ch);
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));
    }
    return (ledc_fade_func_install(0)); // Initialize fade service
}

/**
 * @brief Init timer for main entity task
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    uint64_t entity_timer_alarm
        = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
        TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
        TIMER_DIVIDER };

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_entity(hospital_control* control)
{
    ESP_LOGD(TAG, "Init GPIO");
    init_gpio();
    ESP_LOGD(TAG, "Init PWM");
    init_pwm();

    ESP_LOGD(TAG, "Init ID bus");
    init_id_bus(&control->id_bus, PIN_EEPROM);
    entity.id = read_id(&control->id_bus);
    if (entity.id == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init I2C");
    init_i2c(CONFIG_PWR_MON_PIN_SDA, CONFIG_PWR_MON_PIN_SCL,
        CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_I2C_FREQ);
    control->power_mon
        = pwr_monitor_init(CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon,
        atof(CONFIG_PWR_MON_R_SHUNT), atof(CONFIG_PWR_MON_I_MAX)));

    // init virtual ups
    entity.ups.charge_level = 0.0;
    entity.ups.charge_rate = 5.0;
    entity.ups.discharge_rate = 10.0;

    // init leds
    control->pca = pca9956_init(CONFIG_PWR_MON_I2C_BUS, 0x19, 2000, 19);
    control->ups_led = init_ups_leds(&control->pca);
    control->car_led = init_car_leds(&control->pca);
    // startup animation, pwm switch because roof/helicopter leds are not
    // handled by the led driver in the current version of the pcb
    hospital_leds_startup_animation(&control->ups_led, &control->car_led);
    pwm_switch(LED_ROOM_1, entity.led_intensity * LED_INTENSITY_100);
    vTaskDelay(500 / portTICK_PERIOD_MS);
    pwm_switch(LED_ROOM_1, LED_INTENSITY_0);

    ESP_LOGD(TAG, "Charge Level %f", entity.ups.charge_level);

    // init virtual load
    control->virtual_load = virtual_load_init(3, 7.5, 0.6, DAC_CHANNEL_2, 27);
    virtual_load_disable(&control->virtual_load, false);

    ESP_LOGD(TAG, "Starting main task");
    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());
}

//
//---- EXTERNAL CALLBACKS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t hospital_http_resp(char* str)
{
    sprintf(str, "power|%.2f;source|%s", entity.power_in.power / 1000,
        entity.power_source ? "ups" : "grid");

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t hospital_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.led_intensity = val;
            ESP_LOGD(TAG, "LIGHT");
        }
        return ESP_OK;
    }

    if (msg_unmarshall(str, "status", &msg_payload)) {
        uint8_t val = atoi(msg_payload.val);
        if (val <= 3) {
            overrides.remote = val;
            ESP_LOGD(TAG, "STATUS");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void hospital_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_service));
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_sub));
}

void hospital_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    // surely can be written nicer. But this is C...
    char car_accident[6];
    char heli_accident[6];
    switch (entity.status) {
    case EMERGENCY_NONE:
        sprintf(car_accident, "false");
        sprintf(heli_accident, "false");
        break;

    case EMERGENCY_CAR:
        sprintf(car_accident, "true");
        sprintf(heli_accident, "false");
        break;

    case EMERGENCY_HELI:
        sprintf(car_accident, "false");
        sprintf(heli_accident, "true");
        break;

    case EMERGENCY_BOTH:
        sprintf(car_accident, "true");
        sprintf(heli_accident, "true");
        break;
    }

    char data[128];
    sprintf(data,
        "{\"current_in\": %6.1f, \"voltage\": %4.2f, \"power_source\": \"%s\", "
        "\"emergency_car\": %s, \"emergency_heli\": %s }",
        entity.power_in.current, entity.power_in.voltage,
        print_power_source(entity.power_source), car_accident, heli_accident);

    esp_mqtt_client_publish(
        mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
}

void hospital_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }

        if (msg_unmarshall(data, "light", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0)) {
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return;
        }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    hospital_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    ESP_LOGI(TAG, "Init Wifi");
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY);
    mqtt_callbacks_t mqtt_cb
        = { .init_callback = hospital_mqtt_subscribe_topics,
              .post_callback = hospital_mqtt_post,
              .read_callback = hospital_mqtt_read };
    init_mqtt(CONFIG_ENTITY, &mqtt_cb); // Init MQTT message protocol
                                        //
    ESP_LOGI(TAG, "--- Initialization Complete! ---");

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
