//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "driver/ledc.h"
#include "driver/spi_master.h"
#include "driver/timer.h"
#include "driver/touch_pad.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_power.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "sdkconfig.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM GPIO_NUM_0 // 1 Wire EEPROM

#define PIN_TXD GPIO_NUM_1 // Data communication Programming
#define PIN_RXD GPIO_NUM_3

#define PIN_MOSI GPIO_NUM_23 // SPI Pins
#define PIN_CLK GPIO_NUM_21
#define PIN_CS GPIO_NUM_22 // Poti for Currentlimit

#define PIN_BRIGHT GPIO_NUM_14 // 7 SEG brightness
#define PIN_SEG_0 GPIO_NUM_4   // 7 SEG enable
#define PIN_SEG_1 GPIO_NUM_16
#define PIN_SEG_2 GPIO_NUM_17

#define PIN_BCD_0 GPIO_NUM_27 // 7 SEG value in BCD
#define PIN_BCD_1 GPIO_NUM_33
#define PIN_BCD_2 GPIO_NUM_25
#define PIN_BCD_3 GPIO_NUM_26

#define PIN_SRC GPIO_NUM_12 // Power source selection (0 GRID, 1 GAS)
#define PIN_PRICE GPIO_NUM_2
#define PIN_LOAD_EL GPIO_NUM_19
#define PIN_LOAD_TH GPIO_NUM_15
#define PIN_STORAGE GPIO_NUM_39

#define IRQ_INA_GAS             // INA233 GAS Interrupt
#define IRQ_INA_BUS GPIO_NUM_36 // INA233 Bus Interrupt

// ---------- Touch Definitions ----------
#define PIN_TOUCH_NEG TOUCH_PAD_NUM9 // GPIO_NUM_32
#define PIN_TOUCH_POS GPIO_NUM_34    // GPIO_NUM_34
#define PIN_TOUCH_SEL TOUCH_PAD_NUM4 // GPIO_NUM_13

#define TOUCH_THRESH_NO_USE (0)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

// ---------- PWM Channel Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

enum pwm_channels {
    PWM_SEG_BRIGHT = 0, // GPIO_NUM_14
    PWM_SOURCE,         // GPIO_NUM_12
    PWM_LOAD_EL,        // GPIO_NUM_19
    PWM_MAX
};

// ---------- Entity Definitions ----------

/**
 * @brief Energy source selection
 */
enum price_sel_t { PRICE_GRID = 0, PRICE_GAS };

/**
 * @brief Energy source price cent/kWh
 */
typedef struct {
    float grid;
    float gas;
} price_t;

/**
 * @brief Entity status structure
 */
struct Supermarket {
    uint64_t id;              // Unique port identifier
    uint8_t setpoint_imax;    // Current output level
    price_t price;            // Energy market cost
    float source_mix;         // Percentage from grid
    float storage;            // Thermal storage level
    float led_intensity;      // Normalized global brightness
    power_readings power_in;  // Input telemetry
    power_readings power_out; // Output telemetry
} entity;

typedef struct supermarket_control {
    power_monitor power_mon;
    power_monitor power_mon_chp;
    id_bus id_bus;
    // TODO: GPIO
} supermarket_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/supermarket";
static const char mqtt_topic_sub[] = "legos/supermarket/cmd";

static const char* TAG = "Supermarket"; // Logging tag

spi_device_handle_t AD8400_IMAX;  // SPI handle to Digital Pot
static uint8_t setpoint_imax_old; // Current setpoint

static uint16_t display_timer_ms = 1;    // Display-tasks Thread interval
static uint64_t display_timer_alarm;     // Display-tasks Thread timer
static TaskHandle_t display_task_handle; // Display-tasks Thread task handle
static uint16_t entity_timer_ms = 500;   // Main-tasks Thread interval
static uint64_t entity_timer_alarm;      // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;  // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
    TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
    TIMER_DIVIDER };

static uint16_t touchpad_thresh = 600; // Touchpad sensitivity threshold

static uint8_t PIN_DIG[3] = { PIN_SEG_0, PIN_SEG_1, PIN_SEG_2 };
static uint8_t PIN_BCD[4] = { PIN_BCD_0, PIN_BCD_1, PIN_BCD_2, PIN_BCD_3 };
static uint8_t DIG_BUF[3];
static uint8_t BCD_BUF[4][3];

// LEDC channels configuration
ledc_channel_config_t pwm_channel[PWM_MAX] = {
    { .channel = 0,
        .duty = 0,
        .gpio_num = PIN_BRIGHT, // SEG_bright
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 1,
        .duty = 0,
        .gpio_num = PIN_SRC, // Source
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
    { .channel = 2,
        .duty = 0,
        .gpio_num = PIN_LOAD_EL, // Electric_load
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 5000,                      // PWM frequency
    .speed_mode = PWM_HS_MODE,            // PWM timer mode
    .timer_num = PWM_HS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

// SPI bus configuration
static spi_bus_config_t buscfg = {
    .miso_io_num = -1,       // -1 if not used
    .mosi_io_num = PIN_MOSI, // SPI MOSI
    .sclk_io_num = PIN_CLK,  // SPI CLK
    .quadwp_io_num = -1,     // -1 if not used
    .quadhd_io_num = -1      // -1 if not used
};

// SPI device configuration
spi_device_interface_config_t AD8400_IMAX_cfg = {
    .clock_speed_hz = 1000000, // SPI Clock out at 1 MHz
    .mode = 0,                 // SPI Mode 0
    .spics_io_num = PIN_CS,    // SPI CS
    .queue_size = 1,           // SPI queue dimension
                     //.pre_cb=spi_pre_transfer_callback,    // Specify
                     // pre-transfer callback to handle D/C line
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 *
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel,
        duty, fade_time_ms);
    ledc_fade_start(
        pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG, "PWM fade\tch:%d@%d in %d ms", ch, duty, fade_time_ms);
}

/**
 * @brief Set PWM channel dutycycle
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 *
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG, "PWM switch\tch:%d@%d", ch, duty);
}

/**
 * @brief Set Digital Potentiometer
 *
 * @param[in] handle SPI device handle
 * @param[in] value Value
 *
 * @return void
 */
void set_digital_pot(spi_device_handle_t handle, uint8_t value)
{
    uint8_t buffer[2];

    // Serial Data-Word Format [A1 A0 D7 D6 D5 D4 D3 D2, D1 D0]
    buffer[0] = value >> 2; // MSB zero padding
    buffer[1] = value << 6;

    spi_transaction_t t;
    memset(&t, 0, sizeof(t)); // Zero out the transaction
    t.length = 10;            // Command is 10 bits
    t.tx_buffer = &buffer;    // The data is the cmd itself
    // t.tx_buffer=&cmd;                                        // The data is
    // the cmd itself
    ESP_ERROR_CHECK(spi_device_transmit(handle, &t));
}

/**
 * @brief Refresh 7SEG Display
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void display_refresh_task(void* arg)
{
    uint8_t d, b;

    while (1) {
        for (d = 0; d < 3; d++) {
            gpio_set_level(PIN_DIG[0], 0);
            gpio_set_level(PIN_DIG[1], 0);
            gpio_set_level(PIN_DIG[2], 0);
            for (b = 0; b < 4; b++)
                gpio_set_level(PIN_BCD[b], BCD_BUF[b][d]);
            gpio_set_level(PIN_DIG[d], 1);

            xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);
        }
    }
}

/**
 * @brief Set 7SEG Display Value
 *
 * @param[in] val_f Value to set
 *
 * @return bool
 */
void set_display_value(float val_f)
{
    uint16_t value = 10.0 * val_f;

    DIG_BUF[0] = value % 10;
    value /= 10;
    DIG_BUF[1] = value % 10;
    value /= 10;
    DIG_BUF[2] = value % 10;

    for (int d = 0; d < 3; d++)
        for (int b = 0; b < 4; b++)
            BCD_BUF[b][d] = ((DIG_BUF[d] >> b) & 0x01);
}

/**
 * @brief Read Touchpad level
 *
 * @param[in] ch Touchpad channel
 *
 * @return bool
 */
bool read_touch(touch_pad_t ch)
{
    uint16_t touch_value;
    touch_pad_read_filtered(ch, &touch_value);

    return (touch_value < touchpad_thresh);
}

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief Display task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR display_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t1 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_1, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(display_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

void entity_main_tasks(void* arg)
{
    supermarket_control* control = (supermarket_control*)arg;
    uint8_t debounce = 0;
    uint8_t th_cycle = 0;
    uint8_t price_sel = PRICE_GRID;

    while (1) {
        // Wait timer
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
        entity.power_in = read_power(&control->power_mon);
        entity.power_in = read_power(&control->power_mon_chp);

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity * LED_INTENSITY_100;

        // Set display intensity
        pwm_switch(PWM_SEG_BRIGHT, led_intensity);

        // Set electrical load
        pwm_switch(PWM_LOAD_EL, led_intensity);

        // Set thermal load
        th_cycle++;
        if (th_cycle == 1)
            gpio_set_level(PIN_LOAD_TH, 1);
        if (th_cycle == 30)
            gpio_set_level(PIN_LOAD_TH, 0);
        if (th_cycle == 150)
            th_cycle = 0;

        // Get touchpad status
        if (gpio_get_level(PIN_TOUCH_POS) && (debounce == 0)) {
            debounce = 5;
            switch (price_sel) {
            case PRICE_GRID:
                entity.price.grid += 0.1; // 0.1€ increment per touch
                if (entity.price.grid > 30.0)
                    entity.price.grid = 30.0;
                break;
            case PRICE_GAS:
                entity.price.gas += 0.1; // 0.1€ increment per touch
                if (entity.price.gas > 30.0)
                    entity.price.gas = 30.0;
                break;
            }
        }
        if (read_touch(PIN_TOUCH_NEG) && (debounce == 0)) {
            debounce = 5;
            switch (price_sel) {
            case PRICE_GRID:
                entity.price.grid -= 0.1; // 0.1€ decrement per touch
                if (entity.price.grid < 10.0)
                    entity.price.grid = 10.0;
                break;
            case PRICE_GAS:
                entity.price.gas -= 0.1; // 0.1€ decrement per touch
                if (entity.price.gas < 10.0)
                    entity.price.gas = 10.0;
                break;
            }
        }
        if (read_touch(PIN_TOUCH_SEL) && (debounce == 0)) {
            debounce = 10;
            switch (price_sel) {
            case PRICE_GRID:
                price_sel = PRICE_GAS;
                break;
            case PRICE_GAS:
                price_sel = PRICE_GRID;
                break;
            }
            gpio_set_level(PIN_PRICE, price_sel);
        }
        if (debounce > 0)
            debounce--;

        // Update source selection indicator
        switch (price_sel) {
        case PRICE_GRID:
            set_display_value(entity.price.grid);
            break;
        case PRICE_GAS:
            set_display_value(entity.price.gas);
            break;
        }

        // Set energy source mix
        entity.source_mix = ((entity.price.gas - entity.price.grid) + 0.5);
        if (entity.source_mix < 0.0)
            entity.source_mix = 0.0;
        if (entity.source_mix > 1.0)
            entity.source_mix = 1.0;

        pwm_fade(PWM_SOURCE, entity.source_mix * LED_INTENSITY_100, 100);

        // Source control action
        entity.setpoint_imax = (1 - entity.source_mix) * 0xFF;

        // Update current limit
        if (abs(entity.setpoint_imax - setpoint_imax_old) > 2) {
            setpoint_imax_old = entity.setpoint_imax;
            set_digital_pot(AD8400_IMAX, entity.setpoint_imax);
        }
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init GPIO
 *
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = ((1ULL << PIN_BCD_0) | (1ULL << PIN_BCD_1)
        | (1ULL << PIN_BCD_2) | (1ULL << PIN_BCD_3) | (1ULL << PIN_SEG_0)
        | (1ULL << PIN_SEG_1) | (1ULL << PIN_SEG_2) | (1ULL << PIN_PRICE)
        | (1ULL << PIN_LOAD_TH));
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);

    gpio_set_level(PIN_BCD_0, 0);
    gpio_set_level(PIN_BCD_1, 0);
    gpio_set_level(PIN_BCD_2, 0);
    gpio_set_level(PIN_BCD_3, 0);
    gpio_set_level(PIN_SEG_0, 0);
    gpio_set_level(PIN_SEG_1, 0);
    gpio_set_level(PIN_SEG_2, 0);
    gpio_set_level(PIN_LOAD_TH, 0);

    // INA IRQ Missing
    return ESP_OK;
}

/**
 * @brief Init PWM channels
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(
        &pwm_timer)); // Set configuration of timer0 for low speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < PWM_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0)); // Initialize fade service
}

/**
 * @brief Init the SPI Interface
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_spi()
{
    ESP_ERROR_CHECK(
        spi_bus_initialize(HSPI_HOST, &buscfg, 0)); // Initialize the SPI bus
    return (spi_bus_add_device(HSPI_HOST, &AD8400_IMAX_cfg,
        &AD8400_IMAX)); // Attach the Poti to the SPI bus
}

/**
 * @brief Init Touch Channels
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_touch()
{
    ESP_ERROR_CHECK(
        touch_pad_init()); // The default fsm mode is software trigger mode.

    ESP_ERROR_CHECK(touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5,
        TOUCH_HVOLT_ATTEN_1V)); // set ref voltage vor charging/discharging
    ESP_ERROR_CHECK(touch_pad_config(
        PIN_TOUCH_NEG, TOUCH_THRESH_NO_USE)); // insert touchpad channels that
                                              // are used (see schematic)
    ESP_ERROR_CHECK(gpio_set_direction(PIN_TOUCH_POS, GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_SEL, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD));

    return ESP_OK;
}

/**
 * @brief Init timer for 7SEG Display refresh task
 *
 * @return
 *     - ESP_OK   Success
 */
void init_display_task()
{
    set_display_value(entity.price.grid);

    // Create display refresh task
    display_timer_alarm = (uint64_t)display_timer_ms * TIMER_SCALE / 1000ULL;
    xTaskCreate(display_refresh_task, "display_refresh_task", 2048, NULL, 4,
        &display_task_handle);

    // Refresh timer
    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_1, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_1));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_1, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_1, display_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_1));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_1, display_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_1));
}

/**
 * @brief Init timer for main entity task
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    entity_timer_alarm = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_entity(supermarket_control* control)
{
    // Set default LED intensity
    entity.led_intensity = 0.5;

    // Set default prices
    entity.price.gas = 18.5F;
    entity.price.grid = 18.5F;

    ESP_LOGD(TAG, "Init GPIO");
    init_gpio();
    ESP_LOGD(TAG, "Init PWM");
    init_pwm();
    ESP_LOGD(TAG, "Init SPI");
    init_spi();
    ESP_LOGD(TAG, "Init touch slider");
    init_touch();

    ESP_LOGD(TAG, "Init ID bus");
    init_id_bus(&control->id_bus, PIN_EEPROM);
    entity.id = read_id(&control->id_bus);
    if (entity.id == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init I2C");
    init_i2c(CONFIG_PIN_SDA, CONFIG_PIN_SCL, CONFIG_PWR_MON_I2C_BUS,
        CONFIG_I2C_FREQ);

    ESP_LOGD(TAG, "Init power monitor");
    control->power_mon
        = pwr_monitor_init(CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon,
        atof(CONFIG_PWR_MON_R_SHUNT), atof(CONFIG_PWR_MON_I_MAX)));
    ESP_LOGD(TAG, "Init power monitor for CHP");
    control->power_mon
        = pwr_monitor_init(CONFIG_PWR_MON_CHP_I2C_BUS, CONFIG_PWR_MON_CHP_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon,
        atof(CONFIG_PWR_MON_CHP_R_SHUNT), atof(CONFIG_PWR_MON_CHP_I_MAX)));

    ESP_LOGD(TAG, "Init display task");
    init_display_task();

    ESP_LOGD(TAG, "Starting main task");
    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());
}

//
//---- EXTERNAL FUNCTIONS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t supermarket_http_resp(char* str)
{
    sprintf(str, "power|%.2f;chp|%.2f", entity.power_in.power / 1000,
        entity.power_out.power * 1000);

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t supermarket_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.led_intensity = val;
            ESP_LOGD(TAG, "LIGHT");
        }
        return ESP_OK;
    }

    if (msg_unmarshall(str, "delta", &msg_payload)) {
        int8_t val = atoi(msg_payload.val);
        if ((val >= -2) && (val <= 2)) {
            entity.price.gas = val * 0.25 + entity.price.grid;
            ESP_LOGD(TAG, "DELTA");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void supermarket_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_service));
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_sub));
}

void supermarket_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    char data[256];
    sprintf(data,
        "%10s GRID: %7.1f mA [%4.2f V] (%4.1f €)\tGAS: %7.1f mA [%4.2f V] "
        "(%4.1f €)",
        TAG, entity.power_in.current, entity.power_in.voltage,
        entity.price.grid, entity.power_out.current, entity.power_out.voltage,
        entity.price.gas);
    esp_mqtt_client_publish(
        mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
}

void supermarket_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }

        if (msg_unmarshall(data, "light", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0)) {
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return;
        }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    supermarket_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY);
    mqtt_callbacks_t mqtt_cb
        = { .init_callback = supermarket_mqtt_subscribe_topics,
              .post_callback = supermarket_mqtt_post,
              .read_callback = supermarket_mqtt_read };
    init_mqtt(CONFIG_ENTITY, &mqtt_cb); // Init MQTT message protocol

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
