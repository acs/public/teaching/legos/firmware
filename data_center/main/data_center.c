//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "driver/ledc.h"
#include "driver/spi_master.h"
#include "driver/timer.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "hal/dac_types.h"
#include "leds.h"
#include "legos_common.h"
#include "legos_fs.h"
#include "legos_http.h"
#include "legos_ids.h"
#include "legos_init.h"
#include "legos_libs.h"
#include "legos_mqtt.h"
#include "legos_power.h"
#include "legos_wifi.h"
#include "mqtt_client.h"
#include "pca9956.h"
#include "sdkconfig.h"
#include "ups.h"
#include "virtual_load.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM GPIO_NUM_0 // 1 Wire EEPROM

#define PIN_TXD GPIO_NUM_1 // Data communication
#define PIN_RXD GPIO_NUM_3

#define PIN_MISO GPIO_NUM_19 // SPI Pins
#define PIN_MOSI GPIO_NUM_22
#define PIN_CLK GPIO_NUM_21
#define PIN_CS GPIO_NUM_23

#define PIN_LOAD GPIO_NUM_32 // LED Servers
#define PIN_DATA_1 GPIO_NUM_12
#define PIN_DATA_2 GPIO_NUM_14
#define PIN_DATA_3 GPIO_NUM_33
#define PIN_DATA_4 GPIO_NUM_27
#define PIN_DATA_5 GPIO_NUM_26
#define PIN_DATA_6 GPIO_NUM_25

#define PIN_UPS_1 GPIO_NUM_13 // LED UPS
#define PIN_UPS_2 GPIO_NUM_2
#define PIN_UPS_3 GPIO_NUM_4

#define PIN_SRC GPIO_NUM_15 // Power source selection (0 GRID, 1 UPS)

#define IRQ_INA_BUS GPIO_NUM_17 // INA233 Interrupt

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

/**
 * @brief Entity status structure
 */
struct DataCenter {
    uint64_t id;             // Unique port identifier
    bool power_source;       // Power source (0 GRID, 1 UPS)
    float led_intensity;     // Normalized global brightness
    float server_load;       // Normalized load
    power_readings power_in; // Input telemetry
    ups ups;
} entity;

typedef struct data_center_control {
    power_monitor power_mon;
    id_bus id_bus;
    pca9956_t pca;
    server_leds_t servers;
    ups_leds_t ups_led;
    virtual_load_t virtual_load;
    // TODO: GPIO
} data_center_control;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "legos/data_center";
static const char mqtt_topic_sub[] = "legos/data_center/cmd";

static const char* TAG = "Data center"; // Logging tag

static spi_device_handle_t SD_CARD; // SPI handle to SD Card Reader

static uint16_t entity_timer_ms = 100;  // Main-tasks Thread interval
static uint64_t entity_timer_alarm;     // Main-tasks Thread timer
static TaskHandle_t entity_task_handle; // Main-tasks Thread task handle

// LEDC timer configuration
static ledc_timer_config_t pwm_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // PWM duty-cycle resolution
    .freq_hz = 5000,                      // PWM frequency
    .speed_mode = PWM_LS_MODE,            // PWM timer mode
    .timer_num = PWM_LS_TIMER,            // PWM timer index
    .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
};

// SPI bus configuration
static spi_bus_config_t buscfg = {
    .miso_io_num = PIN_MISO, // SPI MISO
    .mosi_io_num = PIN_MOSI, // SPI MOSI
    .sclk_io_num = PIN_CLK,  // SPI CLK
    .quadwp_io_num = -1,     // -1 if not used
    .quadhd_io_num = -1      // -1 if not used
};

// SPI device configuration
static spi_device_interface_config_t SD_CARD_cfg = {
    .clock_speed_hz = 1000000, // SPI Clock out at 1 MHz
    .mode = 0,                 // SPI Mode 0
    .spics_io_num = PIN_CS,    // SPI CS
    .queue_size = 1,           // SPI queue dimension
                     //.pre_cb=spi_pre_transfer_callback,    // Specify
                     // pre-transfer callback to handle D/C line
};

//
//---- SERVICE FUNCTIONS ------------------------------------------------------
//

/**
 * @brief Entity main task timer ISR
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
    // Clear interrupt
    TIMERG0.int_clr_timers.t0 = 1;

    // Restart timer
    timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

    // Notify task
    xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
    portYIELD_FROM_ISR();
}

/**
 * @brief Entity main task
 *
 * @param[in] arg Parameters passed to the task
 *
 * @return void
 */
void entity_main_tasks(void* arg)
{
    data_center_control* control = (data_center_control*)arg;
    // Initialize random seed
    srand(time(NULL));

    while (1) {
        // Wait timer
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
        entity.power_in = read_power(&control->power_mon);

        // check bus voltage.
        // voltage above 1600 mV is considered as active power supply
        if (entity.power_in.voltage > 1600) {
            entity.power_source = 1;
            charge(&entity.ups, 100);
        } else {
            entity.power_source = 0;
            discharge(&entity.ups, 100);
        }

        // set virtual load
        if (entity.ups.charge_level < 99.9f) {
            virtual_load_set_percent(&control->virtual_load, .3);
        } else {
            virtual_load_set_percent(&control->virtual_load, .15);
        }

        // Set Server LED blink
        if (entity.ups.charge_level < 0.01) {
            server_rack_shut_down(&control->servers);
        } else {
            server_rack_random_blink(&control->servers);
        }

        // Set the UPS to display the current value. TODO: Implement UPS
        ups_leds_set_perc(&control->ups_led, entity.ups.charge_level, false);

        ESP_LOGD(TAG, "Charge: %f", entity.ups.charge_level);
    }
    vTaskDelete(NULL);
}

//
//---- INIT FUNCTIONS ----------------------------------------------------------
//

/**
 * @brief Init GPIO
 *
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    ESP_ERROR_CHECK(gpio_set_direction(PIN_SRC, GPIO_MODE_INPUT));

    return ESP_OK;
}

esp_err_t init_spi()
{
    ESP_ERROR_CHECK(
        spi_bus_initialize(HSPI_HOST, &buscfg, 0)); // Initialize the SPI bus
    return (spi_bus_add_device(
        HSPI_HOST, &SD_CARD_cfg, &SD_CARD)); // Add the SD-Card to the SPI bus
}

esp_err_t init_tasks_timer()
{
    timer_config_t cfg = { TIMER_ALARM_EN, // Timer configuration
        TIMER_PAUSE, TIMER_INTR_LEVEL, TIMER_COUNT_UP, TIMER_AUTORELOAD_EN,
        TIMER_DIVIDER };

    entity_timer_alarm = (uint64_t)entity_timer_ms * TIMER_SCALE / 1000ULL;

    ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
    ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(
        timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
    ESP_ERROR_CHECK(
        timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
    ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
    ESP_ERROR_CHECK(timer_isr_register(
        TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

    ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

    return ESP_OK;
}

void init_entity(data_center_control* control)
{
    // Set default LED intensity
    entity.led_intensity = 0.5;
    entity.server_load = 0.25;

    ESP_LOGD(TAG, "Init GPIO");
    init_gpio();
    ESP_LOGD(TAG, "Init SPI");
    init_spi();

    ESP_LOGD(TAG, "Init ID bus");
    init_id_bus(&control->id_bus, PIN_EEPROM);
    entity.id = read_id(&control->id_bus);
    if (entity.id == 0) {
        ESP_LOGE(TAG, "can´t get connected node ID");
    }

    ESP_LOGD(TAG, "Init I2C");
    init_i2c(CONFIG_PWR_MON_PIN_SDA, CONFIG_PWR_MON_PIN_SCL,
        CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_I2C_FREQ);
    control->power_mon
        = pwr_monitor_init(CONFIG_PWR_MON_I2C_BUS, CONFIG_PWR_MON_ADDR);
    ESP_ERROR_CHECK(set_calibration(&control->power_mon,
        atof(CONFIG_PWR_MON_R_SHUNT), atof(CONFIG_PWR_MON_I_MAX)));

    ESP_LOGD(TAG, "Init LEDs");
    // pca reset pin TODO: Put this in config
    gpio_set_direction(25, GPIO_MODE_OUTPUT);
    gpio_set_level(25, 1);
    control->pca = pca9956_init(CONFIG_PWR_MON_I2C_BUS, 0x19, 2000, 17);
    control->ups_led = init_ups_leds(&control->pca);
    ups_leds_startup_animation(&control->ups_led);
    control->servers = init_server_leds(&control->pca);
    server_rack_startup_animation(&control->servers);

    // init virtual load
    control->virtual_load = virtual_load_init(3, 7.5, 0.6, DAC_CHANNEL_2, 27);
    virtual_load_disable(&control->virtual_load, false);

    ESP_LOGD(TAG, "Starting main task");
    xTaskCreate(entity_main_tasks, "entity_main_task", 2048, (void*)control, 4,
        &entity_task_handle);
    ESP_ERROR_CHECK(init_tasks_timer());

    // ups config
    entity.ups.charge_level = 0.0;
    entity.ups.charge_rate = 2.5;
    entity.ups.discharge_rate = 5.0;
}

//
//---- EXTERNAL CALLBACKS ------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *     - ESP_OK   Success
 */
esp_err_t data_center_http_resp(char* str)
{
    sprintf(str, "power|%.2f;source|%s", entity.power_in.power / 1000,
        entity.power_source ? "ups" : "grid");

    return ESP_OK;
}

/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 *
 * @param[in,out] str Message buffer
 *
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t data_center_http_read(char* str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)) {
        float val = atof(msg_payload.val);
        if ((val >= 0.0) && (val <= 1.0)) {
            entity.led_intensity = val;
            ESP_LOGD(TAG, "LIGHT");
        }
        return ESP_OK;
    }

    return ESP_FAIL;
}

void data_center_mqtt_subscribe_topics(esp_mqtt_client_handle_t mqtt_client)
{
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_service));
    ESP_ERROR_CHECK(mqtt_subscribe(&mqtt_client, mqtt_topic_sub));
}

void data_center_mqtt_post(esp_mqtt_client_handle_t mqtt_client)
{
    char data[128];
    sprintf(data, "{\"current_in\": %6.1f, \"voltage\": %4.2f, \"UPS\": %s}",
        -entity.power_in.current, entity.power_in.voltage,
        entity.power_source ? "true" : "false");
    esp_mqtt_client_publish(
        mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
}

void data_center_mqtt_read(esp_mqtt_event_handle_t mqtt_event)
{
    char* data = (char*)malloc(mqtt_event->data_len + 1);
    memcpy(data, mqtt_event->data, mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len + 1);
    memcpy(topic, mqtt_event->topic, mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic, mqtt_topic_service)) {
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val, TAG)) {
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return;
            }

        if (msg_unmarshall(data, "light", &msg_payload)) {
            float val = atof(msg_payload.val);
            if ((val >= 0.0) && (val <= 1.0)) {
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return;
        }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    ESP_LOGI(TAG, "Configuring Log levels");
    /*esp_log_level_set("*", ESP_LOG_INFO);*/

    ESP_LOGI(TAG, "Init modules..");
    data_center_control control;
    init_entity(&control); // Init entity profile
    ESP_ERROR_CHECK(init_flash());
    ESP_ERROR_CHECK(init_spiffs());
    init_wifi(); // Init WIFI network interface
    ESP_ERROR_CHECK(server_start());

    ESP_LOGI(TAG, "Init MQTT name: %s", CONFIG_ENTITY);
    mqtt_callbacks_t mqtt_cb
        = { .init_callback = data_center_mqtt_subscribe_topics,
              .post_callback = data_center_mqtt_post,
              .read_callback = data_center_mqtt_read };
    init_mqtt(CONFIG_ENTITY, &mqtt_cb); // Init MQTT message protocol

    while (1) {
        vTaskDelay(100 / portTICK_PERIOD_MS); // Yield task
    }
}
