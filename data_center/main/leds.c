#include "leds.h"
#include "esp_log.h"
#include "pca9956.h"

static const char* TAG = "Data center LEDs"; // Logging tag

int ups_led_pins[12] = { 9, 8, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 };
float i_white = (100 / 180.0)
    * 5; // white led has Iv = 140mcd @ 5ma, we normalize to 100mcd
float i_blue = (100 / 140.0)
    * 20; // white led has Iv = 140mcd @ 5ma, we normalize to 100mcd

ups_leds_t init_ups_leds(pca9956_t* pca)
{
    for (int i = 0; i < 12; ++i) {
        pca9956_set_max_current(pca, ups_led_pins[i], i_white);
    }
    ups_leds_t ups_leds = {
        .controller = pca,
        .anim_step = 0,
    };
    return ups_leds;
}

void ups_leds_set_perc(ups_leds_t* ups_leds, uint8_t perc, bool inv)
{
    int max_led = (12.0 / 100.0) * (float)perc;
    /*ESP_LOGD(TAG, "max_led %d", max_led);*/
    for (int i = 0; i < max_led; ++i) {
        uint8_t duty = 255;
        if (inv) {
            duty = 0;
        }
        pca9956_single_switch(ups_leds->controller, ups_led_pins[i], duty);
    }
    for (int i = max_led; i < 12; ++i) {
        uint8_t duty = 0;
        if (inv) {
            duty = 255;
        }
        pca9956_single_switch(ups_leds->controller, ups_led_pins[i], duty);
    }
    if (max_led != 12) {
        uint8_t last_led_duty
            = ((12.0 / 100.0) * (float)perc - (float)max_led) * 255;
        /*ESP_LOGD(TAG, "last_led_duty %d", last_led_duty);*/
        if (inv) {
            last_led_duty = 255 - last_led_duty;
        }
        pca9956_single_switch(
            ups_leds->controller, ups_led_pins[max_led], last_led_duty);
    }
}

void ups_leds_startup_animation(ups_leds_t* ups_leds)
{
    const int speed = 3;
    for (int i = 0; i < 100; i += 2) {
        ups_leds_set_perc(ups_leds, i, false);
        vTaskDelay(speed / portTICK_PERIOD_MS); // Yield task
    }
    for (int i = 0; i < 100; i += 2) {
        ups_leds_set_perc(ups_leds, i, true);
        vTaskDelay(speed / portTICK_PERIOD_MS); // Yield task
    }
    for (int i = 0; i < 12; ++i) {
        pca9956_single_switch(ups_leds->controller, ups_led_pins[i], 0);
    }
}

void init_server_rack(server_rack_t* rack, pca9956_t* pca)
{
    pca9956_set_max_current(pca, rack->led_blue, i_blue);
    pca9956_set_max_current(pca, rack->led_white, i_white);
}

server_leds_t init_server_leds(pca9956_t* pca)
{
    server_leds_t server_leds = { .controller = pca,
        .servers = { (server_rack_t) { 1, 0 }, (server_rack_t) { 23, 22 },
            (server_rack_t) { 10, 11 }, (server_rack_t) { 2, 3 },
            (server_rack_t) { 4, 5 }, (server_rack_t) { 6, 7 } } };
    for (int i = 0; i < 6; ++i) {
        init_server_rack(&server_leds.servers[i], pca);
    }
    return server_leds;
}

void light_server_rack(
    server_rack_t* rack, pca9956_t* pca, uint8_t perc_blue, uint8_t perc_white)
{
    pca9956_single_switch(pca, rack->led_blue, perc_blue);
    pca9956_single_switch(pca, rack->led_white, perc_white);
}

void server_rack_startup_animation(server_leds_t* leds)
{
    const int speed = 150;

    for (int i = 0; i < 3; ++i) {
        light_server_rack(&leds->servers[0], leds->controller, 255, 0);
        light_server_rack(&leds->servers[1], leds->controller, 0, 255);
        light_server_rack(&leds->servers[2], leds->controller, 255, 0);
        light_server_rack(&leds->servers[3], leds->controller, 0, 255);
        light_server_rack(&leds->servers[4], leds->controller, 255, 0);
        light_server_rack(&leds->servers[5], leds->controller, 0, 255);
        vTaskDelay(speed / portTICK_PERIOD_MS); // Yield task
        light_server_rack(&leds->servers[0], leds->controller, 0, 255);
        light_server_rack(&leds->servers[1], leds->controller, 255, 0);
        light_server_rack(&leds->servers[2], leds->controller, 0, 255);
        light_server_rack(&leds->servers[3], leds->controller, 255, 0);
        light_server_rack(&leds->servers[4], leds->controller, 0, 255);
        light_server_rack(&leds->servers[5], leds->controller, 255, 0);
        vTaskDelay(speed / portTICK_PERIOD_MS); // Yield task
    }
    for (int i = 0; i < 6; ++i) {
        light_server_rack(&leds->servers[5], leds->controller, 0, 0);
    }
}

void server_rack_random_blink(server_leds_t* leds)
{
    uint8_t rand_led = rand() % 0x40;
    for (int serv = 0; serv < 6; ++serv) {
        if (rand_led & (0x01 << serv)) {
            light_server_rack(&leds->servers[serv], leds->controller, 255, 255);
        } else {
            light_server_rack(&leds->servers[serv], leds->controller, 0, 0);
        }
    }
}

void server_rack_shut_down(server_leds_t* leds)
{
    for (int i = 0; i < 6; ++i) {
        light_server_rack(&leds->servers[i], leds->controller, 0, 0);
    }
}
