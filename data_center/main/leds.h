#include "pca9956.h"

typedef struct ups_leds {
    pca9956_t* controller;
    uint8_t anim_step;
} ups_leds_t;

typedef struct server_rack {
    uint8_t led_blue;
    uint8_t led_white;
} server_rack_t;

typedef struct server_leds {
    pca9956_t* controller;
    server_rack_t servers[6];
} server_leds_t;

ups_leds_t init_ups_leds(pca9956_t* pca);

void ups_leds_set_perc(ups_leds_t* ups_leds, uint8_t perc, bool inv);

void ups_leds_startup_animation(ups_leds_t* ups_leds);

server_leds_t init_server_leds(pca9956_t* pca);

void light_server_rack(
    server_rack_t* rack, pca9956_t* pca, uint8_t perc_blue, uint8_t perc_white);

void server_rack_startup_animation(server_leds_t* leds);

void server_rack_random_blink(server_leds_t* leds);

void server_rack_shut_down(server_leds_t* leds);