# General CMake options for LEGOS

set(EXTRA_COMPONENT_DIRS ../components)
add_compile_options (-fdiagnostics-color=always)
include($ENV{IDF_PATH}/tools/cmake/project.cmake)
